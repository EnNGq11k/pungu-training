#include "unpack.hpp"

#include "zlib.h"
#include <fstream>

#if defined(__linux__)
#include <cstring>
#endif

std::vector<unsigned char> zip::unzipBZP(const std::string &filePath)
{
    std::vector<unsigned char> data;

    FILE *f = fopen(filePath.c_str(), "rb");
    if (f == NULL)
    {
        return data;
    }

    // Read all the bytes in the file
    int c = fgetc(f);
    while (c != EOF)
    {
        data.push_back((char)c);
        c = fgetc(f);
    }

    fclose(f);

    return gzipInflate(data);
}

// https://windrealm.org/tutorials/decompress-gzip-stream.php
std::vector<unsigned char> zip::gzipInflate(const std::vector<unsigned char> &compressedBytes)
{
    std::vector<unsigned char> uncompressedBytes;
    if (compressedBytes.size() == 0)
    {
        uncompressedBytes = compressedBytes;
        return uncompressedBytes;
    }

    uncompressedBytes.clear();

    unsigned full_length = (unsigned int)compressedBytes.size();
    unsigned half_length = (unsigned int)(full_length / 2);

    unsigned uncompLength = full_length;
    char *uncomp = (char *)calloc(sizeof(char), uncompLength);

    z_stream strm;
    strm.next_in = (Bytef *)compressedBytes.data();
    strm.avail_in = (uInt)compressedBytes.size();
    strm.total_out = 0;
    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;

    bool done = false;

    if (inflateInit2(&strm, (16 + MAX_WBITS)) != Z_OK)
    {
        free(uncomp);
        return uncompressedBytes;
    }

    while (!done)
    {
        // If our output buffer is too small
        if (strm.total_out >= uncompLength)
        {
            // Increase size of output buffer
            char *uncomp2 = (char *)calloc(sizeof(char), uncompLength + half_length);
            memcpy(uncomp2, uncomp, uncompLength);
            uncompLength += half_length;
            free(uncomp);
            uncomp = uncomp2;
        }

        strm.next_out = (Bytef *)(uncomp + strm.total_out);
        strm.avail_out = uncompLength - strm.total_out;

        // Inflate another chunk.
        int err = inflate(&strm, Z_SYNC_FLUSH);
        if (err == Z_STREAM_END)
        {
            done = true;
        }
        else if (err != Z_OK)
        {
            break;
        }
    }

    if (inflateEnd(&strm) != Z_OK)
    {
        free(uncomp);
        return uncompressedBytes;
    }

    for (size_t i = 0; i < strm.total_out; ++i)
    {
        uncompressedBytes.push_back(uncomp[i]);
    }

    free(uncomp);
    return uncompressedBytes;
}