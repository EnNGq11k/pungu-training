#ifndef UNPACK_PPP_H
#define UNPACK_PPP_H

#include <string>
#include <vector>

namespace zip
{
std::vector<unsigned char> unzipBZP(const std::string &filePath);
std::vector<unsigned char> gzipInflate(const std::vector<unsigned char> &compressedBytes);
} // namespace zip

#endif // UNPACK_PPP_H