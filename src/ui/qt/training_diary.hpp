#ifndef PO_TRAININGDIARY_H
#define PO_TRAININGDIARY_H

#include <QWidget>

QT_BEGIN_NAMESPACE
class QItemSelection;
class QHBoxLayout;
QT_END_NAMESPACE

class training_view;
class training_summary_view;
class training_cache;
class training_calendar;
class training_list;

namespace pt_data
{
class training_bikes;
}

class training_diary : public QWidget
{
    Q_OBJECT

  private:
    training_list *training_list_view;
    training_view *training_details;
    training_summary_view *summary_widget;
    QHBoxLayout *main_layout;
    std::shared_ptr<training_cache> cache;
    std::shared_ptr<pt_data::training_bikes> bikes;
    training_calendar *calendar;

  public:
    training_diary(QWidget *parent = 0);
    ~training_diary();
    void load_data(const QString &path, const int year, const qlonglong &date_time, const bool refresh_cache);
    QDate get_selected_date() const;
    void new_training(const QDate &data);

  private:
    void load_bikes(const QString &path);
    void training_selected(const QItemSelection &selected, const QItemSelection &deselected);
    void write_to_log(const QString &message) const;
    void edit_activity(const QModelIndex &index);
    void calendar_clicked(const QDate &date);
    QString get_data_path();
};

#endif