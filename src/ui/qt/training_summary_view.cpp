#include "training_summary_view.hpp"

#include <QtWidgets>

#include "../../pt_proto/pt_bikes.pb.h"
#include "../../training_data/activity_type.hpp"
#include "../../training_data/duration.hpp"
#include "running_index_chart.hpp"
#include "training_charts.hpp"
#include "training_summary_detail.hpp"

training_summary_view::training_summary_view(QWidget *parent)
    : QWidget(parent), details_view(new training_summary_detail)
{
    auto main_layout = new QVBoxLayout(this);
    main_layout->setContentsMargins(0, 5, 0, 0);

    details_view->setSelectionMode(QAbstractItemView::SelectionMode::NoSelection);
    details_view->setEditTriggers(QAbstractItemView::NoEditTriggers);

    chart_widget = new running_index_chart();

    auto tab_view = new QTabWidget;
    tab_view->addTab(details_view, tr("&Details"));
    tab_view->addTab(chart_widget, tr("&Graphs"));
    main_layout->addWidget(tab_view);
}

void training_summary_view::update(const std::map<long long, pt_data::training> &training_data, const uint &year,
                                   const pt_data::training_bikes &bikes)
{
    chart_widget->load_running_index(training_data, year);
    details_view->load(training_data, year, bikes);
}

void training_summary_view::write_to_log(const QString &message) const
{
    static QLoggingCategory category("Summary view");
    qCInfo(category) << message;
}