#ifndef PO_TRAINING_CHARTS_H
#define PO_TRAINING_CHARTS_H

#include "../../pt_proto/pt_data.pb.h"
#include "google/protobuf/repeated_field.h"
#include <QWidget>

class QLineSeries;
class QAbstractAxis;
class QDateTimeAxis;
class QChartView;
class QGridLayout;

class training_charts : public QWidget
{
    template <class T> void set_min_max(const T &value, T &min, T &max)
    {
        if (value < min)
        {
            min = value;
        }
        if (value > max)
        {
            max = value;
        }
    }

    template <class T> bool contains_valid_data(const google::protobuf::RepeatedField<T> data)
    {
        for (auto &d : data)
        {
            if (d > 1)
            {
                return true;
            }
        }

        return false;
    }

    Q_OBJECT

  private:
    QGridLayout *main_layout;
    QChartView *speed_chart_view;

    QWidget *current_hr_power_view;
    QWidget *current_speed_view;

  public:
    training_charts(QWidget *parent = 0);

    void load(const pt_data::training &training);

  private:
    void load_hr_power(const pt_data::training &training);
    void load_speed(const pt_data::training &training);
    void create_default_speed_chart(const pt_data::training &training);
    void create_running_speed_chart(const pt_data::training &training);
    void create_swim_speed_chart(const pt_data::training &training);
    void add_speed_chart_view(QLineSeries *speed_series, QAbstractAxis *y_axis);
    void add_no_speed_chart_view();
    void add_hr_power_chart_view(std::vector<QLineSeries *> &hr_power_series, QAbstractAxis *y_axis);

    void set_axis_style(QDateTimeAxis *x_axis, QAbstractAxis *y_axis);

    void hr_power_tooltip(QPointF point, bool /*state*/);
    void speed_tooltip(QPointF point, bool /*state*/);
    void show_tooltip(const QPointF &point, const QChartView *const chartView);

    void replace_hr_power_view(QWidget *new_view, int stretch);
    void replace_speed_view(QWidget *new_view, int stretch);

    void write_to_log(const QString &message) const;
};

#endif