#include "training_detail_list.hpp"

#include <QtWidgets>

#include "../../pt_proto/pt_data.pb.h"
#include "../../training_data/activity_type.hpp"
#include "activity_translation.hpp"
#include "date_time.hpp"

training_detail_list::training_detail_list(QWidget *parent) : QTreeView(parent)
{
    setModel(new QStandardItemModel(this));
}

void training_detail_list::load(const pt_data::training &training)
{
    QStandardItemModel *model = static_cast<QStandardItemModel *>(this->model());
    model->clear();

    QStringList labels = {"Name", "Value"};
    model->setHorizontalHeaderLabels(labels);

    QString date_time;
    date_time_to_qstring(training.date_time(), Q_DATE_TIME_FORMAT, date_time);

    QString duration;
    duration_to_qstring(training.duration(), duration);

    append_row(tr("Mood"), QString::fromStdString(training.mood()), model);
    append_row("", "", model);
    append_row(tr("Activity"), activity_type_translation::translate(training.activity_type()), model);
    append_row(tr("Date"), date_time, model);
    append_row(tr("Duration"), duration, model);

    if (training.distance() != 0)
    {
        append_row(tr("Distance"), training.distance(), model);
    }

    if (training.energy() != 0)
    {
        append_row(tr("Energy"), training.energy(), model);
    }

    if (training.running_index() != 0)
    {
        append_row(tr("Running index"), training.running_index(), model);
    }

    if (training.heart_rate().avg() != 0)
    {
        append_row("", "", model);
        append_row(tr("Heart rate average"), training.heart_rate().avg(), model);
        append_row(tr("Heart rate max"), training.heart_rate().max(), model);
    }

    if (training.speed().avg() != 0)
    {

        append_row("", "", model);

        if (activity_type::is_running_activity(training.activity_type()))
        {
            append_row(tr("Speed average"), QString::fromStdString(training.speed_minutes_per_km().avg()) + " (min/km)",
                       model);
            append_row(tr("Speed max"), QString::fromStdString(training.speed_minutes_per_km().max()) + " (min/km)",
                       model);
        }
        else if (activity_type::is_swim_activity(training.activity_type()))
        {
            append_row(tr("Speed average"),
                       QString::fromStdString(training.speed_minutes_per_100m().avg()) + " (min/100m)", model);
            append_row(tr("Speed max"), QString::fromStdString(training.speed_minutes_per_100m().max()) + " (min/100m)",
                       model);
        }
        else
        {
            append_row(tr("Speed average"), tr("%1 %2").arg(training.speed().avg()).arg("(km/h)"), model);
            append_row(tr("Speed max"), tr("%1 %2").arg(training.speed().max()).arg("(km/h)"), model);
        }
    }

    if (training.power().avg() != 0)
    {
        append_row("", "", model);
        append_row(tr("Power average"), training.power().avg(), model);
        append_row(tr("Power max"), training.power().max(), model);
    }

    if (training.cadence().avg() != 0)
    {
        append_row("", "", model);
        append_row(tr("Cadence average"), training.cadence().avg(), model);
        append_row(tr("Cadence max"), training.cadence().max(), model);
    }

    if (training.altitude().avg() != 0)
    {
        append_row("", "", model);
        append_row(tr("Altitude"), training.altitude().avg(), model);
        append_row(tr("Altitude min"), training.altitude().min(), model);
        append_row(tr("Altitude max"), training.altitude().max(), model);
        append_row(tr("Ascent"), training.altitude().ascent(), model);
        append_row(tr("Descent"), training.altitude().descent(), model);
    }

    append_row("", "", model);
    append_row(tr("Imported from Polar device"),
               training.data_type() == pt_data::data_type::CUSTOM ? tr("no") : tr("yes"), model);
    append_row(tr("Comment"), QString::fromStdString(training.comment()), model);

    // Fit to size
    for (int i = 0; i < this->model()->columnCount(); ++i)
    {
        resizeColumnToContents(i);
    }
}