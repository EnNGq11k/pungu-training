QT += gui widgets charts

RC_ICONS = img/Pungu.ico

INCLUDEPATH += \
    /usr/include/libusb-1.0 \
    . -F/usr/include/aarch64-linux-gnu/qt6

LIBS += \
    -lz -lprotobuf -lusb-1.0

# Remove Wextra, because protobuf will generate warnings...
QMAKE_CXXFLAGS_WARN_ON -= -Wextra
QMAKE_CFLAGS_WARN_ON -= -Wextra