#ifndef PT_DATE_TIME_H
#define PT_DATE_TIME_H

#include <QDateTime>
#include <QString>

#include "../../pt_proto/pt_data.pb.h"

static const QString Q_DATE_TIME_FORMAT = "dd.MM.yyyy hh:mm:ss";
static const QString Q_DATE_FORMAT_WITH_WEEKDAY = "ddd dd.MM.yyyy";
static const QString Q_TIME_FORMAT = "hh:mm:ss";
static const QString Q_SORTABLE_DATE_FORMAT = "yyyyMMdd000000";

inline QDate date_time_to_qdate(const pt_data::date_time &date_time)
{
    return QDate(date_time.year(), date_time.month(), date_time.day());
}

inline QTime date_time_to_qtime(const pt_data::date_time &date_time)
{
    return QTime(date_time.hours(), date_time.minutes(), date_time.seconds(), 0);
}

inline QDateTime date_time_to_qdatetime(const pt_data::date_time &date_time)
{
    return QDateTime(date_time_to_qdate(date_time), date_time_to_qtime(date_time));
}

inline void date_time_to_qstring(const pt_data::date_time &date_time, const QString &format, QString &date_time_string)
{
    date_time_string = date_time_to_qdatetime(date_time).toString(format);
}

inline void duration_to_qstring(const pt_data::duration &duration, QString &date_time_string)
{
    QTime time(duration.hours(), duration.minutes(), duration.seconds(), 0);
    date_time_string = time.toString(Q_TIME_FORMAT);
}

inline qint64 date_time_to_msecs_since_epoch(const pt_data::date_time &date_time)
{
    QDateTime dt(QDate(date_time.year(), date_time.month(), date_time.day()),
                 QTime(date_time.hours(), date_time.minutes(), date_time.seconds(), 0));
    return dt.toMSecsSinceEpoch();
}

#endif