#ifndef PO_TRAININGCHART_H
#define PO_TRAININGCHART_H

#include <QWidget>

namespace pt_data
{
class training;
}

class training_charts;
class training_detail_list;

class training_view : public QWidget
{
    Q_OBJECT

  private:
    training_charts *charts;
    training_detail_list *details_view;

  public:
    training_view(QWidget *parent = 0);
    void load_data(const pt_data::training &training);

  private:
    void write_to_log(const QString &message) const;
};

#endif