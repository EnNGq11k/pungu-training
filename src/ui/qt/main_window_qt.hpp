#ifndef PO_MAINWINDOW_QT_H
#define PO_MAINWINDOW_QT_H

#include <QDialog>
#include <QFuture>
#include <atomic>

QT_BEGIN_NAMESPACE
class QVBoxLayout;
class QHBoxLayout;
class QItemSelection;
class QProgressDialog;
QT_END_NAMESPACE

class training_diary;

class main_window_qt : public QDialog
{
    Q_OBJECT

  private:
    training_diary *diary;

    QString data_path;

  public:
    main_window_qt();

  private:
    void load();
    void new_activity();
    void read_settings();
    void set_data_path();
    void change_data_path();
    void edit_bikes();

    void download();

    void write_to_log(const QString &message) const;

  private:
    virtual void closeEvent(QCloseEvent *event) override;
    virtual void keyPressEvent(QKeyEvent *event) override;
};

#endif