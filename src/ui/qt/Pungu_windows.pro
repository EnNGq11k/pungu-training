contains(QT_ARCH, i386) {
    message("32-bit not supported! Stop and re-build in x64 command line.")
} else {
    message("64-bit")
}

QMAKE_MSC_VER=1929
QT += charts
CONFIG += c++17

RC_ICONS = img/Pungu.ico

INCLUDEPATH += \
    ..\..\..\windows\packages\zlib-msvc-x64.1.2.11.8900\build\native\include \
    ..\..\..\windows\packages\libusb.1.0.21\src\include\libusb-1.0 \
    ..\..\..\windows\packages\protobuf-3.21.7\include

LIBS += \
    -L..\..\..\windows\packages\protobuf-3.21.7\lib -llibprotobuf \
    -L..\..\..\windows\packages\zlib-msvc-x64.1.2.11.8900\build\native\lib_release -lzlib \
    -L..\..\..\windows\packages\libusb.1.0.21\lib\native\x64 -llibusb-1.0

# Remove Wextra, because protobuf will generate warnings...
QMAKE_CXXFLAGS_WARN_ON -= -Wextra
QMAKE_CFLAGS_WARN_ON -= -Wextra

# QMAKE_CXXFLAGS += /NODEFAULTLIB:library
# QMAKE_CFLAGS += /NODEFAULTLIB:library

CONFIG += release
