#!/bin/sh
mkdir ../../../bin/Pungu.app/Contents/Frameworks/
mkdir ../../../bin/Pungu.app/Contents/MacOS/platforms/
mkdir ../../../bin/Pungu.app/Contents/MacOS/styles/

# qt
QTCORE=QtCore
QTGUI=QtGui
QTCHARTS=QtCharts
QTWIDGETS=QtWidgets
QTOPENGL=QtOpenGL
QTOPENGLWIDGETS=QtOpenGLWidgets
QTDBUS=QtDBus

## qtcore
FRAMEWORK=$QTCORE
cp -R /opt/homebrew/opt/qt/lib/$FRAMEWORK.framework/Versions/A/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks
install_name_tool -id @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK
install_name_tool -change /opt/homebrew/opt/qt/lib/$FRAMEWORK.framework/Versions/A/$FRAMEWORK @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/MacOS/Pungu

## qtdbus
FRAMEWORK=$QTDBUS
cp -R /opt/homebrew/opt/qt/lib/$FRAMEWORK.framework/Versions/A/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks
install_name_tool -id @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK
install_name_tool -change /opt/homebrew/opt/qt/lib/$FRAMEWORK.framework/Versions/A/$FRAMEWORK @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/MacOS/Pungu

install_name_tool -change @rpath/$QTCORE.framework/Versions/A/$QTCORE @executable_path/../Frameworks/$QTCORE ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK

## qtgui
FRAMEWORK=$QTGUI
cp -R /opt/homebrew/opt/qt/lib/$FRAMEWORK.framework/Versions/A/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks
install_name_tool -id @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK
install_name_tool -change /opt/homebrew/opt/qt/lib/$FRAMEWORK.framework/Versions/A/$FRAMEWORK @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/MacOS/Pungu

install_name_tool -change @rpath/$QTCORE.framework/Versions/A/$QTCORE @executable_path/../Frameworks/$QTCORE ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK
install_name_tool -change @rpath/$QTDBUS.framework/Versions/A/$QTDBUS @executable_path/../Frameworks/$QTDBUS ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK

## qtcharts
FRAMEWORK=$QTCHARTS
cp -R /opt/homebrew/opt/qt/lib/$FRAMEWORK.framework/Versions/A/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks
install_name_tool -id @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK
install_name_tool -change /opt/homebrew/opt/qt/lib/$FRAMEWORK.framework/Versions/A/$FRAMEWORK @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/MacOS/Pungu

install_name_tool -change @rpath/$QTCORE.framework/Versions/A/$QTCORE @executable_path/../Frameworks/$QTCORE ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK
install_name_tool -change @rpath/$QTGUI.framework/Versions/A/$QTGUI @executable_path/../Frameworks/$QTGUI ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK
install_name_tool -change @rpath/$QTOPENGL.framework/Versions/A/$QTOPENGL @executable_path/../Frameworks/$QTOPENGL ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK
install_name_tool -change @rpath/$QTWIDGETS.framework/Versions/A/$QTWIDGETS @executable_path/../Frameworks/$QTWIDGETS ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK
install_name_tool -change @rpath/$QTOPENGLWIDGETS.framework/Versions/A/$QTOPENGLWIDGETS @executable_path/../Frameworks/$QTOPENGLWIDGETS ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK

## qtwidgets
FRAMEWORK=$QTWIDGETS
cp -R /opt/homebrew/opt/qt/lib/$FRAMEWORK.framework/Versions/A/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks
install_name_tool -id @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK
install_name_tool -change /opt/homebrew/opt/qt/lib/$FRAMEWORK.framework/Versions/A/$FRAMEWORK @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/MacOS/Pungu

install_name_tool -change @rpath/$QTCORE.framework/Versions/A/$QTCORE @executable_path/../Frameworks/$QTCORE ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK
install_name_tool -change @rpath/$QTGUI.framework/Versions/A/$QTGUI @executable_path/../Frameworks/$QTGUI ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK

## qtopengl
FRAMEWORK=$QTOPENGL
cp -R /opt/homebrew/opt/qt/lib/$FRAMEWORK.framework/Versions/A/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks
install_name_tool -id @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK
install_name_tool -change /opt/homebrew/opt/qt/lib/$FRAMEWORK.framework/Versions/A/$FRAMEWORK @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/MacOS/Pungu

install_name_tool -change @rpath/$QTCORE.framework/Versions/A/$QTCORE @executable_path/../Frameworks/$QTCORE ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK
install_name_tool -change @rpath/$QTGUI.framework/Versions/A/$QTGUI @executable_path/../Frameworks/$QTGUI ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK

## qtopenglwidgets
FRAMEWORK=$QTOPENGLWIDGETS
cp -R /opt/homebrew/opt/qt/lib/$FRAMEWORK.framework/Versions/A/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks
install_name_tool -id @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK
install_name_tool -change /opt/homebrew/opt/qt/lib/$FRAMEWORK.framework/Versions/A/$FRAMEWORK @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/MacOS/Pungu

install_name_tool -change @rpath/$QTCORE.framework/Versions/A/$QTCORE @executable_path/../Frameworks/$QTCORE ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK
install_name_tool -change @rpath/$QTGUI.framework/Versions/A/$QTGUI @executable_path/../Frameworks/$QTGUI ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK
install_name_tool -change @rpath/$QTOPENGL.framework/Versions/A/$QTOPENGL @executable_path/../Frameworks/$QTOPENGL ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK
install_name_tool -change @rpath/$QTWIDGETS.framework/Versions/A/$QTWIDGETS @executable_path/../Frameworks/$QTWIDGETS ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK

# qt platform
FRAMEWORK=libqcocoa.dylib
cp -R /opt/homebrew/opt/qt/share/qt/plugins/platforms/$FRAMEWORK ../../../bin/Pungu.app/Contents/MacOS/platforms/
install_name_tool -id @executable_path/../MacOS/platforms/$FRAMEWORK ../../../bin/Pungu.app/Contents/MacOS/platforms/$FRAMEWORK

install_name_tool -change @rpath/$QTCORE.framework/Versions/A/$QTCORE @executable_path/../Frameworks/$QTCORE ../../../bin/Pungu.app/Contents/MacOS/platforms/$FRAMEWORK
install_name_tool -change @rpath/$QTGUI.framework/Versions/A/$QTGUI @executable_path/../Frameworks/$QTGUI ../../../bin/Pungu.app/Contents/MacOS/platforms/$FRAMEWORK

# qt minimal
LIBQMINIMAL=libqminimal.dylib
FRAMEWORK=$LIBQMINIMAL
cp -R /opt/homebrew/opt/qt/share/qt/plugins/platforms/$FRAMEWORK ../../../bin/Pungu.app/Contents/MacOS/platforms/
install_name_tool -id @executable_path/../MacOS/platforms/$FRAMEWORK ../../../bin/Pungu.app/Contents/MacOS/platforms/$FRAMEWORK

install_name_tool -change @rpath/$QTCORE.framework/Versions/A/$QTCORE @executable_path/../Frameworks/$QTCORE ../../../bin/Pungu.app/Contents/MacOS/platforms/$FRAMEWORK
install_name_tool -change @rpath/$QTGUI.framework/Versions/A/$QTGUI @executable_path/../Frameworks/$QTGUI ../../../bin/Pungu.app/Contents/MacOS/platforms/$FRAMEWORK

# qt offscreen
FRAMEWORK=libqoffscreen.dylib
cp -R /opt/homebrew/opt/qt/share/qt/plugins/platforms/$FRAMEWORK ../../../bin/Pungu.app/Contents/MacOS/platforms/
install_name_tool -id @executable_path/../MacOS/platforms/$FRAMEWORK ../../../bin/Pungu.app/Contents/MacOS/platforms/$FRAMEWORK

install_name_tool -change @rpath/$QTCORE.framework/Versions/A/$QTCORE @executable_path/../Frameworks/$QTCORE ../../../bin/Pungu.app/Contents/MacOS/platforms/$FRAMEWORK
install_name_tool -change @rpath/$QTGUI.framework/Versions/A/$QTGUI @executable_path/../Frameworks/$QTGUI ../../../bin/Pungu.app/Contents/MacOS/platforms/$FRAMEWORK

# qt macos style
FRAMEWORK=libqmacstyle.dylib
cp -R /opt/homebrew/opt/qt/share/qt/plugins/styles/$FRAMEWORK ../../../bin/Pungu.app/Contents/MacOS/styles/
install_name_tool -id @executable_path/../MacOS/styles/$FRAMEWORK ../../../bin/Pungu.app/Contents/MacOS/styles/$FRAMEWORK

install_name_tool -change @rpath/$QTCORE.framework/Versions/A/$QTCORE @executable_path/../Frameworks/$QTCORE ../../../bin/Pungu.app/Contents/MacOS/styles/$FRAMEWORK
install_name_tool -change @rpath/$QTGUI.framework/Versions/A/$QTGUI @executable_path/../Frameworks/$QTGUI ../../../bin/Pungu.app/Contents/MacOS/styles/$FRAMEWORK
install_name_tool -change @rpath/$QTWIDGETS.framework/Versions/A/$QTWIDGETS @executable_path/../Frameworks/$QTWIDGETS ../../../bin/Pungu.app/Contents/MacOS/styles/$FRAMEWORK

# protobuf
PROTOBUF=libprotobuf.32.dylib
PROTOBUFPATH=/opt/homebrew/opt/protobuf@21/lib/$PROTOBUF
FRAMEWORK=$PROTOBUF
cp -RL $PROTOBUFPATH ../../../bin/Pungu.app/Contents/Frameworks
install_name_tool -id @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK
install_name_tool -change $PROTOBUFPATH @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/MacOS/Pungu

# libusb
LIBUSB=libusb-1.0.0.dylib
LIBUSBPATH=/opt/homebrew/opt/libusb/lib/$LIBUSB
FRAMEWORK=$LIBUSB
cp -RL $LIBUSBPATH ../../../bin/Pungu.app/Contents/Frameworks
install_name_tool -id @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK
install_name_tool -change $LIBUSBPATH @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/MacOS/Pungu

# zlib
ZLIB=libz.1.dylib
ZLIBBPATH=/opt/homebrew/opt/zlib/lib/$ZLIB
FRAMEWORK=$ZLIB
cp -RL $ZLIBBPATH ../../../bin/Pungu.app/Contents/Frameworks
install_name_tool -id @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK
install_name_tool -change $ZLIBBPATH @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/MacOS/Pungu

# qtcore dependecies
FRAMEWORK=libicui18n.73.dylib
FRAMEWORKPATH=/opt/homebrew/opt/icu4c/lib/$FRAMEWORK
cp -RL $FRAMEWORKPATH ../../../bin/Pungu.app/Contents/Frameworks
install_name_tool -id @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK
install_name_tool -change $FRAMEWORKPATH @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$QTCORE

FRAMEWORK=libicuuc.73.dylib
FRAMEWORKPATH=/opt/homebrew/opt/icu4c/lib/$FRAMEWORK
cp -RL $FRAMEWORKPATH ../../../bin/Pungu.app/Contents/Frameworks
install_name_tool -id @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK
install_name_tool -change $FRAMEWORKPATH @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$QTCORE

FRAMEWORK=libicudata.73.dylib
FRAMEWORKPATH=/opt/homebrew/opt/icu4c/lib/$FRAMEWORK
cp -RL $FRAMEWORKPATH ../../../bin/Pungu.app/Contents/Frameworks
install_name_tool -id @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK
install_name_tool -change $FRAMEWORKPATH @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$QTCORE

FRAMEWORK=libdouble-conversion.3.dylib
FRAMEWORKPATH=/opt/homebrew/opt/double-conversion/lib/$FRAMEWORK
cp -RL $FRAMEWORKPATH ../../../bin/Pungu.app/Contents/Frameworks
install_name_tool -id @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK
install_name_tool -change $FRAMEWORKPATH @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$QTCORE

FRAMEWORK=libb2.1.dylib
FRAMEWORKPATH=/opt/homebrew/opt/libb2/lib/$FRAMEWORK
cp -RL $FRAMEWORKPATH ../../../bin/Pungu.app/Contents/Frameworks
install_name_tool -id @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK
install_name_tool -change $FRAMEWORKPATH @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$QTCORE

LIBGTHREAD=libgthread-2.0.0.dylib
FRAMEWORK=$LIBGTHREAD
FRAMEWORKPATH=/opt/homebrew/opt/glib/lib/$FRAMEWORK
cp -RL $FRAMEWORKPATH ../../../bin/Pungu.app/Contents/Frameworks
install_name_tool -id @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK
install_name_tool -change $FRAMEWORKPATH @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$QTCORE
install_name_tool -change $FRAMEWORKPATH @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$QTGUI

LIBHARFBUZZ=libharfbuzz.0.dylib
FRAMEWORK=$LIBHARFBUZZ
FRAMEWORKPATH=/opt/homebrew/opt/harfbuzz/lib/$FRAMEWORK
cp -RL $FRAMEWORKPATH ../../../bin/Pungu.app/Contents/Frameworks
install_name_tool -id @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK
install_name_tool -change $FRAMEWORKPATH @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$QTGUI

LIBGLIB=libglib-2.0.0.dylib
FRAMEWORK=$LIBGLIB
FRAMEWORKPATH=/opt/homebrew/opt/glib/lib/$FRAMEWORK
cp -RL $FRAMEWORKPATH ../../../bin/Pungu.app/Contents/Frameworks
install_name_tool -id @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK
install_name_tool -change $FRAMEWORKPATH @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$QTCORE
install_name_tool -change $FRAMEWORKPATH @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$QTGUI
install_name_tool -change $FRAMEWORKPATH @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$LIBHARFBUZZ

# ToDo: Get version from file
FRAMEWORKPATH=/opt/homebrew/Cellar/glib/2.78.0/lib/$FRAMEWORK
install_name_tool -change $FRAMEWORKPATH @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$LIBGTHREAD

FRAMEWORK=libpcre2-16.0.dylib
FRAMEWORKPATH=/opt/homebrew/opt/pcre2/lib/$FRAMEWORK
cp -RL $FRAMEWORKPATH ../../../bin/Pungu.app/Contents/Frameworks
install_name_tool -id @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK
install_name_tool -change $FRAMEWORKPATH @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$QTCORE
install_name_tool -change $FRAMEWORKPATH @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$LIBGLIB

FRAMEWORK=libpcre2-8.0.dylib
FRAMEWORKPATH=/opt/homebrew/opt/pcre2/lib/$FRAMEWORK
cp -RL $FRAMEWORKPATH ../../../bin/Pungu.app/Contents/Frameworks
install_name_tool -id @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK
install_name_tool -change $FRAMEWORKPATH @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$LIBGLIB

# qt dbus dependencies
FRAMEWORK=libdbus-1.3.dylib
FRAMEWORKPATH=/opt/homebrew/opt/dbus/lib/$FRAMEWORK
cp -RL $FRAMEWORKPATH ../../../bin/Pungu.app/Contents/Frameworks
install_name_tool -id @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK
install_name_tool -change $FRAMEWORKPATH @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$QTDBUS

# qt gui dependencies
FRAMEWORK=libmd4c.0.dylib
FRAMEWORKPATH=/opt/homebrew/opt/md4c/lib/$FRAMEWORK
cp -RL $FRAMEWORKPATH ../../../bin/Pungu.app/Contents/Frameworks
install_name_tool -id @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK
install_name_tool -change $FRAMEWORKPATH @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$QTGUI

LIBFREETYPE=libfreetype.6.dylib
FRAMEWORK=$LIBFREETYPE
FRAMEWORKPATH=/opt/homebrew/opt/freetype/lib/$FRAMEWORK
cp -RL $FRAMEWORKPATH ../../../bin/Pungu.app/Contents/Frameworks
install_name_tool -id @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK
install_name_tool -change $FRAMEWORKPATH @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$QTGUI
install_name_tool -change $FRAMEWORKPATH @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$LIBHARFBUZZ
install_name_tool -change $FRAMEWORKPATH @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/MacOS/platforms/$LIBQMINIMAL

FRAMEWORK=libpng16.16.dylib
FRAMEWORKPATH=/opt/homebrew/opt/libpng/lib/$FRAMEWORK
cp -RL $FRAMEWORKPATH ../../../bin/Pungu.app/Contents/Frameworks
install_name_tool -id @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK
install_name_tool -change $FRAMEWORKPATH @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$QTGUI

# libglib dependencies
FRAMEWORK=libpcre.1.dylib
FRAMEWORKPATH=/opt/homebrew/opt/pcre/lib/$FRAMEWORK
cp -RL $FRAMEWORKPATH ../../../bin/Pungu.app/Contents/Frameworks
install_name_tool -id @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK
install_name_tool -change $FRAMEWORKPATH @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$LIBGLIB

FRAMEWORK=libintl.8.dylib
FRAMEWORKPATH=/opt/homebrew/opt/gettext/lib/$FRAMEWORK
cp -RL $FRAMEWORKPATH ../../../bin/Pungu.app/Contents/Frameworks
install_name_tool -id @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK
install_name_tool -change $FRAMEWORKPATH @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$LIBGLIB

# libfreetype dependencies
FRAMEWORK=libpng16.16.dylib
FRAMEWORKPATH=/opt/homebrew/opt/libpng/lib/$FRAMEWORK
cp -RL $FRAMEWORKPATH ../../../bin/Pungu.app/Contents/Frameworks
install_name_tool -id @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK
install_name_tool -change $FRAMEWORKPATH @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$LIBFREETYPE

FRAMEWORK=libgraphite2.3.dylib
FRAMEWORKPATH=/opt/homebrew/opt/graphite2/lib/$FRAMEWORK
cp -RL $FRAMEWORKPATH ../../../bin/Pungu.app/Contents/Frameworks
install_name_tool -id @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$FRAMEWORK
install_name_tool -change $FRAMEWORKPATH @executable_path/../Frameworks/$FRAMEWORK ../../../bin/Pungu.app/Contents/Frameworks/$LIBHARFBUZZ

# re sign
codesign --force --deep --sign - ../../../bin/Pungu.app

# Check if we changed everything
for i in ../../../bin/Pungu.app/Contents/Frameworks/*; do echo $i && otool -L $i | grep opt ;done
for i in ../../../bin/Pungu.app/Contents/MacOS/platforms/*; do echo $i && otool -L $i | grep opt ;done
for i in ../../../bin/Pungu.app/Contents/MacOS/styles/*; do echo $i && otool -L $i | grep opt ;done

# copy qt conf
cp -RL ../../../macOS/qt.conf ../../../bin/Pungu.app/Contents/MacOS