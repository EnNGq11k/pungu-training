mkdir pt.iconset
sips -z 16 16     icon_512_osx.png --out pt.iconset/icon_16x16.png
sips -z 32 32     icon_512_osx.png --out pt.iconset/icon_16x16@2x.png
sips -z 32 32     icon_512_osx.png --out pt.iconset/icon_32x32.png
sips -z 64 64     icon_512_osx.png --out pt.iconset/icon_32x32@2x.png
sips -z 128 128   icon_512_osx.png --out pt.iconset/icon_128x128.png
sips -z 256 256   icon_512_osx.png --out pt.iconset/icon_128x128@2x.png
sips -z 256 256   icon_512_osx.png --out pt.iconset/icon_256x256.png
sips -z 512 512   icon_512_osx.png --out pt.iconset/icon_256x256@2x.png
sips -z 512 512   icon_512_osx.png --out pt.iconset/icon_512x512.png

cp icon_512_osx.png pt.iconset/icon_512x512@2x.png
iconutil -c icns pt.iconset
rm -R pt.iconset