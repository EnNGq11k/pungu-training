#include "polar_download.hpp"

#include <set>

#include <QLoggingCategory>

#include "../../usb/polar_usb.hpp"
#include "../../usb/polar_usb_v1.hpp"
#include "../../usb/polar_usb_v2.hpp"

void polar_download::download_from_devices(const device_check::polar_devices &available_devices)
{
    if (available_devices.v1_available)
    {
        emit update_text(tr("Downloading from device (v1)..."));

        download_v1();
    }

    emit update_min_max(0, 0);

    if (!cancelled && available_devices.v2_available)
    {
        emit update_text(tr("Downloading from device (v2)..."));

        download_v2(available_devices.v2_serial_number);
    }
}

void polar_download::cancel()
{
    cancelled = true;
}

void polar_download::download_v1()
{
    polar_usb_v1 v1;
    int result = v1.connect();
    if (result != 0)
    {
        write_to_log(tr("Failed to connect to v1 device: %1").arg(result));
        return;
    }

    start_download(v1);
}

void polar_download::download_v2(const std::string &serial_number)
{
    polar_usb_v2 v2;
    v2.serial_number = serial_number;
    int result = v2.connect();
    if (result != 0)
    {
        write_to_log(
            tr("Failed to connect to v2 device (%1): %2").arg(QString::fromStdString(serial_number)).arg(result));
        return;
    }

    start_download(v2);
}

void polar_download::start_download(const polar_usb &usb) const
{
    if (!cancelled)
    {
        std::vector<path_and_filename> files;
        write_to_log(tr("Get files for download"));
        get_files_to_download(usb, TRAINING_ROOT_DIR, files);

        write_to_log(tr("Found %1 files for download").arg(files.size()));
        emit update_min_max(0, (int)files.size());

        download_files(usb, data_path, files);
    }

    if (cancelled)
    {
        write_to_log(tr("Download cancelled"));
    }
}

void polar_download::get_files_to_download(const polar_usb &usb, const std::string &path,
                                           std::vector<path_and_filename> &files) const
{
    static const std::set<std::string> blocklist{
        "/U/0/AUTOS/",   "/U/0/DGOAL/",     "/U/0/FAV/",       "/U/0/FIXSPROF/",  "/U/0/NR/",         "/U/0/ONDEMAND/",
        "/U/0/PBEST/",   "/U/0/ROUTES/",    "/U/0/S/",         "/U/0/SLEEP/",     "/U/0/SPROF/",      "/U/0/TL/",
        "/U/0/DBDC.DAT", "/U/0/DHRBUF.DAT", "/U/0/DHRMAX.DAT", "/U/0/USERID.BPB", "/U/0/FAVCACHE.DAT"};

    if (!cancelled)
    {
        std::vector<std::string> files_and_directories;

        usb.get_files_and_directories(path, files_and_directories);

        for (auto &filename : files_and_directories)
        {
            if (cancelled)
            {
                return;
            }

            if (blocklist.find(path + filename) != blocklist.end())
            {
                continue;
            }

            if (filename.find("/") != std::string::npos)
            {
                get_files_to_download(usb, path + filename, files);
            }
            else
            {
                files.push_back(path_and_filename(path, filename));
            }
        }
    }
}

void polar_download::download_files(const polar_usb &usb, const std::string &output_path,
                                    const std::vector<path_and_filename> &files) const
{
    write_to_log(tr("Starting downloads"));
    for (size_t i = 0; i < files.size(); ++i)
    {
        if (!cancelled)
        {
            write_to_log(tr("Downloading %1").arg(QString::fromStdString(files[i].path + files[i].filename)));

            auto error = usb.download_file(output_path, files[i].path, files[i].filename);

            if (error != polar_usb::download_error::ok)
            {
                error == polar_usb::download_error::file_empty
                    ? write_to_log(tr("Download of %1 failed. File empty.")
                                       .arg(QString::fromStdString(files[i].path + files[i].filename)))
                    : write_to_log(
                          tr("Download of %1 failed").arg(QString::fromStdString(files[i].path + files[i].filename)));
            }

            emit update_progress((int)i);
        }
    }

    write_to_log(tr("Download finished"));
}

void polar_download::write_to_log(const QString &message) const
{
    static QLoggingCategory category("Downloader");
    qCInfo(category) << message;
}