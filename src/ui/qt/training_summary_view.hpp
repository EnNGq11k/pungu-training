#ifndef PO_TRAINING_SUMMARY_VIEW_H
#define PO_TRAINING_SUMMARY_VIEW_H

#include <QStandardItem>
#include <QWidget>

#include "../../pt_proto/pt_data.pb.h"

QT_BEGIN_NAMESPACE
class QTreeView;
class QStandardItemModel;
QT_END_NAMESPACE

namespace pt_data
{
class training_bikes;
}

class running_index_chart;
class training_summary_detail;

class training_summary_view : public QWidget
{
    Q_OBJECT

  private:
    training_summary_detail *details_view;
    running_index_chart *chart_widget;

  public:
    training_summary_view(QWidget *parent = 0);
    void update(const std::map<long long, pt_data::training> &training_data, const uint &year,
                const pt_data::training_bikes &bikes);

  private:
    void write_to_log(const QString &message) const;
};

#endif