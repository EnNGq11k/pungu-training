#include <qapplication.h>

#include "main_window_qt.hpp"
#include <QTranslator>

QT_USE_NAMESPACE

int main(int argc, char *argv[])
{
    Q_INIT_RESOURCE(i18n);

    QTranslator translator;
    QApplication a(argc, argv);
    // https://doc.qt.io/qt-6/qtlinguist-hellotr-example.html

    QString langPath = ":/translations/pungu_";
    if (!translator.load(langPath + QLocale::system().name()))
    {
        if (!translator.load(langPath + "en_US"))
        {
            return 1;
        }
    }

    a.installTranslator(&translator);
    main_window_qt main_window;
    main_window.show();

    return a.exec();
}