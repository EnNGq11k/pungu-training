#ifndef PO_RUNNING_INDEX_CHART_H
#define PO_RUNNING_INDEX_CHART_H

#include "../../pt_proto/pt_data.pb.h"
#include <QWidget>

class QLineSeries;
class QAbstractAxis;
class QDateTimeAxis;

class running_index_chart : public QWidget
{
    template <class T> void set_min_max(const T &value, T &min, T &max)
    {
        if (value < min)
        {
            min = value;
        }
        if (value > max)
        {
            max = value;
        }
    }

    template <class T> bool contains_valid_data(const std::vector<T> data)
    {
        for (auto &d : data)
        {
            if (d > 1)
            {
                return true;
            }
        }

        return false;
    }

    Q_OBJECT

  private:
    QWidget *current_view;

  public:
    running_index_chart(QWidget *parent = 0);
    void load_running_index(const std::map<long long, pt_data::training> &training_data, const uint &year);

  private:
    void add_chart_view(std::vector<QLineSeries *> &hr_power_series, QAbstractAxis *y_axis);
    void set_axis_style(QDateTimeAxis *x_axis, QAbstractAxis *y_axis);
    void show_tooltip(QPointF point, bool state);
    void write_to_log(const QString &message) const;
};

#endif