#ifndef PO_ACTIVITY_TRANSLATION_H
#define PO_ACTIVITY_TRANSLATION_H

#include <QObject>
#include <map>

#include "../../pt_proto/pt_data.pb.h"
class activity_type_translation : QObject
{
    Q_OBJECT

  public:
    static const std::map<pt_data::activity_type, QString> &get_all_activities()
    {
        static std::map<pt_data::activity_type, QString> activities{
            {pt_data::activity_type::Running, tr("Running")},
            {pt_data::activity_type::Cycling, tr("Cycling")},
            {pt_data::activity_type::Walking, tr("Walking")},
            {pt_data::activity_type::Jogging, tr("Jogging")},
            {pt_data::activity_type::MountainBiking, tr("Mountain biking")},
            {pt_data::activity_type::Skiing, tr("Skiing")},
            {pt_data::activity_type::DownhillSkiing, tr("Downhill skiing")},
            {pt_data::activity_type::Rowing, tr("Rowing")},
            {pt_data::activity_type::NordicWalking, tr("Nordic walking")},
            {pt_data::activity_type::Skating, tr("Skating")},
            {pt_data::activity_type::Hiking, tr("Hiking")},
            {pt_data::activity_type::Tennis, tr("Tennis")},
            {pt_data::activity_type::Squash, tr("Squash")},
            {pt_data::activity_type::Badminton, tr("Badminton")},
            {pt_data::activity_type::StrengthTraining, tr("Strength training")},
            {pt_data::activity_type::Outdoor, tr("Other outdoor")},
            {pt_data::activity_type::TreadmillRunning, tr("Running (Treadmill)")},
            {pt_data::activity_type::BikeIndoor, tr("Cycling (Indoor)")},
            {pt_data::activity_type::RoadRunning, tr("Running (Road)")},
            {pt_data::activity_type::CircuitTraining, tr("Circuit training")},
            {pt_data::activity_type::Snowboarding, tr("Snowboarding")},
            {pt_data::activity_type::Swim_Pool, tr("Swimming (Pool)")},
            {pt_data::activity_type::FreestyleXCSkiing, tr("Freestyle XC skiing")},
            {pt_data::activity_type::ClassicXCSkiing, tr("Classic XC skiing")},
            {pt_data::activity_type::TrailRunning, tr("Running (Trail)")},
            {pt_data::activity_type::IceSkating, tr("Ice skating")},
            {pt_data::activity_type::InlineSkating, tr("Inline skating")},
            {pt_data::activity_type::RollerSkating, tr("Roller skating")},
            {pt_data::activity_type::GroupExercise, tr("Group exercise")},
            {pt_data::activity_type::Yoga, tr("Yoga")},
            {pt_data::activity_type::Crossfit, tr("Crossfit")},
            {pt_data::activity_type::Golf, tr("Golf")},
            {pt_data::activity_type::TrackFieldRunning, tr("Running (Track & field)")},
            {pt_data::activity_type::BikeRoad, tr("Cycling (Road)")},
            {pt_data::activity_type::Soccer, tr("Soccer")},
            {pt_data::activity_type::Cricket, tr("Cricket")},
            {pt_data::activity_type::Basketball, tr("Basketball")},
            {pt_data::activity_type::Baseball, tr("Baseball")},
            {pt_data::activity_type::Rugby, tr("Rugby")},
            {pt_data::activity_type::FieldHockey, tr("Field hockey")},
            {pt_data::activity_type::Volleyball, tr("Volleyball")},
            {pt_data::activity_type::IceHockey, tr("Ice hockey")},
            {pt_data::activity_type::Football, tr("Football")},
            {pt_data::activity_type::Handball, tr("Handball")},
            {pt_data::activity_type::BeachVolley, tr("Beach volley")},
            {pt_data::activity_type::Futsal, tr("Futsal")},
            {pt_data::activity_type::Floorball, tr("Floorball")},
            {pt_data::activity_type::Dancing, tr("Dancing")},
            {pt_data::activity_type::Trotting, tr("Trotting")},
            {pt_data::activity_type::Riding, tr("Riding")},
            {pt_data::activity_type::CrossTrainer, tr("Cross-trainer")},
            {pt_data::activity_type::FitnessMartialArts, tr("Fitness martial arts")},
            {pt_data::activity_type::FunctionalTraining, tr("Functional training")},
            {pt_data::activity_type::Bootcamp, tr("Bootcamp")},
            {pt_data::activity_type::FreestyleRollerSkiing, tr("Freestyle roller skiing")},
            {pt_data::activity_type::ClassicRollerSkiing, tr("Classic roller skiing")},
            {pt_data::activity_type::Aerobics, tr("Aerobics")},
            {pt_data::activity_type::AquaFitness, tr("Aqua fitness")},
            {pt_data::activity_type::StepWorkout, tr("Step workout")},
            {pt_data::activity_type::BodyMind, tr("Body & Mind")},
            {pt_data::activity_type::Pilates, tr("Pilates")},
            {pt_data::activity_type::Stretching, tr("Stretching")},
            {pt_data::activity_type::FitnessDancing, tr("Fitness dancing")},
            {pt_data::activity_type::TriathlonOld, tr("Triathlon")},
            {pt_data::activity_type::Duathlon, tr("Duathlon")},
            {pt_data::activity_type::OffRoadTriathlon, tr("Triathlon (Off-road)")},
            {pt_data::activity_type::OffRoadDuathlon, tr("Duathlon (Off-road)")},
            {pt_data::activity_type::TriathlonNew, tr("Triathlon")},
            {pt_data::activity_type::Multisport, tr("Multisport")},
            {pt_data::activity_type::Indoor, tr("Other indoor")},
            {pt_data::activity_type::ClimbingIndoor, tr("Climbing (Indoor)")},
            {pt_data::activity_type::SwimOpenWater, tr("Swimming (Open water)")},
            {pt_data::activity_type::Core, tr("Core-Training")},
            {pt_data::activity_type::ClimbingOutdoor, tr("Climbing (Outdoor)")},
            {pt_data::activity_type::Regeneration, tr("Regeneration")},
            {pt_data::activity_type::SwimCords, tr("Swim (Cords)")},
            {pt_data::activity_type::ImportedBike, tr("Bike (Imported)")},
            {pt_data::activity_type::ImportedRunning, tr("Run (Imported)")},
            {pt_data::activity_type::ImportedSwim, tr("Swim (Imported)")},
            {pt_data::activity_type::Note, tr("Note")},
            {pt_data::activity_type::Sick, tr("Sick")}};

        return activities;
    }

    static QString translate(const pt_data::activity_type &activity_type)
    {
        auto translation = get_all_activities().find(activity_type);

        if (translation != get_all_activities().end())
        {
            return translation->second;
        }

        return tr("Unknown %1").arg((int)activity_type);
    }

    static const std::map<pt_data::activity_type, QString> &get_all_activities_emoji()
    {
        static std::map<pt_data::activity_type, QString> activities{
            {pt_data::activity_type::Running, "🏃"},
            {pt_data::activity_type::Cycling, "🚲"},
            {pt_data::activity_type::Walking, "🚶"},
            {pt_data::activity_type::Jogging, "🏃"},
            {pt_data::activity_type::MountainBiking, "🚵"},
            {pt_data::activity_type::Skiing, "⛷️"},
            {pt_data::activity_type::DownhillSkiing, "⛷️"},
            {pt_data::activity_type::Rowing, "🚣‍♀️"},
            {pt_data::activity_type::NordicWalking, "🚶"},
            {pt_data::activity_type::Skating, "🛼"},
            {pt_data::activity_type::Hiking, "🥾"},
            {pt_data::activity_type::Tennis, "🎾"},
            {pt_data::activity_type::Squash, "☄️"},
            {pt_data::activity_type::Badminton, "🏸"},
            {pt_data::activity_type::StrengthTraining, "🏋️‍♀️"},
            {pt_data::activity_type::Outdoor, "⛰️"},
            {pt_data::activity_type::TreadmillRunning, "🏃"},
            {pt_data::activity_type::BikeIndoor, "🚴"},
            {pt_data::activity_type::RoadRunning, "🏃"},
            {pt_data::activity_type::CircuitTraining, "💪"},
            {pt_data::activity_type::Snowboarding, "🏂"},
            {pt_data::activity_type::Swim_Pool, "🏊"},
            {pt_data::activity_type::FreestyleXCSkiing, "⛷️"},
            {pt_data::activity_type::ClassicXCSkiing, "⛷️"},
            {pt_data::activity_type::TrailRunning, "🏃"},
            {pt_data::activity_type::IceSkating, "⛸️"},
            {pt_data::activity_type::InlineSkating, "🛼"},
            {pt_data::activity_type::RollerSkating, "🛼"},
            {pt_data::activity_type::GroupExercise, "👨‍👩‍👧‍👦"},
            {pt_data::activity_type::Yoga, "🧘"},
            {pt_data::activity_type::Crossfit, "💪"},
            {pt_data::activity_type::Golf, "🏌️"},
            {pt_data::activity_type::TrackFieldRunning, "🏃"},
            {pt_data::activity_type::BikeRoad, "🚴"},
            {pt_data::activity_type::Soccer, "⚽️"},
            {pt_data::activity_type::Cricket, "🏏"},
            {pt_data::activity_type::Basketball, "⛹️"},
            {pt_data::activity_type::Baseball, "⚾️"},
            {pt_data::activity_type::Rugby, "🏉"},
            {pt_data::activity_type::FieldHockey, "🏑"},
            {pt_data::activity_type::Volleyball, "🏐"},
            {pt_data::activity_type::IceHockey, "🏒"},
            {pt_data::activity_type::Football, "🏈"},
            {pt_data::activity_type::Handball, "🤾"},
            {pt_data::activity_type::BeachVolley, "🏐🏐"},
            {pt_data::activity_type::Futsal, "⚽️"},
            {pt_data::activity_type::Floorball, "🏑"},
            {pt_data::activity_type::Dancing, "🪩"},
            {pt_data::activity_type::Trotting, "🐎"},
            {pt_data::activity_type::Riding, "🏇"},
            {pt_data::activity_type::CrossTrainer, "💪"},
            {pt_data::activity_type::FitnessMartialArts, "🥷"},
            {pt_data::activity_type::FunctionalTraining, "👏"},
            {pt_data::activity_type::Bootcamp, "⛺️"},
            {pt_data::activity_type::FreestyleRollerSkiing, "🛼"},
            {pt_data::activity_type::ClassicRollerSkiing, "🛼"},
            {pt_data::activity_type::Aerobics, "👏"},
            {pt_data::activity_type::AquaFitness, "🐬"},
            {pt_data::activity_type::StepWorkout, "👏"},
            {pt_data::activity_type::BodyMind, "🧘"},
            {pt_data::activity_type::Pilates, "🧘"},
            {pt_data::activity_type::Stretching, "🙌"},
            {pt_data::activity_type::FitnessDancing, "🪩"},
            {pt_data::activity_type::TriathlonOld, "🎽"},
            {pt_data::activity_type::Duathlon, "🎽"},
            {pt_data::activity_type::OffRoadTriathlon, "🎽"},
            {pt_data::activity_type::OffRoadDuathlon, "🎽"},
            {pt_data::activity_type::TriathlonNew, "🎽"},
            {pt_data::activity_type::Multisport, "🎽"},
            {pt_data::activity_type::Indoor, "🏠"},
            {pt_data::activity_type::ClimbingIndoor, "🧗"},
            {pt_data::activity_type::SwimOpenWater, "🏊"},
            {pt_data::activity_type::Core, "💪"},
            {pt_data::activity_type::ClimbingOutdoor, "🧗"},
            {pt_data::activity_type::Regeneration, "💆"},
            {pt_data::activity_type::SwimCords, "🤲"},
            {pt_data::activity_type::ImportedBike, "🚴"},
            {pt_data::activity_type::ImportedRunning, "🏃"},
            {pt_data::activity_type::ImportedSwim, "🏊"},
            {pt_data::activity_type::Note, "📝"},
            {pt_data::activity_type::Sick, "🤕"}};

        return activities;
    }

    static QString translate_to_emoji(const pt_data::activity_type &activity_type)
    {
        auto translation = get_all_activities_emoji().find(activity_type);

        if (translation != get_all_activities_emoji().end())
        {
            return translation->second;
        }

        return "💪";
    }
};
#endif