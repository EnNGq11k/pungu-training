#include "training_view.hpp"

#include <QtWidgets>

#include "training_charts.hpp"
#include "training_detail_list.hpp"

training_view::training_view(QWidget *parent)
    : QWidget(parent), charts(new training_charts), details_view(new training_detail_list)
{
    auto main_layout = new QVBoxLayout(this);
    main_layout->setContentsMargins(0, 5, 0, 0);

    auto chart_widget = new QWidget();
    auto chart_layout = new QVBoxLayout();
    chart_layout->setContentsMargins(0, 0, 0, 0);
    chart_layout->addWidget(charts);
    chart_widget->setLayout(chart_layout);

    auto details_widget = new QWidget();
    auto details_layout = new QVBoxLayout();
    details_layout->setContentsMargins(0, 0, 0, 0);
    details_layout->addWidget(details_view);
    details_widget->setLayout(details_layout);

    details_view->setSelectionMode(QAbstractItemView::SelectionMode::NoSelection);
    details_view->setEditTriggers(QAbstractItemView::NoEditTriggers);

    auto tab_view = new QTabWidget;
    tab_view->addTab(details_widget, tr("&Details"));
    tab_view->addTab(chart_widget, tr("&Graphs"));
    main_layout->addWidget(tab_view);
}

void training_view::load_data(const pt_data::training &training)
{
    details_view->load(training);
    charts->load(training);
}

void training_view::write_to_log(const QString &message) const
{
    static QLoggingCategory category("Charts");
    qCInfo(category) << message;
}