#ifndef PO_POLAR_DOWNLOAD_H
#define PO_POLAR_DOWNLOAD_H

#include <QObject>

#include "../../usb/device_check.hpp"

class polar_usb;

class polar_download : public QObject
{
  private:
    struct path_and_filename
    {
        path_and_filename(std::string path, std::string filename) : path(path), filename(filename)
        {
        }

        std::string path;
        std::string filename;
    };

    Q_OBJECT

  private:
    std::string data_path;
    std::atomic<bool> cancelled;

  public:
    polar_download(std::string data_path) : data_path(data_path), cancelled(false)
    {
    }

    ~polar_download() = default;

    void download_from_devices(const device_check::polar_devices &available_devices);
    void cancel();

  signals:
    void update_min_max(const int &min, const int &max) const;
    void update_progress(const int &i) const;
    void update_text(const QString &s) const;

  private:
    void download_v1();
    void download_v2(const std::string &serial_number);
    void start_download(const polar_usb &usb) const;
    void get_files_to_download(const polar_usb &usb, const std::string &path,
                               std::vector<path_and_filename> &files) const;
    void download_files(const polar_usb &usb, const std::string &output_path,
                        const std::vector<path_and_filename> &files) const;

    void write_to_log(const QString &message) const;
};

#endif