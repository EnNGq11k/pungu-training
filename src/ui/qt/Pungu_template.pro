HEADERS += \
    main_window_qt.hpp \
    ../../training_data/training_path_parser.hpp ../../training_data/time_extensions.hpp ../../training_data/duration.hpp ../../training_data/training_cache.hpp\
    ../../usb/polar_usb.hpp ../../usb/polar_usb_v1.hpp ../../usb/polar_usb_v2.hpp ../../usb/backend/serial_usb.hpp ../../usb/backend/native_usb.hpp ../../usb/device_check.hpp\
    ../../usb/backend/rs232.h \
    running_index_chart.hpp \
    logging/logger.hpp \
    polar_download.hpp \
    training_view.hpp \
    training_list.hpp \
    training_diary.hpp \
    training_charts.hpp \
    training_calendar.hpp \
    training_detail_list.hpp \
    activity_translation.hpp \
    training_summary_view.hpp \
    training_summary_detail.hpp \
    date_time.hpp \
    ../../pt_proto/pt_data_version.hpp \
    dialogs/new_activity_dialog.hpp dialogs/edit_activity_dialog.hpp dialogs/edit_bikes_dialog.hpp

SOURCES += \
    pungu_qt.cpp main_window_qt.cpp \
    ../../usb/polar_usb.cpp ../../usb/polar_usb_v1.cpp ../../usb/polar_usb_v2.cpp ../../usb/backend/serial_usb.cpp ../../usb/backend/native_usb.cpp ../../usb/device_check.cpp \
    ../../usb/backend/rs232.c ../../usb/backend/osx/rawhid_api.c ../../usb/backend/osx/rs232_osx.c \
    running_index_chart.cpp \
    logging/logger.cpp \
    training_view.cpp \
    polar_download.cpp \
    training_diary.cpp \
    training_list.cpp \
    training_charts.cpp \
    training_calendar.cpp \
    training_detail_list.cpp \
    training_summary_view.cpp \
    training_summary_detail.cpp \
    dialogs/new_activity_dialog.cpp dialogs/edit_activity_dialog.cpp dialogs/edit_bikes_dialog.cpp \
    ../../training_data/training_parser.cpp ../../training_data/training_saver.cpp \
    ../../proto/training_session.pb.cc ../../proto/exercise_route.pb.cc ../../proto/types.pb.cc ../../proto/structures.pb.cc ../../proto/exercise_samples.pb.cc ../../proto/exercise_rr_samples.pb.cc ../../proto/exercise_base.pb.cc ../../proto/exercise_stats.pb.cc ../../proto/pftp_request.pb.cc ../../proto/pftp_response.pb.cc \
    ../../pt_proto/pt_data.pb.cc ../../pt_proto/pt_bikes.pb.cc\
    ../../zip/unpack.cpp

RESOURCES    += i18n.qrc
TRANSLATIONS += translations/pungu_de_DE.ts \
                translations/pungu_en_US.ts

OBJECTS_DIR = ../../../bin/obj
DESTDIR = ../../../bin

VERSION = 0.4.0
DEFINES += APP_VERSION=\\\"$$VERSION\\\"

CONFIG += release

# Keep newline :)
