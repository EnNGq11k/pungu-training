#include "running_index_chart.hpp"

#include <QDateTimeAxis>
#include <QValueAxis>
#include <QtCharts/QChartView>
#include <QtCharts/QSplineSeries>
#include <QtWidgets>

#include "date_time.hpp"

running_index_chart::running_index_chart(QWidget *parent) : QWidget(parent), current_view(0)
{
    auto main_layout = new QVBoxLayout(this);
    main_layout->setContentsMargins(0, 0, 0, 0);
}

void running_index_chart::load_running_index(const std::map<long long, pt_data::training> &training_data,
                                             const uint &year)
{
    QLineSeries *ri_series = new QLineSeries();
    ri_series->setName("Running index");

    int min = std::numeric_limits<int>().max();
    int max = std::numeric_limits<int>().min();

    if (training_data.size() > 0)
    {
        for (auto &training_pair : training_data)
        {
            auto training = &training_pair.second;

            if (training->running_index() > 0 && (year == 0 || training->date_time().year() == year))
            {
                qint64 current_time_ms = date_time_to_msecs_since_epoch(training->date_time());
                ri_series->append(current_time_ms, training->running_index());
                set_min_max((int)training->running_index(), min, max);
            }
        }
    }

    QWidget *new_view;

    if (ri_series->count() > 0)
    {
        auto chart = new QChart();
        chart->setContentsMargins(0, 0, 0, 0);
        chart->setAcceptHoverEvents(true);
        chart->addSeries(ri_series);

        auto chart_view = new QChartView(chart);
        chart_view->setRenderHint(QPainter::Antialiasing);
        chart_view->setContentsMargins(0, 0, 0, 0);
        new_view = chart_view;

        auto *x_axis = new QDateTimeAxis;
        x_axis->setTruncateLabels(false);
        x_axis->setFormat("dd.MM");
        auto x_font = x_axis->labelsFont();
        x_font.setPointSize(x_font.pointSize() - 3);
        x_axis->setLabelsFont(x_font);

        auto *y_axis = new QValueAxis;
        y_axis->setLabelFormat("%i");
        y_axis->setMin(min - 5);
        y_axis->setMax(max + 5);

        chart->addAxis(x_axis, Qt::AlignBottom);
        chart->addAxis(y_axis, Qt::AlignLeft);
        ri_series->attachAxis(x_axis);
        ri_series->attachAxis(y_axis);

        connect(ri_series, &QLineSeries::hovered, this, &running_index_chart::show_tooltip);
    }
    else
    {
        delete ri_series;
        auto group_box = new QGroupBox;
        auto layout = new QGridLayout;
        auto no_data_label = new QLabel(tr("No running index data available"));
        layout->addWidget(no_data_label, 0, 0, Qt::AlignCenter);
        group_box->setLayout(layout);
        new_view = group_box;
    }

    if (current_view != nullptr)
    {
        this->layout()->replaceWidget(current_view, new_view);
        current_view->deleteLater();
    }
    else
    {
        this->layout()->addWidget(new_view);
    }

    current_view = new_view;
}

void running_index_chart::set_axis_style(QDateTimeAxis *x_axis, QAbstractAxis *y_axis)
{
    x_axis->setTickCount(10);
    x_axis->setFormat("hh:mm:ss");
    auto x_font = x_axis->labelsFont();
    x_font.setPointSize(x_font.pointSize() - 3);
    x_axis->setLabelsFont(x_font);

    y_axis->setTruncateLabels(false);
    auto y_font = y_axis->labelsFont();
    y_font.setPointSize(y_font.pointSize() - 1);
    y_axis->setLabelsFont(y_font);
}

void running_index_chart::show_tooltip(QPointF point, bool /*state*/)
{
    QString tooltip;
    tooltip.append(tr("%1").arg(point.y()));
    auto pos = QCursor::pos();
    pos.setY(pos.y() - 50);
    QToolTip::showText(pos, tooltip);
}

void running_index_chart::write_to_log(const QString &message) const
{
    static QLoggingCategory category("Charts");
    qCInfo(category) << message;
}