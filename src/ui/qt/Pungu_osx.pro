QT += charts

ICON = img/pt.icns

INCLUDEPATH += \
    /opt/homebrew/opt/protobuf@21/include \
    /opt/homebrew/include/libusb-1.0 \
    /opt/homebrew/opt/zlib/include \
    /opt/homebrew/include

LIBS += \
    -L/opt/homebrew/opt/protobuf@21/lib \
    -L/opt/homebrew/lib/ -lz -lprotobuf -lusb-1.0 \
    -L/opt/homebrew/opt/zlib/lib -lz

# Remove Wextra, because protobuf will generate warnings...
QMAKE_CXXFLAGS_WARN_ON -= -Wextra
QMAKE_CFLAGS_WARN_ON -= -Wextra

# QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.15