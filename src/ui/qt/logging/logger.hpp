// https://andydunkel.net/2017/11/08/qt_log_file_rotation_with_qdebug/

#ifndef PO_LOGGER_H
#define PO_LOGGER_H

#define LOGSIZE 1024 * 100 // log size in bytes
#define LOGFILES 5

#include <QDate>
#include <QDebug>
#include <QObject>
#include <QString>
#include <QTime>

namespace logger
{
static QString log_folder_name;
static QString log_file_name;

bool init_logging(const QString &data_path);
void message_handler(QtMsgType type, const QMessageLogContext &context, const QString &msg);
}; // namespace logger

#endif // PO_LOGGER_H
