#include "logger.hpp"

#include <QDebug>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QFileInfoList>
#include <QTime>

namespace logger
{

QString mesage_type_to_string(const QtMsgType &type)
{
    switch (type)
    {
    case QtMsgType::QtDebugMsg:
        return "DEBUG";

    case QtMsgType::QtWarningMsg:
        return "WARNING";

    case QtMsgType::QtCriticalMsg:
        return "CRITICAL";

    case QtMsgType::QtFatalMsg:
        return "FATAL";

    case QtMsgType::QtInfoMsg:
    default:
        return "INFO";
    }
}

void init_log_file_name()
{
    log_file_name = QString(log_folder_name + "/Log_%1__%2.log")
                        .arg(QDate::currentDate().toString("yyyy_MM_dd"))
                        .arg(QTime::currentTime().toString("hh_mm_ss_zzz"));
}

/**
 * @brief deletes old log files, only the last ones are kept
 */
void delete_old_logs()
{
    QDir dir;
    dir.setFilter(QDir::Files | QDir::Hidden | QDir::NoSymLinks);
    dir.setSorting(QDir::Time | QDir::Reversed);
    dir.setPath(log_folder_name);

    QFileInfoList list = dir.entryInfoList();
    if (list.size() <= LOGFILES)
    {
        return; // no files to delete
    }
    else
    {
        for (int i = 0; i < (list.size() - LOGFILES); i++)
        {
            QString path = list.at(i).absoluteFilePath();
            QFile file(path);
            file.remove();
        }
    }
}

bool init_logging(const QString &data_path)
{
    log_folder_name = data_path + "/logs";

    // Create folder for logfiles if not exists
    if (!QDir(log_folder_name).exists())
    {
        QDir().mkdir(log_folder_name);
    }

    delete_old_logs();    // delete old log files
    init_log_file_name(); // create the logfile name

    QFile out_file(log_file_name);
    if (out_file.open(QIODevice::WriteOnly | QIODevice::Append))
    {
        qInstallMessageHandler(logger::message_handler);
        return true;
    }
    else
    {
        return false;
    }
}

void message_handler(QtMsgType type, const QMessageLogContext &context, const QString &txt)
{
    // check file size and if needed create new log!
    {
        QFile out_file_check(log_file_name);
        int size = out_file_check.size();

        if (size > LOGSIZE) // check current log size
        {
            delete_old_logs();
            init_log_file_name();
        }
    }

    QFile out_file(log_file_name);
    out_file.open(QIODevice::WriteOnly | QIODevice::Append);
    QTextStream ts(&out_file);

    QString now = QDateTime::currentDateTime().toString("dd.MM.yyyy HH:mm:ss");
    QString log_type = mesage_type_to_string(type);
    ts << now << "\t" << log_type << "\t" << context.category << "\t" << txt << Qt::endl;
}
} // namespace logger