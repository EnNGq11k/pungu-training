<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>activity_type_translation</name>
    <message>
        <location filename="../activity_translation.hpp" line="16"/>
        <source>Running</source>
        <translation>Laufen</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="17"/>
        <source>Cycling</source>
        <translation>Rad</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="18"/>
        <source>Walking</source>
        <translation>Spazieren</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="19"/>
        <source>Jogging</source>
        <translation>Joggen</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="20"/>
        <source>Mountain biking</source>
        <translation>Mountain bike</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="21"/>
        <source>Skiing</source>
        <translation>Ski</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="22"/>
        <source>Downhill skiing</source>
        <translation>Downhill Ski</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="23"/>
        <source>Rowing</source>
        <translation>Rudern</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="24"/>
        <source>Nordic walking</source>
        <translation>Nordic Walking</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="25"/>
        <source>Skating</source>
        <translation>Eislaufen</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="26"/>
        <source>Hiking</source>
        <translation>Wandern</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="27"/>
        <source>Tennis</source>
        <translation>Tennis</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="28"/>
        <source>Squash</source>
        <translation>Squash</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="29"/>
        <source>Badminton</source>
        <translation>Badminton</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="30"/>
        <source>Strength training</source>
        <translation>Krafttraining</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="31"/>
        <source>Other outdoor</source>
        <translation>Sonstige Outdoor-Sportart</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="32"/>
        <source>Running (Treadmill)</source>
        <translation>Laufen (Band)</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="33"/>
        <source>Cycling (Indoor)</source>
        <translation>Rad (Rolle)</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="34"/>
        <source>Running (Road)</source>
        <translation>Laufen (Straße)</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="35"/>
        <source>Circuit training</source>
        <translation>Zirkeltraining</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="36"/>
        <source>Snowboarding</source>
        <translation>Snwoboard</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="37"/>
        <source>Swimming (Pool)</source>
        <translation>Schwimmen (Halle)</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="38"/>
        <source>Freestyle XC skiing</source>
        <translation>Freestyle XC Ski</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="39"/>
        <source>Classic XC skiing</source>
        <translation>Classic XC Ski</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="40"/>
        <source>Running (Trail)</source>
        <translation>Laufen (Trail)</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="41"/>
        <source>Ice skating</source>
        <translation>Schlittschuhlaufen</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="42"/>
        <source>Inline skating</source>
        <translation>Inlineskates</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="43"/>
        <source>Roller skating</source>
        <translation>Rollerskates</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="44"/>
        <source>Group exercise</source>
        <translation>Gruppenübung</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="45"/>
        <source>Yoga</source>
        <translation>Yoga</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="46"/>
        <source>Crossfit</source>
        <translation>Crossfit</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="47"/>
        <source>Golf</source>
        <translation>Golf</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="48"/>
        <source>Running (Track &amp; field)</source>
        <translation>Laufen (Bahn)</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="49"/>
        <source>Cycling (Road)</source>
        <translation>Rad (Straße)</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="50"/>
        <source>Soccer</source>
        <translation>Fußball</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="51"/>
        <source>Cricket</source>
        <translation>Cricket</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="52"/>
        <source>Basketball</source>
        <translation>Baketball</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="53"/>
        <source>Baseball</source>
        <translation>Baseball</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="54"/>
        <source>Rugby</source>
        <translation>Rugby</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="55"/>
        <source>Field hockey</source>
        <translation>Feldhockey</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="56"/>
        <source>Volleyball</source>
        <translation>Volleyball</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="57"/>
        <source>Ice hockey</source>
        <translation>Eishockey</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="58"/>
        <source>Football</source>
        <translation>Football</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="59"/>
        <source>Handball</source>
        <translation>Handball</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="60"/>
        <source>Beach volley</source>
        <translation>Beach Volleyball</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="61"/>
        <source>Futsal</source>
        <translation>Futsal</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="62"/>
        <source>Floorball</source>
        <translation>Unihockey</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="63"/>
        <source>Dancing</source>
        <translation>Tanzen</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="64"/>
        <source>Trotting</source>
        <translation>Traben</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="65"/>
        <source>Riding</source>
        <translation>Reiten</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="66"/>
        <source>Cross-trainer</source>
        <translation>Crosstrainer</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="67"/>
        <source>Fitness martial arts</source>
        <translation>Martial arts</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="68"/>
        <source>Functional training</source>
        <translation>Funktionelles Training</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="69"/>
        <source>Bootcamp</source>
        <translation>Bootcamp</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="70"/>
        <source>Freestyle roller skiing</source>
        <translation>Freestyle Rollerski</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="71"/>
        <source>Classic roller skiing</source>
        <translation>Rollerski</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="72"/>
        <source>Aerobics</source>
        <translation>Aerobic</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="73"/>
        <source>Aqua fitness</source>
        <translation>Aqua fitness</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="74"/>
        <source>Step workout</source>
        <translation>Step workout</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="75"/>
        <source>Body &amp; Mind</source>
        <translation>Körper und Geist</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="76"/>
        <source>Pilates</source>
        <translation>Pilates</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="77"/>
        <source>Stretching</source>
        <translation>Dehnen</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="78"/>
        <source>Fitness dancing</source>
        <translation>Fitness tanzen</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="79"/>
        <location filename="../activity_translation.hpp" line="83"/>
        <source>Triathlon</source>
        <translation>Triathlon</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="80"/>
        <source>Duathlon</source>
        <translation>Duathlon</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="81"/>
        <source>Triathlon (Off-road)</source>
        <translation>Triathlon (Off-road)</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="82"/>
        <source>Duathlon (Off-road)</source>
        <translation>Duathlon (Off-road)</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="84"/>
        <source>Multisport</source>
        <translation>Multisport</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="85"/>
        <source>Other indoor</source>
        <translation>Sonstige Indoor-Sportart</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="86"/>
        <source>Climbing (Indoor)</source>
        <translation>Klettern (Halle)</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="87"/>
        <source>Swimming (Open water)</source>
        <translation>Schwimmen (Freiwasser)</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="88"/>
        <source>Core-Training</source>
        <translation>Core-Training</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="89"/>
        <source>Climbing (Outdoor)</source>
        <translation>Klettern (Fels)</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="90"/>
        <source>Regeneration</source>
        <translation>Regeneration</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="91"/>
        <source>Swim (Cords)</source>
        <translation>Schwimmen (Paddles)</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="92"/>
        <source>Bike (Imported)</source>
        <translation>Rad (Importiert)</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="93"/>
        <source>Run (Imported)</source>
        <translation>Laufen (Importiert)</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="94"/>
        <source>Swim (Imported)</source>
        <translation>Schwimmen (Importiert)</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="95"/>
        <source>Note</source>
        <translation>Notiz</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="96"/>
        <source>Sick</source>
        <translation>Krank</translation>
    </message>
    <message>
        <location filename="../activity_translation.hpp" line="110"/>
        <source>Unknown %1</source>
        <translation>Unbekannt %1</translation>
    </message>
</context>
<context>
    <name>edit_activity_dialog</name>
    <message>
        <location filename="../dialogs/edit_activity_dialog.cpp" line="50"/>
        <location filename="../dialogs/edit_activity_dialog.cpp" line="60"/>
        <source>Edit activity</source>
        <translation>Einheit bearbeiten</translation>
    </message>
    <message>
        <location filename="../dialogs/edit_activity_dialog.cpp" line="62"/>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
    <message>
        <location filename="../dialogs/edit_activity_dialog.cpp" line="86"/>
        <source>&amp;Activity:</source>
        <translation>&amp;Aktivität:</translation>
    </message>
    <message>
        <location filename="../dialogs/edit_activity_dialog.cpp" line="116"/>
        <source>&amp;Time:</source>
        <translation>&amp;Zeit:</translation>
    </message>
    <message>
        <location filename="../dialogs/edit_activity_dialog.cpp" line="140"/>
        <source>&amp;Duration:</source>
        <translation>&amp;Dauer:</translation>
    </message>
    <message>
        <location filename="../dialogs/edit_activity_dialog.cpp" line="184"/>
        <source>&amp;Mood:</source>
        <translation>&amp;Stimmung:</translation>
    </message>
    <message>
        <location filename="../dialogs/edit_activity_dialog.cpp" line="200"/>
        <source>&amp;Bike:</source>
        <translation>&amp;Rad:</translation>
    </message>
    <message>
        <location filename="../dialogs/edit_activity_dialog.cpp" line="226"/>
        <source>&amp;Comment:</source>
        <translation>&amp;Kommentar:</translation>
    </message>
    <message>
        <location filename="../dialogs/edit_activity_dialog.cpp" line="249"/>
        <source>Delete activity</source>
        <translation>Einheit löschen</translation>
    </message>
    <message>
        <location filename="../dialogs/edit_activity_dialog.cpp" line="255"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../dialogs/edit_activity_dialog.cpp" line="260"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="../dialogs/edit_activity_dialog.cpp" line="316"/>
        <source>An activity for %1 already exists.

Please change time or edit existing activity.</source>
        <translation>Eine Einheit für %1 existiert bereits.

Bitte ändern Sie die Zeit oder bearbeiten Sie die existierende Einheit.</translation>
    </message>
    <message>
        <location filename="../dialogs/edit_activity_dialog.cpp" line="319"/>
        <source>Activity already exits</source>
        <translation>Einheit exitsiert bereits</translation>
    </message>
    <message>
        <location filename="../dialogs/edit_activity_dialog.cpp" line="331"/>
        <source>Saving failed. Please try again. (ERROR: %1)</source>
        <translation>Speichern fehlgeschlagen. Bitte versuchen Sie es erneut (Fehler: %1)</translation>
    </message>
    <message>
        <location filename="../dialogs/edit_activity_dialog.cpp" line="333"/>
        <source>Save failed</source>
        <translation>Speichern fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../dialogs/edit_activity_dialog.cpp" line="391"/>
        <location filename="../dialogs/edit_activity_dialog.cpp" line="393"/>
        <source>Do you really want to delete this activity?</source>
        <translation>Möchten Sie diese Einheit wirklich löschen?</translation>
    </message>
    <message>
        <location filename="../dialogs/edit_activity_dialog.cpp" line="393"/>
        <source>Delete activity?</source>
        <translation>Einheit löschen?</translation>
    </message>
    <message>
        <location filename="../dialogs/edit_activity_dialog.cpp" line="401"/>
        <source>Failed to delete activity. (ERROR: %1)</source>
        <translation>Einheit konnte nicht gelöscht werden. (Fehler: %1)</translation>
    </message>
    <message>
        <location filename="../dialogs/edit_activity_dialog.cpp" line="403"/>
        <source>Deletion failed</source>
        <translation>Löschen fehlgeschlagen</translation>
    </message>
</context>
<context>
    <name>edit_bikes_dialog</name>
    <message>
        <location filename="../dialogs/edit_bikes_dialog.cpp" line="41"/>
        <source>Edit bikes</source>
        <translation>Fahrräder bearbeiten</translation>
    </message>
    <message>
        <location filename="../dialogs/edit_bikes_dialog.cpp" line="75"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../dialogs/edit_bikes_dialog.cpp" line="80"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../dialogs/edit_bikes_dialog.cpp" line="93"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../dialogs/edit_bikes_dialog.cpp" line="98"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="../dialogs/edit_bikes_dialog.cpp" line="109"/>
        <source>New bike</source>
        <translation>Neues Rad</translation>
    </message>
    <message>
        <location filename="../dialogs/edit_bikes_dialog.cpp" line="150"/>
        <source>&amp;Name:</source>
        <translation>&amp;Name:</translation>
    </message>
    <message>
        <location filename="../dialogs/edit_bikes_dialog.cpp" line="158"/>
        <source>&amp;Chain wear (km):</source>
        <translation>&amp;Kettenverschleiß:</translation>
    </message>
</context>
<context>
    <name>main_window_qt</name>
    <message>
        <location filename="../main_window_qt.cpp" line="46"/>
        <source>Download from device</source>
        <translation>Einheiten herunterladen</translation>
    </message>
    <message>
        <location filename="../main_window_qt.cpp" line="45"/>
        <source>New activity</source>
        <translation>Neue Einheit</translation>
    </message>
    <message>
        <location filename="../main_window_qt.cpp" line="44"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location filename="../main_window_qt.cpp" line="48"/>
        <source>Edit bikes</source>
        <translation>Fahhräder bearbeiten</translation>
    </message>
    <message>
        <location filename="../main_window_qt.cpp" line="50"/>
        <source>Reload</source>
        <translation>Neu laden</translation>
    </message>
    <message>
        <location filename="../main_window_qt.cpp" line="52"/>
        <source>Change data folder</source>
        <translation>Speicherort ändern</translation>
    </message>
    <message>
        <location filename="../main_window_qt.cpp" line="56"/>
        <source>&amp;Edit</source>
        <translation>&amp;Bearbeiten</translation>
    </message>
    <message>
        <location filename="../main_window_qt.cpp" line="88"/>
        <source>No device found</source>
        <translation>Kein Gerät gefunden</translation>
    </message>
    <message>
        <location filename="../main_window_qt.cpp" line="89"/>
        <source>No polar device found. Please check connection.</source>
        <translation>Es konnte kein Polar-Gerät gefunden werden. Bitte überprüfen Sie die Verbindung.</translation>
    </message>
    <message>
        <location filename="../main_window_qt.cpp" line="98"/>
        <source>Downloading</source>
        <translation>Laden</translation>
    </message>
    <message>
        <location filename="../main_window_qt.cpp" line="60"/>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
    <message>
        <location filename="../main_window_qt.cpp" line="60"/>
        <source>Pungu Training</source>
        <translation>Pungu Training</translation>
    </message>
    <message>
        <location filename="../main_window_qt.cpp" line="98"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../main_window_qt.cpp" line="125"/>
        <source>Please wait for usb device to cancel download...</source>
        <translation>Download wird abgebrochen. Bitte warten...</translation>
    </message>
    <message>
        <location filename="../main_window_qt.cpp" line="177"/>
        <source>Data path: %1</source>
        <translation>Data path: %1</translation>
    </message>
    <message>
        <location filename="../main_window_qt.cpp" line="182"/>
        <source>Select data folder</source>
        <translation>Speicherort auswählen</translation>
    </message>
    <message>
        <location filename="../main_window_qt.cpp" line="188"/>
        <source>New data path: %1</source>
        <translation>New data path: %1</translation>
    </message>
</context>
<context>
    <name>new_activity_dialog</name>
    <message>
        <location filename="../dialogs/new_activity_dialog.cpp" line="36"/>
        <source>New activity</source>
        <translation>Neue Einheit</translation>
    </message>
    <message>
        <location filename="../dialogs/new_activity_dialog.cpp" line="57"/>
        <source>&amp;Time:</source>
        <translation>&amp;Zeit:</translation>
    </message>
    <message>
        <location filename="../dialogs/new_activity_dialog.cpp" line="88"/>
        <source>&amp;Activity:</source>
        <translation>&amp;Aktivität:</translation>
    </message>
    <message>
        <location filename="../dialogs/new_activity_dialog.cpp" line="105"/>
        <source>&amp;Duration:</source>
        <translation>&amp;Dauer:</translation>
    </message>
    <message>
        <location filename="../dialogs/new_activity_dialog.cpp" line="138"/>
        <source>&amp;Mood:</source>
        <translation>&amp;Stimmung:</translation>
    </message>
    <message>
        <location filename="../dialogs/new_activity_dialog.cpp" line="153"/>
        <source>&amp;Comment:</source>
        <translation>&amp;Kommentar:</translation>
    </message>
    <message>
        <location filename="../dialogs/new_activity_dialog.cpp" line="173"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../dialogs/new_activity_dialog.cpp" line="178"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="../dialogs/new_activity_dialog.cpp" line="231"/>
        <source>An activity for %1 already exists.

Please change time or edit existing activity.</source>
        <translation>Eine Einheit für %1 existiert bereits.

Bitte ändern Sie die Zeit oder bearbeiten Sie die existierende Einheit.</translation>
    </message>
    <message>
        <location filename="../dialogs/new_activity_dialog.cpp" line="233"/>
        <source>Activity already exits</source>
        <translation>Einheit exitsiert bereits</translation>
    </message>
    <message>
        <location filename="../dialogs/new_activity_dialog.cpp" line="245"/>
        <source>Saving failed. Please try again. (ERROR: %1)</source>
        <translation>Speichern fehlgeschlagen. Bitte versuchen Sie es erneut (Fehler: %1)</translation>
    </message>
    <message>
        <location filename="../dialogs/new_activity_dialog.cpp" line="247"/>
        <source>Save failed</source>
        <translation>Speichern fehlgeschlagen</translation>
    </message>
</context>
<context>
    <name>polar_download</name>
    <message>
        <location filename="../polar_download.cpp" line="15"/>
        <source>Downloading from device (v1)...</source>
        <translation>Lade Einheiten (v1)...</translation>
    </message>
    <message>
        <location filename="../polar_download.cpp" line="24"/>
        <source>Downloading from device (v2)...</source>
        <translation>Lade Einheiten (v2)...</translation>
    </message>
    <message>
        <location filename="../polar_download.cpp" line="41"/>
        <source>Failed to connect to v1 device: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../polar_download.cpp" line="56"/>
        <source>Failed to connect to v2 device (%1): %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../polar_download.cpp" line="68"/>
        <source>Get files for download</source>
        <translation>Get files for download</translation>
    </message>
    <message>
        <location filename="../polar_download.cpp" line="71"/>
        <source>Found %1 files for download</source>
        <translation>Found %1 files for download</translation>
    </message>
    <message>
        <location filename="../polar_download.cpp" line="79"/>
        <source>Download cancelled</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../polar_download.cpp" line="124"/>
        <source>Starting downloads</source>
        <translation>Starting downloads</translation>
    </message>
    <message>
        <location filename="../polar_download.cpp" line="129"/>
        <source>Downloading %1</source>
        <translation>Downloading %1</translation>
    </message>
    <message>
        <location filename="../polar_download.cpp" line="136"/>
        <source>Download of %1 failed. File empty.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../polar_download.cpp" line="139"/>
        <source>Download of %1 failed</source>
        <translation>Download of %1 failed</translation>
    </message>
    <message>
        <location filename="../polar_download.cpp" line="146"/>
        <source>Download finished</source>
        <translation>Download finished</translation>
    </message>
</context>
<context>
    <name>running_index_chart</name>
    <message>
        <location filename="../running_index_chart.cpp" line="79"/>
        <source>No running index data available</source>
        <translation>Keine Running Index-Daten verfügbar</translation>
    </message>
    <message>
        <location filename="../running_index_chart.cpp" line="115"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
</context>
<context>
    <name>training_charts</name>
    <message>
        <location filename="../training_charts.cpp" line="39"/>
        <source>Power</source>
        <translation>Kraft</translation>
    </message>
    <message>
        <location filename="../training_charts.cpp" line="80"/>
        <source>HR</source>
        <translation>Puls</translation>
    </message>
    <message>
        <location filename="../training_charts.cpp" line="105"/>
        <source>No hr and power data available</source>
        <translation>Keine Puls- und Kraft-Daten verfügbar</translation>
    </message>
    <message>
        <location filename="../training_charts.cpp" line="133"/>
        <source>Speed (km/h)</source>
        <translation>Geschwindigkeit (km/h)</translation>
    </message>
    <message>
        <location filename="../training_charts.cpp" line="162"/>
        <source>No speed data available</source>
        <translation>Keine Geschwindigkeitsdaten verfügbar</translation>
    </message>
    <message>
        <location filename="../training_charts.cpp" line="173"/>
        <source>Speed (min/km)</source>
        <translation>Geschwindigkeit (min/km)</translation>
    </message>
    <message>
        <location filename="../training_charts.cpp" line="213"/>
        <source>Speed (min/100m)</source>
        <translation>Geschwindigkeit (min/100m)</translation>
    </message>
    <message>
        <location filename="../training_charts.cpp" line="319"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="../training_charts.cpp" line="349"/>
        <location filename="../training_charts.cpp" line="353"/>
        <source>%1: %2
</source>
        <translation>%1 %2
</translation>
    </message>
</context>
<context>
    <name>training_detail_list</name>
    <message>
        <location filename="../training_detail_list.cpp" line="29"/>
        <source>Mood</source>
        <translation>Stimmung</translation>
    </message>
    <message>
        <location filename="../training_detail_list.cpp" line="31"/>
        <source>Activity</source>
        <translation>Einheit</translation>
    </message>
    <message>
        <location filename="../training_detail_list.cpp" line="32"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../training_detail_list.cpp" line="33"/>
        <source>Duration</source>
        <translation>Dauer</translation>
    </message>
    <message>
        <location filename="../training_detail_list.cpp" line="37"/>
        <source>Distance</source>
        <translation>Distanz</translation>
    </message>
    <message>
        <location filename="../training_detail_list.cpp" line="42"/>
        <source>Energy</source>
        <translation>Energie</translation>
    </message>
    <message>
        <location filename="../training_detail_list.cpp" line="47"/>
        <source>Running index</source>
        <translation>Running Index</translation>
    </message>
    <message>
        <location filename="../training_detail_list.cpp" line="53"/>
        <source>Heart rate average</source>
        <translation>ø Puls</translation>
    </message>
    <message>
        <location filename="../training_detail_list.cpp" line="54"/>
        <source>Heart rate max</source>
        <translation>Maximalpuls</translation>
    </message>
    <message>
        <location filename="../training_detail_list.cpp" line="64"/>
        <location filename="../training_detail_list.cpp" line="71"/>
        <location filename="../training_detail_list.cpp" line="78"/>
        <source>Speed average</source>
        <translation>ø Geschwindigkeit</translation>
    </message>
    <message>
        <location filename="../training_detail_list.cpp" line="66"/>
        <location filename="../training_detail_list.cpp" line="73"/>
        <location filename="../training_detail_list.cpp" line="79"/>
        <source>Speed max</source>
        <translation>Maximale Geschwindigkeit</translation>
    </message>
    <message>
        <location filename="../training_detail_list.cpp" line="78"/>
        <location filename="../training_detail_list.cpp" line="79"/>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
    <message>
        <location filename="../training_detail_list.cpp" line="86"/>
        <source>Power average</source>
        <translation>ø Kraft</translation>
    </message>
    <message>
        <location filename="../training_detail_list.cpp" line="87"/>
        <source>Power max</source>
        <translation>Maximale Kraft</translation>
    </message>
    <message>
        <location filename="../training_detail_list.cpp" line="93"/>
        <source>Cadence average</source>
        <translation>ø Trittfrequenz</translation>
    </message>
    <message>
        <location filename="../training_detail_list.cpp" line="94"/>
        <source>Cadence max</source>
        <translation>Maximale Trittfrequenz</translation>
    </message>
    <message>
        <location filename="../training_detail_list.cpp" line="100"/>
        <source>Altitude</source>
        <translation>Höhe</translation>
    </message>
    <message>
        <location filename="../training_detail_list.cpp" line="101"/>
        <source>Altitude min</source>
        <translation>Mindesthöhe</translation>
    </message>
    <message>
        <location filename="../training_detail_list.cpp" line="102"/>
        <source>Altitude max</source>
        <translation>Maximale Höhe</translation>
    </message>
    <message>
        <location filename="../training_detail_list.cpp" line="103"/>
        <source>Ascent</source>
        <translation>Aufstieg</translation>
    </message>
    <message>
        <location filename="../training_detail_list.cpp" line="104"/>
        <source>Descent</source>
        <translation>Abstieg</translation>
    </message>
    <message>
        <location filename="../training_detail_list.cpp" line="108"/>
        <source>Imported from Polar device</source>
        <translation>Vom Polar-Gerät geladen</translation>
    </message>
    <message>
        <location filename="../training_detail_list.cpp" line="109"/>
        <source>no</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location filename="../training_detail_list.cpp" line="109"/>
        <source>yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../training_detail_list.cpp" line="110"/>
        <source>Comment</source>
        <translation>Kommentar</translation>
    </message>
</context>
<context>
    <name>training_detail_list::T</name>
    <message>
        <location filename="../training_detail_list.hpp" line="21"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
</context>
<context>
    <name>training_diary</name>
    <message>
        <location filename="../training_diary.cpp" line="61"/>
        <source>Init cache: %1 µs</source>
        <translation>Init cache: %1 µs</translation>
    </message>
    <message>
        <location filename="../training_diary.cpp" line="65"/>
        <source>Found %1 activities</source>
        <translation>Found %1 activities</translation>
    </message>
</context>
<context>
    <name>training_list</name>
    <message>
        <location filename="../training_list.cpp" line="44"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../training_list.cpp" line="44"/>
        <source>Time</source>
        <translation>Zeit</translation>
    </message>
    <message>
        <location filename="../training_list.cpp" line="44"/>
        <source>Activity</source>
        <translation>Einheit</translation>
    </message>
    <message>
        <location filename="../training_list.cpp" line="44"/>
        <source>Duration</source>
        <translation>Dauer</translation>
    </message>
    <message>
        <location filename="../training_list.cpp" line="44"/>
        <source>Distance</source>
        <translation>Distanz</translation>
    </message>
    <message>
        <location filename="../training_list.cpp" line="45"/>
        <source>Energy</source>
        <translation>Energie</translation>
    </message>
    <message>
        <location filename="../training_list.cpp" line="45"/>
        <source>ø HR</source>
        <translation>ø Puls</translation>
    </message>
    <message>
        <location filename="../training_list.cpp" line="45"/>
        <source>RI</source>
        <translation>RI</translation>
    </message>
    <message>
        <location filename="../training_list.cpp" line="45"/>
        <source>Mood</source>
        <translation>Stimmung</translation>
    </message>
    <message>
        <location filename="../training_list.cpp" line="45"/>
        <source>Comment</source>
        <translation>Kommentar</translation>
    </message>
    <message>
        <location filename="../training_list.cpp" line="123"/>
        <source>Total %1</source>
        <translation>Geammt %1</translation>
    </message>
    <message>
        <location filename="../training_list.cpp" line="125"/>
        <source>%1 activities</source>
        <translation>%1 Einheiten</translation>
    </message>
</context>
<context>
    <name>training_list::T</name>
    <message>
        <location filename="../training_list.hpp" line="27"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
</context>
<context>
    <name>training_summary_detail</name>
    <message>
        <location filename="../training_summary_detail.cpp" line="30"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../training_summary_detail.cpp" line="30"/>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
    <message>
        <location filename="../training_summary_detail.cpp" line="94"/>
        <location filename="../training_summary_detail.cpp" line="100"/>
        <location filename="../training_summary_detail.cpp" line="106"/>
        <location filename="../training_summary_detail.cpp" line="112"/>
        <source>Distance</source>
        <translation>Distanz</translation>
    </message>
    <message>
        <location filename="../training_summary_detail.cpp" line="95"/>
        <location filename="../training_summary_detail.cpp" line="101"/>
        <location filename="../training_summary_detail.cpp" line="107"/>
        <location filename="../training_summary_detail.cpp" line="113"/>
        <location filename="../training_summary_detail.cpp" line="118"/>
        <source>Duration</source>
        <translation>Dauer</translation>
    </message>
    <message>
        <location filename="../training_summary_detail.cpp" line="98"/>
        <source>Run</source>
        <translation>Laufen</translation>
    </message>
    <message>
        <location filename="../training_summary_detail.cpp" line="93"/>
        <location filename="../training_summary_detail.cpp" line="99"/>
        <location filename="../training_summary_detail.cpp" line="105"/>
        <location filename="../training_summary_detail.cpp" line="111"/>
        <location filename="../training_summary_detail.cpp" line="117"/>
        <source>Activities</source>
        <translation>Einheiten</translation>
    </message>
    <message>
        <location filename="../training_summary_detail.cpp" line="104"/>
        <source>Bike</source>
        <translation>Rad</translation>
    </message>
    <message>
        <location filename="../training_summary_detail.cpp" line="110"/>
        <source>Swim</source>
        <translation>Schwimmen</translation>
    </message>
    <message>
        <location filename="../training_summary_detail.cpp" line="116"/>
        <source>Other</source>
        <translation>Andere</translation>
    </message>
    <message>
        <location filename="../training_summary_detail.cpp" line="124"/>
        <source>Bike chain wear</source>
        <translation>Kettenverschleiß</translation>
    </message>
    <message>
        <location filename="../training_summary_detail.cpp" line="90"/>
        <location filename="../training_summary_detail.cpp" line="128"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
</context>
<context>
    <name>training_summary_detail::T</name>
    <message>
        <location filename="../training_summary_detail.hpp" line="19"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="../training_summary_detail.hpp" line="26"/>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
</context>
<context>
    <name>training_summary_view</name>
    <message>
        <location filename="../training_summary_view.cpp" line="24"/>
        <source>&amp;Details</source>
        <translation>&amp;Details</translation>
    </message>
    <message>
        <location filename="../training_summary_view.cpp" line="25"/>
        <source>&amp;Graphs</source>
        <translation>&amp;Graphen</translation>
    </message>
</context>
<context>
    <name>training_view</name>
    <message>
        <location filename="../training_view.cpp" line="30"/>
        <source>&amp;Details</source>
        <translation>&amp;Details</translation>
    </message>
    <message>
        <location filename="../training_view.cpp" line="31"/>
        <source>&amp;Graphs</source>
        <translation>&amp;Graphen</translation>
    </message>
</context>
</TS>
