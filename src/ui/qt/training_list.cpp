#include "training_list.hpp"

#include <QtWidgets>

#include "../../training_data/duration.hpp"
#include "activity_translation.hpp"
#include "date_time.hpp"

class TreeItemDelegate : public QStyledItemDelegate
{
  public:
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
    {
        auto c = index.siblingAtColumn(0);
        if (c.isValid() && c.data(is_new_role).toBool())
        {
            QStyleOptionViewItem s = *qstyleoption_cast<const QStyleOptionViewItem *>(&option);
            s.font.setWeight(QFont::Bold);
            QStyledItemDelegate::paint(painter, s, index);
        }
        else
        {
            QStyledItemDelegate::paint(painter, option, index);
        }
    }
};

training_list::training_list(std::shared_ptr<training_cache> cache, QWidget *parent) : QTreeView(parent), cache(cache)
{
    setSelectionMode(QAbstractItemView::SelectionMode::SingleSelection);
    setEditTriggers(QAbstractItemView::NoEditTriggers);
    setItemDelegate(new TreeItemDelegate);
    setModel(new QStandardItemModel(this));
}

void training_list::load_data(const std::map<long long, pt_data::training> &training_data, const int &year,
                              const qlonglong &date_time)
{

    QModelIndex old_selection = get_current_index();
    auto model = static_cast<QStandardItemModel *>(this->model());
    model->clear();

    QStringList labels = {tr("Date"),   tr("Time"), tr("Activity"), tr("Duration"), tr("Distance"),
                          tr("Energy"), tr("ø HR"), tr("RI"),       tr("Mood"),     tr("Comment")};
    model->setHorizontalHeaderLabels(labels);

    float total_distance = 0;
    int total_energy = 0;
    int running_index = 0;
    int running_index_count = 0;
    duration total_duration;
    bool has_new_items = false;

    auto training = training_data.rbegin();

    bool found_training = false;
    while (training != training_data.rend())
    {
        int training_year = training->second.date_time().year();

        qlonglong training_date = training->first - (training->first % 1000000);

        if ((year == 0 && date_time == 0) || training_year == year || training_date == date_time)
        {
            found_training = true;
            has_new_items |= training->second.is_new();
            QString comment = QString::fromStdString(training->second.comment()).simplified().trimmed();
            if (comment.length() > 30)
            {
                comment = comment.left(27);
                comment.append("...");
            }

            QString date_string;
            date_time_to_qstring(training->second.date_time(), Q_DATE_FORMAT_WITH_WEEKDAY, date_string);

            QString time_string;
            date_time_to_qstring(training->second.date_time(), Q_TIME_FORMAT, time_string);

            QString duration_string;
            duration_to_qstring(training->second.duration(), duration_string);

            QList<QStandardItem *> items;
            auto item_data = new QStandardItem(date_string);
            item_data->setData((qlonglong)training->first, date_time_role);
            item_data->setData(training->second.is_new(), is_new_role);
            item_data->setData(training_date, date_role); // Required to search item on calendar selection

            items.append(item_data);
            items.append(new QStandardItem(time_string));
            // items.append(new QStandardItem(Icons::Running(), ""));
            items.append(new QStandardItem(activity_type_translation::translate(training->second.activity_type())));
            items.append(new QStandardItem(duration_string));
            items.append(new QStandardItem(number_to_qstring_zero_empty(training->second.distance())));
            items.append(new QStandardItem(number_to_qstring_zero_empty(training->second.energy())));
            items.append(new QStandardItem(number_to_qstring_zero_empty(training->second.heart_rate().avg())));
            items.append(new QStandardItem(number_to_qstring_zero_empty(training->second.running_index())));
            items.append(new QStandardItem(QString::fromStdString(training->second.mood())));
            items.append(new QStandardItem(comment));

            model->appendRow(items);

            total_distance += training->second.distance();
            total_energy += training->second.energy();
            total_duration += training->second.duration();
            if (training->second.running_index() > 0)
            {
                ++running_index_count;
                running_index += training->second.running_index();
            }
        }
        else if (found_training)
        {
            // Cache sorted by date. If no additional training found, stop
            break;
        }

        ++training;
    }

    QList<QStandardItem *> total;
    total.append(year == 0 ? new QStandardItem() : new QStandardItem(tr("Total %1").arg(year)));
    total.append(new QStandardItem());
    total.append(new QStandardItem(tr("%1 activities").arg(model->rowCount())));
    total.append(new QStandardItem(QString::fromStdString(total_duration.to_string())));
    total.append(new QStandardItem(number_to_qstring_zero_empty(total_distance)));
    total.append(new QStandardItem(number_to_qstring_zero_empty(total_energy)));
    total.append(new QStandardItem());
    if (running_index_count > 0)
    {
        total.append(new QStandardItem(number_to_qstring_zero_empty(running_index / running_index_count)));
    }
    else
    {
        total.append(new QStandardItem());
    }

    total.append(new QStandardItem());

    model->insertRow(0, total);

    // Seperator
    QList<QStandardItem *> empty;
    model->insertRow(1, empty);

    // Fit to size
    for (int i = 0; i < model->columnCount(); ++i)
    {
        resizeColumnToContents(i);

        // Add some pixels to support bold fonts
        if (has_new_items)
        {
            setColumnWidth(i, columnWidth(i) + 5);
        }
    }

    if (old_selection.isValid())
    {
        auto index_to_select = this->model()->index(old_selection.row(), 0);
        if (index_to_select.isValid())
        {
            select_item(index_to_select);
        }
        else
        {
            // Remove details
            select_item(0);
        }
    }
}

void training_list::select_item(qlonglong sortable_date_time)
{
    auto index_to_select = this->model()->index(0, 0);

    if (sortable_date_time != 0)
    {
        auto model = static_cast<QStandardItemModel *>(this->model());
        QModelIndexList list = model->match(model->index(0, 0), date_role, sortable_date_time);
        if (!list.empty())
        {
            index_to_select = list.last();
        }
    }

    select_item(index_to_select);
}

void training_list::select_item(QModelIndex &index_to_select)
{
    scrollTo(index_to_select, QAbstractItemView::PositionAtCenter);
    selectionModel()->select(index_to_select, QItemSelectionModel::SelectionFlag::ClearAndSelect |
                                                  QItemSelectionModel::SelectionFlag::Rows);
}

qlonglong training_list::get_selected_date_time()
{
    return get_date_time_from_index(get_current_index());
}

qlonglong training_list::get_date_time_from_index(const QModelIndex &index)
{
    qlonglong date_time = 0;
    if (index.isValid())
    {
        auto c = index.siblingAtColumn(0);
        if (c.isValid())
        {
            date_time = c.data(date_time_role).toLongLong();
        }
    }

    return date_time;
}

QModelIndex training_list::get_current_index()
{
    QModelIndex index;
    if (selectionModel()->selectedRows().count() == 1)
    {
        index = selectionModel()->selectedRows()[0];
    }

    return index;
}