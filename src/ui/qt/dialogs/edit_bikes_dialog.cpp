#include "edit_bikes_dialog.hpp"

#include <QComboBox>
#include <QHBoxLayout>
#include <QLabel>
#include <QtWidgets>
#include <filesystem>
#include <fstream>
#include <iostream>

#include "google/protobuf/message_lite.h"

#include "../../../training_data/pt_bikes_helper.hpp"

const int label_width = 120;

edit_bikes_dialog::edit_bikes_dialog(const QString &data_path, QWidget *parent)
    : QDialog(parent), was_canceled(true), data_path(data_path), name_edit(new QLineEdit),
      chain_edit(new QDoubleSpinBox), bikes_combo_box(new QComboBox)
{
    pt_data::load_bikes(data_path.toStdString(), bikes);
    connect(name_edit, &QLineEdit::textChanged, this, &edit_bikes_dialog::name_changed);
    connect(chain_edit, &QDoubleSpinBox::valueChanged, this, &edit_bikes_dialog::chain_wear_changed);

    auto main_layout = new QVBoxLayout(this);
    main_layout->setSpacing(15);
    main_layout->addLayout(create_title());
    main_layout->addLayout(create_bike_selection());
    main_layout->addLayout(create_bike_edit());
    main_layout->addLayout(create_buttons());
    chain_edit->setMinimum(0.0);
    chain_edit->setMaximum(99999.0);
}

QLayout *edit_bikes_dialog::create_title()
{
    QHBoxLayout *layout = new QHBoxLayout();
    layout->setAlignment(Qt::AlignmentFlag::AlignCenter);

    QLabel *edit_label = new QLabel(tr("Edit bikes"));
    auto font = edit_label->font();
    font.setBold(true);
    edit_label->setFont(font);

    layout->addWidget(edit_label);
    return layout;
}

QLayout *edit_bikes_dialog::create_bike_selection()
{
    auto bikes_layout = new QHBoxLayout;
    bikes_layout->setAlignment(Qt::AlignmentFlag::AlignHCenter);

    if (!bikes.bikes().empty())
    {
        for (auto &bike : bikes.bikes())
        {
            bikes_combo_box->addItem(QString::fromStdString(bike.name()), bike.id());
        }
    }

    bikes_layout->addWidget(bikes_combo_box, 1);

    connect(bikes_combo_box, &QComboBox::currentIndexChanged, this, &edit_bikes_dialog::bike_changed);
    if (bikes.bikes().size() > 0)
    {
        bike_changed(0);
    }
    else
    {
        add_bike();
    }

    QPushButton *remove_bike_button = new QPushButton(tr("-"));
    remove_bike_button->setAutoDefault(false);
    connect(remove_bike_button, &QPushButton::clicked, this, &edit_bikes_dialog::remove_bike);
    bikes_layout->addWidget(remove_bike_button);

    QPushButton *add_bike_button = new QPushButton(tr("+"));
    add_bike_button->setAutoDefault(false);
    connect(add_bike_button, &QPushButton::clicked, this, &edit_bikes_dialog::add_bike);
    bikes_layout->addWidget(add_bike_button);

    return bikes_layout;
}

QLayout *edit_bikes_dialog::create_buttons()
{
    QHBoxLayout *buttons_layout = new QHBoxLayout;
    buttons_layout->setAlignment(Qt::AlignmentFlag::AlignRight);

    QPushButton *cancel_button = new QPushButton(tr("Cancel"));
    cancel_button->setAutoDefault(false);
    connect(cancel_button, &QPushButton::clicked, this, &edit_bikes_dialog::cancel);
    buttons_layout->addWidget(cancel_button);

    QPushButton *save_button = new QPushButton(tr("Save"));
    save_button->setDefault(true);
    connect(save_button, &QPushButton::clicked, this, &edit_bikes_dialog::save);
    buttons_layout->addWidget(save_button);

    return buttons_layout;
}

void edit_bikes_dialog::add_bike()
{
    auto bike_id = get_next_id();
    QString bike_name = tr("New bike");

    auto bikeToAdd = bikes.mutable_bikes()->Add();
    bikeToAdd->set_name(bike_name.toStdString());
    bikeToAdd->set_chain_km(0);
    bikeToAdd->set_id(bike_id);

    bikes_combo_box->addItem(bike_name, bike_id);
    bikes_combo_box->setCurrentIndex(bikes_combo_box->count() - 1);
    bike_changed(0);
}

void edit_bikes_dialog::remove_bike()
{
    int index = bikes_combo_box->currentIndex();
    if (index > 0)
    {
        bikes.mutable_bikes()->SwapElements(index, bikes.bikes_size() - 1);
    }

    if (index >= 0)
    {
        bikes.mutable_bikes()->RemoveLast();
        bikes_combo_box->removeItem(index);
        bikes_combo_box->setCurrentIndex(index - 1);
        bike_changed(0);
    }
}

int edit_bikes_dialog::get_next_id()
{
    int id = bikes.next_id();
    bikes.set_next_id(id + 1);

    return id;
}

QLayout *edit_bikes_dialog::create_bike_edit()
{
    QVBoxLayout *bike_edit_layout = new QVBoxLayout;

    QLabel *name_label = new QLabel(tr("&Name:"));
    name_label->setMinimumWidth(label_width);
    name_label->setBuddy(name_edit);

    QHBoxLayout *name_edit_layout = new QHBoxLayout;
    name_edit_layout->addWidget(name_label);
    name_edit_layout->addWidget(name_edit);

    QLabel *chain_label = new QLabel(tr("&Chain wear (km):"));
    chain_label->setMinimumWidth(label_width);
    chain_label->setBuddy(chain_edit);

    QHBoxLayout *chain_edit_layout = new QHBoxLayout;
    chain_edit_layout->addWidget(chain_label);
    chain_edit_layout->addWidget(chain_edit);

    bike_edit_layout->addLayout(name_edit_layout);
    bike_edit_layout->addLayout(chain_edit_layout, 1);

    return bike_edit_layout;
}

void edit_bikes_dialog::bike_changed(const int &index)
{
    auto bike = get_selected_bike();
    name_edit->setText(QString::fromStdString(bike.name()));
    chain_edit->setValue(bike.chain_km());
}

void edit_bikes_dialog::name_changed(const QString &bike_name)
{
    std::string oldBikeName = get_selected_bike().name();
    if (!bikes.bikes().empty())
    {
        for (auto &bike : *bikes.mutable_bikes())
        {
            if (bike.name().compare(oldBikeName) == 0)
            {
                bike.set_name(bike_name.toStdString());
            }
        }
    }
}

void edit_bikes_dialog::chain_wear_changed(const double &new_value)
{
    auto bike_name = get_selected_bike().name();

    if (!bikes.bikes().empty())
    {
        for (auto &bike : *bikes.mutable_bikes())
        {
            if (bike.name().compare(bike_name) == 0)
            {
                bike.set_chain_km(new_value);
            }
        }
    }
}

void edit_bikes_dialog::save()
{
    pt_data::save_bikes(data_path.toStdString(), bikes);
    was_canceled = false;
    this->close();
}

void edit_bikes_dialog::cancel()
{
    was_canceled = true;
    this->close();
}

pt_data::bike edit_bikes_dialog::get_selected_bike()
{
    int id = bikes_combo_box->currentData().toInt();
    pt_data::bike result;
    if (!bikes.bikes().empty())
    {
        for (auto &bike : *bikes.mutable_bikes())
        {
            if (bike.id() == id)
            {
                result = bike;
            }
        }
    }

    return result;
}

void edit_bikes_dialog::write_to_log(const QString &message) const
{
    static QLoggingCategory category("Edit activity");
    qCInfo(category) << message;
}