#ifndef PO_EDITACTIVITY_DIALOG_H
#define PO_EDITACTIVITY_DIALOG_H

#include "../../../pt_proto/pt_bikes.pb.h"
#include "../../../pt_proto/pt_data.pb.h"
#include <QDialog>

namespace pt_data
{
class training_bikes;
}

class QPlainTextEdit;
class QLineEdit;
class QComboBox;

class edit_activity_dialog : public QDialog
{
    Q_OBJECT

  public:
    bool was_canceled;
    qlonglong new_date;

  private:
    QLineEdit *mood_edit;
    QPlainTextEdit *comment_edit;
    const QString data_path;
    pt_data::training training_to_save;
    pt_data::training original_training;
    std::shared_ptr<pt_data::training_bikes> bikes;
    QComboBox *bikes_combo_box;

  public:
    edit_activity_dialog(const pt_data::training &training, std::shared_ptr<pt_data::training_bikes> bikes,
                         const QString &data_path, QWidget *parent = 0);

  private:
    void cancel();
    void save();
    void update_bikes();
    void delete_activity();

    void write_to_log(const QString &message) const;

  private: // Change event handler
    void activity_changed(const QString &activity_name);
    void hours_changed(const int &hours);
    void minutes_changed(const int &minutes);
    void seconds_changed(const int &seconds);
    void time_changed(const QDateTime &date_time);
    void bike_changed(const int &index);

  private: // Create widgets
    QLayout *create_title();
    QLayout *create_date_time();
    QLayout *create_activity_type();
    QLayout *create_duration();
    QLayout *create_mood();
    QLayout *create_bike_selection();
    QLayout *create_comment();
    QLayout *create_buttons();
};

#endif