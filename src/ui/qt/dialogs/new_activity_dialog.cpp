#include "new_activity_dialog.hpp"

#include <QComboBox>
#include <QHBoxLayout>
#include <QLabel>
#include <QtWidgets>

#include "../../../training_data/activity_type.hpp"
#include "../../../training_data/time_extensions.hpp"
#include "../../../training_data/training_saver.hpp"
#include "../activity_translation.hpp"
#include "../date_time.hpp"

const int label_width = 90;

new_activity_dialog::new_activity_dialog(const QString &data_path, const QDate &date, QWidget *parent)
    : QDialog(parent), was_canceled(true), data_path(data_path)
{
    auto main_layout = new QVBoxLayout(this);
    main_layout->setSpacing(15);
    main_layout->addLayout(create_title());
    main_layout->addLayout(create_activity_type());
    main_layout->addLayout(create_date_time(date));
    main_layout->addLayout(create_duration());
    main_layout->addLayout(create_mood());
    main_layout->addLayout(create_comment());
    main_layout->addLayout(create_save_cancel());

    training_to_save.set_data_type(pt_data::data_type::CUSTOM);
}

QLayout *new_activity_dialog::create_title()
{
    QHBoxLayout *layout = new QHBoxLayout();
    layout->setAlignment(Qt::AlignmentFlag::AlignCenter);
    QLabel *label = new QLabel(tr("New activity"));

    auto font = label->font();
    font.setBold(true);
    label->setFont(font);

    layout->addWidget(label);

    return layout;
}

QLayout *new_activity_dialog::create_date_time(const QDate &date)
{
    // Set seconds to 0
    QTime current_time = QTime::currentTime();
    current_time.setHMS(current_time.hour(), current_time.minute(), 0);

    auto date_time_edit = new QDateTimeEdit();
    date_time_edit->setTime(current_time);
    date_time_edit->setDate(date);

    QLabel *label = new QLabel(tr("&Time:"));
    label->setMinimumWidth(label_width);
    label->setBuddy(date_time_edit);

    QHBoxLayout *time_layout = new QHBoxLayout;
    time_layout->addWidget(label);
    time_layout->addWidget(date_time_edit, 1);

    connect(date_time_edit, &QDateTimeEdit::dateTimeChanged, this, &new_activity_dialog::time_changed);
    time_changed(date_time_edit->dateTime());

    return time_layout;
}

QLayout *new_activity_dialog::create_activity_type()
{
    QComboBox *activity_combo_box = new QComboBox();
    QStringList activities;

    for (auto &activity : activity_type_translation::get_all_activities())
    {
        // Remove "legacy" triathlon entry
        if (activity.first != pt_data::activity_type::TriathlonOld)
        {
            activities.append(activity.second);
        }
    }

    activities.sort();
    activity_combo_box->addItems(activities);

    QLabel *activity_label = new QLabel(tr("&Activity:"));
    activity_label->setMinimumWidth(label_width);
    activity_label->setBuddy(activity_combo_box);

    connect(activity_combo_box, &QComboBox::textActivated, this, &new_activity_dialog::activity_changed);
    activity_changed(activities.first());

    auto activity_type_layout = new QHBoxLayout;
    activity_type_layout->setAlignment(Qt::AlignmentFlag::AlignLeft);
    activity_type_layout->addWidget(activity_label);
    activity_type_layout->addWidget(activity_combo_box);

    return activity_type_layout;
}

QLayout *new_activity_dialog::create_duration()
{
    QLabel *label = new QLabel(tr("&Duration:"));
    label->setMinimumWidth(label_width);

    QSpinBox *hours = new QSpinBox;
    hours->setMinimum(0);
    label->setBuddy(hours);

    QSpinBox *minutes = new QSpinBox;
    minutes->setMinimum(0);
    minutes->setMaximum(59);

    QSpinBox *seconds = new QSpinBox;
    seconds->setMinimum(0);
    seconds->setMaximum(59);

    QHBoxLayout *duration_layout = new QHBoxLayout;
    duration_layout->addWidget(label);
    duration_layout->addWidget(hours);
    duration_layout->addWidget(minutes);
    duration_layout->addWidget(seconds);

    connect(hours, &QSpinBox::valueChanged, this, &new_activity_dialog::hours_changed);
    connect(minutes, &QSpinBox::valueChanged, this, &new_activity_dialog::minutes_changed);
    connect(seconds, &QSpinBox::valueChanged, this, &new_activity_dialog::seconds_changed);
    hours_changed(0);
    minutes_changed(0);
    seconds_changed(0);

    return duration_layout;
}

QLayout *new_activity_dialog::create_mood()
{
    QLabel *label = new QLabel(tr("&Mood:"));
    label->setMinimumWidth(label_width);

    mood_edit = new QLineEdit;

    label->setBuddy(mood_edit);
    QHBoxLayout *mood_layout = new QHBoxLayout;
    mood_layout->addWidget(label);
    mood_layout->addWidget(mood_edit);

    return mood_layout;
}

QLayout *new_activity_dialog::create_comment()
{
    QLabel *label = new QLabel(tr("&Comment:"));
    label->setMinimumWidth(label_width);

    comment_edit = new QPlainTextEdit;
    comment_edit->setLineWidth(50);
    comment_edit->setMinimumHeight(200);

    label->setBuddy(comment_edit);
    QHBoxLayout *comment_layout = new QHBoxLayout;
    comment_layout->addWidget(label);
    comment_layout->addWidget(comment_edit);

    return comment_layout;
}

QLayout *new_activity_dialog::create_save_cancel()
{
    QHBoxLayout *buttons_layout = new QHBoxLayout;
    buttons_layout->setAlignment(Qt::AlignmentFlag::AlignRight);

    QPushButton *cancel_button = new QPushButton(tr("Cancel"));
    cancel_button->setAutoDefault(false);
    connect(cancel_button, &QPushButton::clicked, this, &new_activity_dialog::cancel);
    buttons_layout->addWidget(cancel_button);

    QPushButton *save_button = new QPushButton(tr("Save"));
    save_button->setDefault(true);
    connect(save_button, &QPushButton::clicked, this, &new_activity_dialog::save);
    buttons_layout->addWidget(save_button);

    return buttons_layout;
}

void new_activity_dialog::activity_changed(const QString &activity_name)
{
    for (auto &activity : activity_type_translation::get_all_activities())
    {
        if (activity.second == activity_name)
        {
            training_to_save.set_activity_type((pt_data::activity_type)activity.first);
            break;
        }
    }
}

void new_activity_dialog::hours_changed(const int &hours)
{
    training_to_save.mutable_duration()->set_hours(hours);
}

void new_activity_dialog::minutes_changed(const int &minutes)
{
    training_to_save.mutable_duration()->set_minutes(minutes);
}

void new_activity_dialog::seconds_changed(const int &seconds)
{
    training_to_save.mutable_duration()->set_seconds(seconds);
}

void new_activity_dialog::time_changed(const QDateTime &date_time)
{
    training_to_save.mutable_date_time()->set_year(date_time.date().year());
    training_to_save.mutable_date_time()->set_month(date_time.date().month());
    training_to_save.mutable_date_time()->set_day(date_time.date().day());
    training_to_save.mutable_date_time()->set_hours(date_time.time().hour());
    training_to_save.mutable_date_time()->set_minutes(date_time.time().minute());
    training_to_save.mutable_date_time()->set_seconds(date_time.time().second());
}

void new_activity_dialog::save()
{
    if (training_saver::training_exist(training_to_save, data_path.toStdString()))
    {
        QString date_time;
        date_time_to_qstring(training_to_save.date_time(), Q_DATE_TIME_FORMAT, date_time);

        auto message =
            tr("An activity for %1 already exists.\n\nPlease change time or edit existing activity.").arg(date_time);
        write_to_log(message);
        QMessageBox message_box(QMessageBox::Information, tr("Activity already exits"), message);
        message_box.exec();
    }
    else
    {
        training_to_save.set_mood(mood_edit->text().toStdString());
        training_to_save.set_comment(comment_edit->toPlainText().toStdString());
        training_to_save.set_is_new(false);
        auto result = training_saver::save_training(training_to_save, data_path.toStdString());

        if (result != training_saver::return_code::OK)
        {
            auto message = tr("Saving failed. Please try again. (ERROR: %1)").arg(result);
            write_to_log(message);
            QMessageBox message_box(QMessageBox::Warning, tr("Save failed"), message);
            message_box.exec();
        }
        else
        {
            was_canceled = false;
            this->close();
        }
    }
}

void new_activity_dialog::cancel()
{
    this->close();
}

void new_activity_dialog::write_to_log(const QString &message) const
{
    static QLoggingCategory category("New workout");
    qCInfo(category) << message;
}