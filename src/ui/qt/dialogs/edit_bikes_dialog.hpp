#ifndef PO_EDITBIKES_DIALOG_H
#define PO_EDITBIKES_DIALOG_H

#include "../../../pt_proto/pt_bikes.pb.h"
#include <QDialog>

class QPlainTextEdit;
class QLineEdit;
class QDoubleSpinBox;
class QComboBox;

class edit_bikes_dialog : public QDialog
{
    Q_OBJECT

  public:
    bool was_canceled;

  private:
    pt_data::training_bikes bikes;
    const QString data_path;

    QLineEdit *name_edit;
    QDoubleSpinBox *chain_edit;
    QComboBox *bikes_combo_box;

  public:
    edit_bikes_dialog(const QString &data_path, QWidget *parent = 0);

  private:
    void cancel();
    void save();

    pt_data::bike get_selected_bike();

    void write_to_log(const QString &message) const;

  private: // Change event handler
    void bike_changed(const int &index);
    void name_changed(const QString &bike_name);
    void chain_wear_changed(const double &newValue);
    void add_bike();
    void remove_bike();
    int get_next_id();

  private: // Create widgets
    QLayout *create_title();
    QLayout *create_buttons();
    QLayout *create_bike_selection();
    QLayout *create_bike_edit();
};

#endif