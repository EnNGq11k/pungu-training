#include "edit_activity_dialog.hpp"

#include <filesystem>
#include <fstream>
#include <iostream>

#include <QComboBox>
#include <QHBoxLayout>
#include <QLabel>
#include <QtWidgets>

#include "../../../pt_proto/pt_bikes.pb.h"
#include "../../../training_data/activity_type.hpp"
#include "../../../training_data/pt_bikes_helper.hpp"
#include "../../../training_data/time_extensions.hpp"
#include "../../../training_data/training_saver.hpp"
#include "../activity_translation.hpp"
#include "../date_time.hpp"

const int label_width = 90;

edit_activity_dialog::edit_activity_dialog(const pt_data::training &training,
                                           std::shared_ptr<pt_data::training_bikes> bikes, const QString &data_path,
                                           QWidget *parent)
    : QDialog(parent), was_canceled(true), data_path(data_path), training_to_save(training),
      original_training(training), bikes(bikes), bikes_combo_box(new QComboBox)
{
    auto main_layout = new QVBoxLayout(this);
    main_layout->setSpacing(15);
    main_layout->addLayout(create_title());
    main_layout->addLayout(create_activity_type());
    main_layout->addLayout(create_date_time());
    main_layout->addLayout(create_duration());
    main_layout->addLayout(create_mood());

    if (activity_type::is_bike_activity(training.activity_type()) && !bikes->bikes().empty())
    {
        main_layout->addLayout(create_bike_selection());
    }

    main_layout->addLayout(create_comment());
    main_layout->addLayout(create_buttons());
}

QLayout *edit_activity_dialog::create_title()
{
    QHBoxLayout *layout = new QHBoxLayout();
    layout->setAlignment(Qt::AlignmentFlag::AlignCenter);

    QLabel *edit_label = new QLabel(tr("Edit activity"));
    auto font = edit_label->font();
    font.setBold(true);
    edit_label->setFont(font);

    layout->addWidget(edit_label);

    QString date_time;
    date_time_to_qstring(original_training.date_time(), Q_DATE_TIME_FORMAT, date_time);

    QLabel *activity_label = new QLabel(tr("Edit activity"));
    activity_label->setText(
        tr("%1 %2").arg(activity_type_translation::translate(original_training.activity_type())).arg(date_time));

    layout->addWidget(activity_label);

    return layout;
}

QLayout *edit_activity_dialog::create_activity_type()
{
    QComboBox *activity_combo_box = new QComboBox();
    QStringList activities;

    for (auto &activity : activity_type_translation::get_all_activities())
    {
        // Remove "legacy" triathlon entry
        if (activity.first != pt_data::activity_type::TriathlonOld)
        {
            activities.append(activity.second);
        }
    }

    activities.sort();
    activity_combo_box->addItems(activities);

    QLabel *activity_label = new QLabel(tr("&Activity:"));
    activity_label->setMinimumWidth(label_width);
    activity_label->setBuddy(activity_combo_box);

    auto activity_type_layout = new QHBoxLayout;
    activity_type_layout->setAlignment(Qt::AlignmentFlag::AlignLeft);
    activity_type_layout->addWidget(activity_label);
    activity_type_layout->addWidget(activity_combo_box);
    activity_combo_box->setCurrentText(activity_type_translation::translate(training_to_save.activity_type()));

    // Changes to imported activites are not allowed (now?)
    if (training_to_save.data_type() == pt_data::data_type::CUSTOM)
    {
        connect(activity_combo_box, &QComboBox::textActivated, this, &edit_activity_dialog::activity_changed);
    }
    else
    {
        activity_combo_box->setEnabled(false);
    }

    return activity_type_layout;
}

QLayout *edit_activity_dialog::create_date_time()
{
    QDateTime date_time = date_time_to_qdatetime(original_training.date_time());

    auto date_time_edit = new QDateTimeEdit();
    date_time_edit->setDateTime(date_time);

    QLabel *label = new QLabel(tr("&Time:"));
    label->setMinimumWidth(label_width);
    label->setBuddy(date_time_edit);

    QHBoxLayout *time_layout = new QHBoxLayout;
    time_layout->addWidget(label);
    time_layout->addWidget(date_time_edit, 1);

    // Changes to imported activites are not allowed (now?)
    if (training_to_save.data_type() == pt_data::data_type::CUSTOM)
    {
        connect(date_time_edit, &QDateTimeEdit::dateTimeChanged, this, &edit_activity_dialog::time_changed);
        time_changed(date_time_edit->dateTime());
    }
    else
    {
        date_time_edit->setEnabled(false);
    }

    return time_layout;
}

QLayout *edit_activity_dialog::create_duration()
{
    QLabel *label = new QLabel(tr("&Duration:"));
    label->setMinimumWidth(label_width);

    QSpinBox *hours = new QSpinBox;
    hours->setMinimum(0);
    hours->setValue(training_to_save.duration().hours());

    QSpinBox *minutes = new QSpinBox;
    minutes->setMinimum(0);
    minutes->setMaximum(59);
    minutes->setValue(training_to_save.duration().minutes());

    QSpinBox *seconds = new QSpinBox;
    seconds->setMinimum(0);
    seconds->setMaximum(59);
    seconds->setValue(training_to_save.duration().seconds());

    label->setBuddy(hours);

    QHBoxLayout *duration_layout = new QHBoxLayout;
    duration_layout->addWidget(label);
    duration_layout->addWidget(hours);
    duration_layout->addWidget(minutes);
    duration_layout->addWidget(seconds);

    // Changes to imported activites are not allowed (now?)
    if (training_to_save.data_type() == pt_data::data_type::CUSTOM)
    {
        connect(hours, &QSpinBox::valueChanged, this, &edit_activity_dialog::hours_changed);
        connect(minutes, &QSpinBox::valueChanged, this, &edit_activity_dialog::minutes_changed);
        connect(seconds, &QSpinBox::valueChanged, this, &edit_activity_dialog::seconds_changed);
    }
    else
    {
        hours->setEnabled(false);
        minutes->setEnabled(false);
        seconds->setEnabled(false);
    }

    return duration_layout;
}

QLayout *edit_activity_dialog::create_mood()
{
    QLabel *label = new QLabel(tr("&Mood:"));
    label->setMinimumWidth(label_width);

    mood_edit = new QLineEdit;
    mood_edit->setText(QString::fromStdString(training_to_save.mood()));

    label->setBuddy(mood_edit);
    QHBoxLayout *mood_layout = new QHBoxLayout;
    mood_layout->addWidget(label);
    mood_layout->addWidget(mood_edit);

    return mood_layout;
}

QLayout *edit_activity_dialog::create_bike_selection()
{
    QLabel *label = new QLabel(tr("&Bike:"));
    label->setMinimumWidth(label_width);

    label->setBuddy(bikes_combo_box);
    bikes_combo_box->addItem("", -1);

    for (auto &bike : bikes->bikes())
    {
        bikes_combo_box->addItem(QString::fromStdString(bike.name()), bike.id());
        if (original_training.bike_id() == bike.id())
        {
            bikes_combo_box->setCurrentIndex(bikes_combo_box->count() - 1);
        }
    }

    QHBoxLayout *bikes_layout = new QHBoxLayout;
    bikes_layout->addWidget(label);
    bikes_layout->addWidget(bikes_combo_box, 1);

    connect(bikes_combo_box, &QComboBox::currentIndexChanged, this, &edit_activity_dialog::bike_changed);

    return bikes_layout;
}

QLayout *edit_activity_dialog::create_comment()
{
    QLabel *label = new QLabel(tr("&Comment:"));
    label->setMinimumWidth(label_width);

    comment_edit = new QPlainTextEdit;
    comment_edit->setPlainText(QString::fromStdString(training_to_save.comment()));
    comment_edit->setLineWidth(50);
    comment_edit->setMinimumHeight(200);

    label->setBuddy(comment_edit);
    QHBoxLayout *comment_layout = new QHBoxLayout;
    comment_layout->addWidget(label);
    comment_layout->addWidget(comment_edit);

    return comment_layout;
}

QLayout *edit_activity_dialog::create_buttons()
{
    QHBoxLayout *buttons_layout = new QHBoxLayout;
    buttons_layout->setAlignment(Qt::AlignmentFlag::AlignRight);

    if (training_to_save.data_type() == pt_data::data_type::CUSTOM)
    {
        QPushButton *delete_button = new QPushButton(tr("Delete activity"));
        delete_button->setAutoDefault(false);
        connect(delete_button, &QPushButton::clicked, this, &edit_activity_dialog::delete_activity);
        buttons_layout->addWidget(delete_button, 0, Qt::AlignLeft);
    }

    QPushButton *cancel_button = new QPushButton(tr("Cancel"));
    cancel_button->setAutoDefault(false);
    connect(cancel_button, &QPushButton::clicked, this, &edit_activity_dialog::cancel);
    buttons_layout->addWidget(cancel_button);

    QPushButton *save_button = new QPushButton(tr("Save"));
    save_button->setDefault(true);
    connect(save_button, &QPushButton::clicked, this, &edit_activity_dialog::save);
    buttons_layout->addWidget(save_button);

    return buttons_layout;
}

void edit_activity_dialog::activity_changed(const QString &activity_name)
{
    for (auto &activity : activity_type_translation::get_all_activities())
    {
        if (activity.second == activity_name)
        {
            training_to_save.set_activity_type((pt_data::activity_type)activity.first);
            break;
        }
    }
}

void edit_activity_dialog::hours_changed(const int &hours)
{
    training_to_save.mutable_duration()->set_hours(hours);
}

void edit_activity_dialog::minutes_changed(const int &minutes)
{
    training_to_save.mutable_duration()->set_minutes(minutes);
}

void edit_activity_dialog::seconds_changed(const int &seconds)
{
    training_to_save.mutable_duration()->set_seconds(seconds);
}

void edit_activity_dialog::time_changed(const QDateTime &date_time)
{
    training_to_save.mutable_date_time()->set_year(date_time.date().year());
    training_to_save.mutable_date_time()->set_month(date_time.date().month());
    training_to_save.mutable_date_time()->set_day(date_time.date().day());
    training_to_save.mutable_date_time()->set_hours(date_time.time().hour());
    training_to_save.mutable_date_time()->set_minutes(date_time.time().minute());
    training_to_save.mutable_date_time()->set_seconds(date_time.time().second());
}

void edit_activity_dialog::save()
{
    QString save_date_time;
    date_time_to_qstring(training_to_save.date_time(), Q_DATE_TIME_FORMAT, save_date_time);

    QString original_date_time;
    date_time_to_qstring(original_training.date_time(), Q_DATE_TIME_FORMAT, original_date_time);

    if (save_date_time != original_date_time &&
        training_saver::training_exist(training_to_save, data_path.toStdString()))
    {
        auto message = tr("An activity for %1 already exists.\n\nPlease change time or edit existing activity.")
                           .arg(save_date_time);
        write_to_log(message);
        QMessageBox message_box(QMessageBox::Information, tr("Activity already exits"), message);
        message_box.exec();
    }
    else
    {
        training_to_save.set_mood(mood_edit->text().toStdString());
        training_to_save.set_comment(comment_edit->toPlainText().toStdString());
        training_to_save.set_is_new(false);
        auto result = training_saver::save_training(training_to_save, data_path.toStdString());

        if (result != training_saver::return_code::OK)
        {
            auto message = tr("Saving failed. Please try again. (ERROR: %1)").arg(result);
            write_to_log(message);
            QMessageBox message_box(QMessageBox::Warning, tr("Save failed"), message);
            message_box.exec();
        }
        else
        {
            if (save_date_time != original_date_time)
            {
                training_saver::delete_training(original_training, data_path.toStdString());
            }

            update_bikes();
            QString new_date_string;
            date_time_to_qstring(training_to_save.date_time(), Q_SORTABLE_DATE_FORMAT, new_date_string);
            new_date = new_date_string.toLongLong();

            was_canceled = false;
            this->close();
        }
    }
}

void edit_activity_dialog::update_bikes()
{
    if (original_training.bike_id() != training_to_save.bike_id())
    {
        for (auto &bike : *bikes->mutable_bikes())
        {
            if (bike.id() == training_to_save.bike_id())
            {
                float new_distance = bike.chain_km() + training_to_save.distance();
                bike.set_chain_km(new_distance);
            }
            else if (bike.id() == original_training.bike_id())
            {
                float new_distance = bike.chain_km() - training_to_save.distance();
                bike.set_chain_km(new_distance);
            }
        }

        pt_data::save_bikes(data_path.toStdString(), *bikes);
    }
}

void edit_activity_dialog::bike_changed(const int &index)
{
    int bike_id = bikes_combo_box->currentData().toInt();

    training_to_save.set_bike_id(bike_id);
}

void edit_activity_dialog::cancel()
{
    this->close();
}

void edit_activity_dialog::delete_activity()
{

    auto message = tr("Do you really want to delete this activity?");

    auto result = QMessageBox::question(this, tr("Delete activity?"), tr("Do you really want to delete this activity?"),
                                        QMessageBox::Yes | QMessageBox::No);

    if (result == QMessageBox::Yes)
    {
        auto result = training_saver::delete_training(original_training, data_path.toStdString());
        if (result.value() != 0)
        {
            auto message = tr("Failed to delete activity. (ERROR: %1)").arg(QString::fromStdString(result.message()));
            write_to_log(message);
            QMessageBox message_box(QMessageBox::Warning, tr("Deletion failed"), message);
            message_box.exec();
        }
        else
        {
            was_canceled = false;
            this->close();
        }
    }
}

void edit_activity_dialog::write_to_log(const QString &message) const
{
    static QLoggingCategory category("Edit activity");
    qCInfo(category) << message;
}