#ifndef PO_NEWACTIVITYDIALOG_H
#define PO_NEWACTIVITYDIALOG_H

#include "../../../pt_proto/pt_data.pb.h"
#include <QDialog>

class QPlainTextEdit;
class QLineEdit;

class new_activity_dialog : public QDialog
{
    Q_OBJECT

  public:
    bool was_canceled;

  private:
    QLineEdit *mood_edit;
    QPlainTextEdit *comment_edit;
    const QString data_path;
    pt_data::training training_to_save;

  public:
    new_activity_dialog(const QString &data_path, const QDate &date, QWidget *parent = 0);

  private:
    void cancel();
    void save();

    void write_to_log(const QString &message) const;

  private: // Change event handler
    void activity_changed(const QString &activity_name);
    void hours_changed(const int &hours);
    void minutes_changed(const int &minutes);
    void seconds_changed(const int &seconds);
    void time_changed(const QDateTime &date_time);

  private: // Create widgets
    QLayout *create_title();
    QLayout *create_date_time(const QDate &date);
    QLayout *create_activity_type();
    QLayout *create_duration();
    QLayout *create_mood();
    QLayout *create_comment();
    QLayout *create_save_cancel();
};

#endif