#include "training_charts.hpp"

#include <QDateTimeAxis>
#include <QValueAxis>
#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>
#include <QtWidgets>

#include "../../training_data/activity_type.hpp"
#include "date_time.hpp"

training_charts::training_charts(QWidget *parent)
    : QWidget(parent), main_layout(0), speed_chart_view(0), current_hr_power_view(0), current_speed_view(0)
{
    main_layout = new QGridLayout(this);
    main_layout->setRowStretch(0, 1);
    main_layout->setRowStretch(1, 1);
    main_layout->setContentsMargins(0, 0, 0, 0);
}

void training_charts::load(const pt_data::training &training)
{
    load_hr_power(training);
    load_speed(training);
}

void training_charts::load_hr_power(const pt_data::training &training)
{
    std::vector<QLineSeries *> hr_power_series;

    int min = std::numeric_limits<int>().max();
    int max = std::numeric_limits<int>().min();

    if (contains_valid_data(training.power_samples()))
    {
        qint64 current_time_ms_power = date_time_to_msecs_since_epoch(training.date_time());
        QLineSeries *series = new QLineSeries();
        series->setColor(QColor(0xf6, 0xa6, 0x25));
        series->setName(tr("Power"));

        // remove spikes
        int step = 20;
        for (int i = 0; i < training.power_samples().size(); i += step)
        {
            int avg = 0;
            int j = i;
            int samples = 0;
            for (; j < (i + step) && j < training.power_samples().size(); ++j, ++samples)
            {
                avg += training.power_samples()[j];
            }

            avg = avg / samples;

            set_min_max(avg, min, max);

            for (j = i; j < (i + step) && j < training.power_samples().size(); ++j)
            {
                series->append(current_time_ms_power, avg);
                current_time_ms_power += 1000;
            }
        }
        /*
        for (int i = 0; i < training.samples.power.size(); ++i)
        {
            series->append(i, training.samples.power[i]);
        }
        */
        hr_power_series.push_back(series);
    }

    if (contains_valid_data(training.heart_rate_samples()))
    {
        qint64 current_time_ms_hr = date_time_to_msecs_since_epoch(training.date_time());
        QLineSeries *series = new QLineSeries();
        auto pen = series->pen();
        pen.setWidth(2);
        series->setPen(pen);
        series->setColor(QColor(0x99, 0xca, 0x53));
        series->setName(tr("HR"));

        for (auto &heart_rate : training.heart_rate_samples())
        {
            set_min_max((int)heart_rate, min, max);
            series->append(current_time_ms_hr, heart_rate);
            current_time_ms_hr += 1000;
        }

        hr_power_series.push_back(series);
    }

    if (hr_power_series.size() > 0)
    {
        QValueAxis *y_axis = new QValueAxis;
        y_axis->setLabelFormat("%i");
        y_axis->setMin(min > 10 ? min - 10 : min - 0.5);
        y_axis->setMax(max + 10);

        add_hr_power_chart_view(hr_power_series, y_axis);
    }
    else
    {
        auto group_box = new QGroupBox;
        auto layout = new QGridLayout;
        auto no_data_label = new QLabel(tr("No hr and power data available"));
        layout->addWidget(no_data_label, 0, 0, Qt::AlignCenter);
        group_box->setLayout(layout);
        replace_hr_power_view(group_box, 0);
    }
}

void training_charts::load_speed(const pt_data::training &training)
{
    if (activity_type::is_running_activity(training.activity_type()))
    {
        create_running_speed_chart(training);
    }
    else if (activity_type::is_swim_activity(training.activity_type()))
    {
        create_swim_speed_chart(training);
    }
    else
    {
        create_default_speed_chart(training);
    }
}

void training_charts::create_default_speed_chart(const pt_data::training &training)
{
    if (contains_valid_data(training.speed_samples()))
    {
        QLineSeries *speed_series = new QLineSeries();
        speed_series->setName(tr("Speed (km/h)"));

        float min = std::numeric_limits<float>().max();
        float max = std::numeric_limits<float>().min();
        qint64 current_time_ms = date_time_to_msecs_since_epoch(training.date_time());
        for (auto &speed : training.speed_samples())
        {
            set_min_max(speed, min, max);
            speed_series->append(current_time_ms, speed);
            current_time_ms += 1000;
        }

        QValueAxis *y_axis = new QValueAxis;
        y_axis->setLabelFormat("%i");
        y_axis->setMin(min - 0.5); // Do not show -1
        y_axis->setMax(max + 1);   // enough space to hover peaks

        add_speed_chart_view(speed_series, y_axis);
    }
    else
    {
        add_no_speed_chart_view();
    }
}

void training_charts::add_no_speed_chart_view()
{
    auto group_box = new QGroupBox;
    auto layout = new QGridLayout;
    auto no_data_label = new QLabel(tr("No speed data available"));
    layout->addWidget(no_data_label, 0, 0, Qt::AlignCenter);
    group_box->setLayout(layout);
    replace_speed_view(group_box, 0);
}

void training_charts::create_running_speed_chart(const pt_data::training &training)
{
    if (contains_valid_data(training.speed_samples()))
    {
        QLineSeries *speed_series = new QLineSeries();
        speed_series->setName(tr("Speed (min/km)"));
        speed_series->setObjectName("run_speed");
        qint64 current_time_ms = date_time_to_msecs_since_epoch(training.date_time());
        for (auto &speed : training.speed_samples())
        {
            double total_miliseconds = 600000; // 10 minutes/km
            if (speed != 0)
            {
                total_miliseconds = 3600 / speed * 1000;
            }

            speed_series->append(current_time_ms, total_miliseconds);
            current_time_ms += 1000;
        }

        QDateTimeAxis *y_axis = new QDateTimeAxis;
        y_axis->setTickCount(10);
        y_axis->setFormat("mm:ss");

        // Max 10 minutes/km to keep graph small
        y_axis->setMax(QDateTime(QDate(1970, 1, 1), QTime(0, 10, 0, 0), Qt::UTC));

        // https://en.wikipedia.org/wiki/1000_metres
        // WR=2:11.96
        // Minimum 2 minutes should be sufficient.
        y_axis->setMin(QDateTime(QDate(1970, 1, 1), QTime(0, 2, 0, 0), Qt::UTC));

        add_speed_chart_view(speed_series, y_axis);
    }
    else
    {
        add_no_speed_chart_view();
    }
}

void training_charts::create_swim_speed_chart(const pt_data::training &training)
{
    if (contains_valid_data(training.speed_samples()))
    {
        QLineSeries *speed_series = new QLineSeries();
        speed_series->setName(tr("Speed (min/100m)"));
        speed_series->setObjectName("swim_speed");
        qint64 current_time_ms = date_time_to_msecs_since_epoch(training.date_time());
        for (auto &speed : training.speed_samples())
        {
            double total_miliseconds = 42000; // 7 minutes/100m
            if (speed != 0)
            {
                total_miliseconds = 3600 / speed * 100;
            }

            speed_series->append(current_time_ms, total_miliseconds);
            current_time_ms += 1000;
        }

        QDateTimeAxis *y_axis = new QDateTimeAxis;
        y_axis->setTickCount(10);
        y_axis->setFormat("mm:ss");

        // 7 minutes/100m should be enough!?
        y_axis->setMax(QDateTime(QDate(1970, 1, 1), QTime(0, 7, 0, 0), Qt::UTC));

        add_speed_chart_view(speed_series, y_axis);
    }
    else
    {
        add_no_speed_chart_view();
    }
}

void training_charts::add_speed_chart_view(QLineSeries *speed_series, QAbstractAxis *y_axis)
{
    auto pen = speed_series->pen();
    pen.setWidth(2);
    speed_series->setPen(pen);
    speed_series->setColor(QColor(0x20, 0x9f, 0xdf));

    auto speed_chart = new QChart();
    speed_chart->setAcceptHoverEvents(true);
    speed_chart->addSeries(speed_series);

    speed_chart_view = new QChartView(speed_chart);
    speed_chart_view->setRenderHint(QPainter::Antialiasing);
    speed_chart_view->setContentsMargins(0, 0, 0, 0);

    QDateTimeAxis *x_axis = new QDateTimeAxis;
    set_axis_style(x_axis, y_axis);

    speed_chart_view->setChart(speed_chart);
    speed_chart->addAxis(x_axis, Qt::AlignBottom);
    speed_series->attachAxis(x_axis);
    speed_chart->addAxis(y_axis, Qt::AlignLeft);
    speed_series->attachAxis(y_axis);

    replace_speed_view(speed_chart_view, 1);
    connect(speed_series, &QLineSeries::hovered, this, &training_charts::speed_tooltip);
}

void training_charts::add_hr_power_chart_view(std::vector<QLineSeries *> &hr_power_series, QAbstractAxis *y_axis)
{
    auto hr_power_chart = new QChart();
    hr_power_chart->setAcceptHoverEvents(true);
    for (auto &series : hr_power_series)
    {
        hr_power_chart->addSeries(series);
    }

    auto hr_power_chart_view = new QChartView(hr_power_chart);
    hr_power_chart_view->setRenderHint(QPainter::Antialiasing);
    hr_power_chart_view->setContentsMargins(0, 0, 0, 0);

    QDateTimeAxis *x_axis = new QDateTimeAxis;
    set_axis_style(x_axis, y_axis);

    hr_power_chart_view->setChart(hr_power_chart);
    hr_power_chart->addAxis(x_axis, Qt::AlignBottom);
    hr_power_chart->addAxis(y_axis, Qt::AlignLeft);

    // Series axis must be set after chart axis!
    for (auto &series : hr_power_series)
    {
        series->attachAxis(x_axis);
        series->attachAxis(y_axis);
        connect(series, &QLineSeries::hovered, this, &training_charts::hr_power_tooltip);
    }

    replace_hr_power_view(hr_power_chart_view, 1);
}

void training_charts::set_axis_style(QDateTimeAxis *x_axis, QAbstractAxis *y_axis)
{
    x_axis->setTickCount(10);
    x_axis->setFormat("hh:mm:ss");
    auto x_font = x_axis->labelsFont();
    x_font.setPointSize(x_font.pointSize() - 3);
    x_axis->setLabelsFont(x_font);

    y_axis->setTruncateLabels(false);
    auto y_font = y_axis->labelsFont();
    y_font.setPointSize(y_font.pointSize() - 1);
    y_axis->setLabelsFont(y_font);
}

void training_charts::hr_power_tooltip(QPointF point, bool /*state*/)
{
    QString tooltip;
    tooltip.append(tr("%1").arg(point.y()));
    auto pos = QCursor::pos();
    pos.setY(pos.y() - 50);
    QToolTip::showText(pos, tooltip);

    // ToDo: Get multiple series working again.
    // Won't work with datetime y-Axis ...
    // show_tooltip(point, hr_power_chart_view);
}

void training_charts::speed_tooltip(QPointF point, bool /*state*/)
{
    show_tooltip(point, speed_chart_view);
}

void training_charts::show_tooltip(const QPointF &point, const QChartView *const chartView)
{
    QString tooltip;

    for (auto &series : chartView->chart()->series())
    {
        auto lineSeries = (QLineSeries *)series;

        if (lineSeries->objectName() == "run_speed" || lineSeries->objectName() == "swim_speed")
        {
            double totalSeconds = point.y() / 1000;
            int minutes = (int)totalSeconds / 60;
            int seconds = totalSeconds - (minutes * 60);
            QTime time(0, minutes, seconds, 0);

            tooltip.append(tr("%1: %2\n").arg(lineSeries->name()).arg(time.toString("mm:ss")));
        }
        else
        {
            tooltip.append(tr("%1: %2\n").arg(lineSeries->name()).arg(point.y()));
        }
    }

    // Lazy way to remove last /n
    tooltip.remove(tooltip.size() - 1, 1);

    auto pos = QCursor::pos();
    pos.setY(pos.y() - 50);
    QToolTip::showText(pos, tooltip);
}

void training_charts::write_to_log(const QString &message) const
{
    static QLoggingCategory category("Charts");
    qCInfo(category) << message;
}

void training_charts::replace_hr_power_view(QWidget *new_view, int stretch)
{
    if (current_hr_power_view != 0)
    {
        main_layout->replaceWidget(current_hr_power_view, new_view);
        current_hr_power_view->deleteLater();
    }
    else
    {
        main_layout->addWidget(new_view, 0, 0);
    }

    main_layout->setRowStretch(0, stretch);

    current_hr_power_view = new_view;
}

void training_charts::replace_speed_view(QWidget *new_view, int stretch)
{
    if (current_speed_view != 0)
    {
        main_layout->replaceWidget(current_speed_view, new_view);
        current_speed_view->deleteLater();
    }
    else
    {
        main_layout->addWidget(new_view, 1, 0);
    }

    main_layout->setRowStretch(1, stretch);

    current_speed_view = new_view;
}