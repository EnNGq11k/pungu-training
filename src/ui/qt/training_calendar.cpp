#include "training_calendar.hpp"

#include <QtGui/QPainter>

#include "../../training_data/training_cache.hpp"
#include "activity_translation.hpp"
#include "date_time.hpp"

training_calendar::training_calendar(std::shared_ptr<training_cache> cache, QWidget *parent)
    : QCalendarWidget(parent), cache(cache)
{
    setNavigationBarVisible(true);
    setFirstDayOfWeek(Qt::Monday);

    // set header view
    QTextCharFormat format;
    format.setForeground(QBrush(QColor(0x00, 0x84, 0xbb), Qt::SolidPattern));
    format.setBackground(QBrush(QColor(0xdd, 0xdd, 0xdd), Qt::SolidPattern));
    setWeekdayTextFormat(Qt::Saturday, format);
    setWeekdayTextFormat(Qt::Sunday, format);
    setHeaderTextFormat(format);

    // set selected date format
    selectedDateFormat.setForeground(QBrush(QColor(0x00, 0x00, 0x00), Qt::SolidPattern));
    selectedDateFormat.setBackground(QBrush(QColor(0x8a, 0xc4, 0xdd), Qt::SolidPattern));

    // set current date format
    currentDateFormat.setForeground(QBrush(QColor(0x00, 0x00, 0x00), Qt::SolidPattern));
    currentDateFormat.setBackground(QBrush(QColor(0xc3, 0xc3, 0xc3), Qt::SolidPattern));

    // set valid date format
    validDateFormat.setForeground(QBrush(QColor(0x00, 0x00, 0x00), Qt::SolidPattern));
    validDateFormat.setBackground(QBrush(QColor(0xff, 0xff, 0xff), Qt::SolidPattern));

    // set invalid date format
    invalidDateFormat.setForeground(QBrush(QColor(0xa3, 0xa3, 0xa3), Qt::SolidPattern));
    invalidDateFormat.setBackground(QBrush(QColor(0xff, 0xff, 0xff), Qt::SolidPattern));

    setHorizontalHeaderFormat(QCalendarWidget::ShortDayNames);
    setVerticalHeaderFormat(QCalendarWidget::NoVerticalHeader);
    setGridVisible(true);
}

void training_calendar::paintCell(QPainter *painter, const QRect &rect, QDate date) const
{
    QRect rectN = rect;
    painter->save();
    if (date == selectedDate())
    {
        painter->fillRect(rectN, selectedDateFormat.background());
        painter->setPen(selectedDateFormat.foreground().color());
    }
    else if (date == QDate::currentDate())
    {
        painter->fillRect(rectN, currentDateFormat.background());
        painter->setPen(currentDateFormat.foreground().color());
    }
    else if (date.month() == monthShown())
    {
        painter->fillRect(rectN, validDateFormat.background());
        painter->setPen(validDateFormat.foreground().color());
    }
    else
    {
        painter->fillRect(rectN, invalidDateFormat.background());
        painter->setPen(invalidDateFormat.foreground().color());
    }

    painter->drawText(rectN.x() + 2, rectN.y() + 15, QString::number(date.day()));

    if (cache)
    {
        QString dateTimeString = date.toString(Q_SORTABLE_DATE_FORMAT);
        long long sortable_date_time = atoll(dateTimeString.toStdString().c_str());
        auto trainings = cache->get_trainings_forDay(sortable_date_time);

        int x = rectN.x() + 2;
        int y = rectN.y() + 34;
        for (auto &training : trainings)
        {
            QString activityType = activity_type_translation::translate_to_emoji(training.activity_type());
            QString duration;
            duration_to_qstring(training.duration(), duration);
            painter->drawText(x, y, activityType + " " + duration);
            y += 16;
        }
    }

    painter->restore();
}