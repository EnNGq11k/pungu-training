#ifndef TRAINING_SUMMARY_DETAIL_H
#define TRAINING_SUMMARY_DETAIL_H

#include <QStandardItem>
#include <QTreeView>

namespace pt_data
{
class training;
class training_bikes;
} // namespace pt_data

class training_summary_detail : public QTreeView
{
    template <class T> void append_row(const QString &name, const T &value, QStandardItemModel *model)
    {
        QList<QStandardItem *> items;
        items.append(new QStandardItem(name));
        items.append(new QStandardItem(tr("%1").arg(value)));

        model->appendRow(items);
    }

    template <class T> QString append(const T &value, const QString &string)
    {
        return tr("%1 %2").arg(value).arg(string);
    }

    Q_OBJECT

  public:
    explicit training_summary_detail(QWidget *parent = 0);
    void load(const std::map<long long, pt_data::training> &training_data, const uint &year,
              const pt_data::training_bikes &bikes);

  private:
    void add_details(const std::map<long long, pt_data::training> &training_data, const uint &year,
                     QStandardItemModel *model);
    void add_bikes(const pt_data::training_bikes &bikes, QStandardItemModel *model);
};

#endif