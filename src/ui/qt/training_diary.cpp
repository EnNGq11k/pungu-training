#include "training_diary.hpp"

#include <QtWidgets>
#include <memory>

#include "../../pt_proto/pt_bikes.pb.h"
#include "../../pt_proto/pt_data.pb.h"
#include "../../training_data/pt_bikes_helper.hpp"
#include "../../training_data/training_cache.hpp"
#include "../../training_data/training_saver.hpp"

#include "dialogs/edit_activity_dialog.hpp"
#include "dialogs/new_activity_dialog.hpp"

#include "date_time.hpp"
#include "training_calendar.hpp"
#include "training_list.hpp"
#include "training_summary_view.hpp"
#include "training_view.hpp"

training_diary::training_diary(QWidget *parent)
    : QWidget(parent), training_list_view(0), training_details(new training_view),
      summary_widget(new training_summary_view), cache(std::make_shared<training_cache>()),
      bikes(std::make_shared<pt_data::training_bikes>())
{
    training_list_view = new training_list(cache);
    connect(training_list_view, &QTreeView::doubleClicked, this, &training_diary::edit_activity);
    connect(training_list_view->selectionModel(), &QItemSelectionModel::selectionChanged, this,
            &training_diary::training_selected);

    calendar = new training_calendar(cache);
    connect(calendar, &QCalendarWidget::clicked, this, &training_diary::calendar_clicked);
    connect(calendar, &QCalendarWidget::activated, this, &training_diary::new_training);

    QVBoxLayout *diary_layout = new QVBoxLayout();
    diary_layout->setContentsMargins(0, 10, 0, 0); // Hack allignment to summary_view...
    diary_layout->addWidget(calendar, 1);
    diary_layout->addWidget(training_list_view, 0);

    main_layout = new QHBoxLayout(this);
    main_layout->addLayout(diary_layout, 1);
    main_layout->addWidget(summary_widget, 1);
    main_layout->addWidget(training_details, 1);
    training_details->setVisible(false);
}

training_diary::~training_diary()
{
}

void training_diary::load_data(const QString &path, const int year, const qlonglong &date_time, bool refresh_cache)
{
    if (refresh_cache)
    {
        load_bikes(path);

        std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
        cache->init_cache(path.toStdString());
        std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
        write_to_log(
            tr("Init cache: %1 µs").arg(std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count()));
    }

    const std::map<long long, pt_data::training> &training_data = cache->get_all();
    write_to_log(tr("Found %1 activities").arg(training_data.size()));

    training_list_view->load_data(training_data, year, date_time);
    summary_widget->update(training_data, year, *bikes);
}

void training_diary::load_bikes(const QString &path)
{
    // Load here to enable bike overview later
    if (!path.isEmpty())
    {
        pt_data::load_bikes(path.toStdString(), *bikes);
    }
}

void training_diary::edit_activity(const QModelIndex & /*index*/)
{
    qlonglong date_time = training_list_view->get_selected_date_time();

    if (date_time != 0)
    {
        const pt_data::training &training = cache->get_training(date_time);
        auto data_path = get_data_path();
        edit_activity_dialog edit_dialog(training, bikes, data_path);
        edit_dialog.exec();

        if (!edit_dialog.was_canceled)
        {
            load_data(data_path, 0, edit_dialog.new_date, true);
        }
    }
}

void training_diary::training_selected(const QItemSelection & /*selected*/, const QItemSelection & /*deselected*/)
{
    qlonglong date_time = training_list_view->get_selected_date_time();

    if (date_time != 0)
    {
        summary_widget->setVisible(false);
        training_details->setVisible(true);

        const pt_data::training &training = cache->get_training(date_time);

        if (training.is_new())
        {
            auto training_to_save = training;

            // Update is_new on filesystem. Do not update cache right now to keep activity bold
            training_to_save.set_is_new(false);
            training_saver::save_training(training_to_save, get_data_path().toStdString());
        }

        training_details->load_data(training);
    }
    else
    {
        training_details->setVisible(false);
        summary_widget->setVisible(true);
    }
}

void training_diary::write_to_log(const QString &message) const
{
    static QLoggingCategory category("Diary");
    qCInfo(category) << message;
}

QDate training_diary::get_selected_date() const
{
    return calendar->selectedDate();
}

void training_diary::calendar_clicked(const QDate &date)
{
    QString dateTimeString = date.toString(Q_SORTABLE_DATE_FORMAT);
    qlonglong sortable_date_time = dateTimeString.toLongLong();

    if (cache->get_trainings_forDay(sortable_date_time).empty())
    {
        load_data(get_data_path(), date.year(), 0, false);
        training_list_view->select_item(0);
    }
    else
    {
        load_data(get_data_path(), 0, sortable_date_time, false);
        training_list_view->select_item(sortable_date_time);
    }
}

void training_diary::new_training(const QDate &date)
{
    new_activity_dialog dialog(get_data_path(), date, this);
    dialog.setWindowModality(Qt::WindowModal);
    dialog.setMinimumHeight(100);
    dialog.setMinimumWidth(100);
    dialog.exec();

    if (!dialog.was_canceled)
    {
        load_data(get_data_path(), calendar->yearShown(), 0, true);
    }
}

QString training_diary::get_data_path()
{
#ifdef QT_DEBUG
    QSettings settings("PunguTraining", "PunguTraining_DEBUG");
#else
    QSettings settings("PunguTraining", "PunguTraining");
#endif

    return settings.value("data_path").toString();
}