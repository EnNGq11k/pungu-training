#include "main_window_qt.hpp"

#include <filesystem>

#include <QMenu>
#include <QMenuBar>
#include <QtConcurrent/QtConcurrentRun>
#include <QtWidgets>

#include "../../training_data/proto_parser.hpp"
#include "../../usb/device_check.hpp"
#include "dialogs/edit_bikes_dialog.hpp"
#include "logging/logger.hpp"
#include "polar_download.hpp"
#include "training_diary.hpp"

// Define in .pro file.
#ifndef APP_VERSION
#define APP_VERSION "0"
#endif

main_window_qt::main_window_qt() : diary(new training_diary), data_path(QString())
{
    Qt::WindowFlags flags;
    flags |= Qt::WindowMaximizeButtonHint | Qt::WindowMinimizeButtonHint | Qt::WindowCloseButtonHint;
    setWindowFlags(flags);
    read_settings();
    logger::init_logging(data_path);
    write_to_log("Starting pungu");

    // Main
    auto main_layout = new QVBoxLayout(this);
    main_layout->setContentsMargins(5, 5, 5, 5);
    main_layout->setSpacing(0);

    // Diary and graph
    auto training_view = new QHBoxLayout;
    training_view->setContentsMargins(0, 0, 0, 0);
    training_view->addWidget(diary);

    main_layout->addLayout(training_view, 1);

    QMenuBar *menuBar = new QMenuBar();
    QMenu *menu = menuBar->addMenu(tr("&File"));
    menu->addAction(tr("New activity"), this, &main_window_qt::new_activity);
    menu->addAction(tr("Download from device"), this, &main_window_qt::download);
    menu->addSeparator();
    menu->addAction(tr("Edit bikes"), this, &main_window_qt::edit_bikes);
    menu->addSeparator();
    menu->addAction(tr("Reload"), this, &main_window_qt::load);
    menu->addSeparator();
    menu->addAction(tr("Change data folder"), this, &main_window_qt::change_data_path);

#if defined(__APPLE__)
    // Show "Emoji & Symbols" on macOS
    menuBar->addMenu(tr("&Edit"));
#endif

    main_layout->setMenuBar(menuBar);
    QString app_title = tr("%1 %2").arg(tr("Pungu Training")).arg(APP_VERSION);

#ifdef QT_DEBUG
    app_title += " DEBUG";
    write_to_log(">> DEBUG BUILD <<");
#endif

    setWindowTitle(app_title);
    load();
}

void main_window_qt::load()
{
    write_to_log("Loading data");
    diary->load_data(data_path, QDate::currentDate().year(), 0, true);
}

void main_window_qt::new_activity()
{
    diary->new_training(diary->get_selected_date());
}

void main_window_qt::download()
{
    auto available_devices = device_check::check_for_polar_devices();

    if (!available_devices.v1_available && !available_devices.v2_available)
    {
        QMessageBox message_box(QMessageBox::Information, tr("No device found"),
                                tr("No polar device found. Please check connection."));
        message_box.exec();
        return;
    }

    polar_download downloader(data_path.toStdString());

    QFutureWatcher<void> futureWatcher;

    QProgressDialog progress(tr("Downloading"), tr("Cancel"), 0, 0, this);
    progress.setWindowModality(Qt::WindowModal);
    progress.setMinimumWidth(300);

    connect(&downloader, &polar_download::update_progress, &progress, &QProgressDialog::setValue);
    connect(&downloader, &polar_download::update_min_max, &progress, &QProgressDialog::setRange);
    connect(&downloader, &polar_download::update_text, &progress, &QProgressDialog::setLabelText);
    connect(&futureWatcher, &QFutureWatcher<void>::finished, &progress, &QProgressDialog::close);

    connect(&progress, &QProgressDialog::canceled, this, [&futureWatcher, &downloader]() {
        futureWatcher.cancel();
        downloader.cancel();
    });

    auto downloads =
        QtConcurrent::run([&downloader, &available_devices]() { downloader.download_from_devices(available_devices); });

    futureWatcher.setFuture(downloads);

    progress.exec();

    if (progress.wasCanceled())
    {
        disconnect(&downloader, &polar_download::update_progress, &progress, &QProgressDialog::setValue);
        disconnect(&downloader, &polar_download::update_min_max, &progress, &QProgressDialog::setRange);
        disconnect(&downloader, &polar_download::update_text, &progress, &QProgressDialog::setLabelText);

        progress.setLabelText(tr("Please wait for usb device to cancel download..."));
        progress.setCancelButtonText(QString());
        progress.setRange(0, 0);

        if (!futureWatcher.isFinished())
        {
            progress.exec();
        }
    }

    futureWatcher.waitForFinished();
    load();
}

void main_window_qt::closeEvent(QCloseEvent *event)
{
#ifdef QT_DEBUG
    QSettings settings("PunguTraining", "PunguTraining_DEBUG");
#else
    QSettings settings("PunguTraining", "PunguTraining");
#endif

    settings.setValue("data_path", data_path);
    settings.setValue("geometry", saveGeometry());

    QDialog::closeEvent(event);
}

void main_window_qt::read_settings()
{
#ifdef QT_DEBUG
    QSettings settings("PunguTraining", "PunguTraining_DEBUG");
#else
    QSettings settings("PunguTraining", "PunguTraining");
#endif

    data_path = settings.value("data_path").toString();
    restoreGeometry(settings.value("geometry").toByteArray());
    set_data_path();
}

void main_window_qt::set_data_path()
{
    if (data_path.isEmpty())
    {
#if defined(__APPLE__) || defined(_WIN32)
        data_path = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
#elif defined(__linux__)
        data_path = QString(getenv("HOME")) + "/.pungu/";
#endif
    }

    write_to_log(tr("Data path: %1").arg(data_path));
}

void main_window_qt::change_data_path()
{
    QString directory = QFileDialog::getExistingDirectory(this, tr("Select data folder"), data_path,
                                                          QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);

    if (!directory.isEmpty())
    {
        data_path = directory;
        write_to_log(tr("New data path: %1").arg(data_path));
        load();
    }
}

void main_window_qt::edit_bikes()
{
    edit_bikes_dialog edit_bikes_dialog(data_path);
    edit_bikes_dialog.exec();

    if (!edit_bikes_dialog.was_canceled)
    {
        load();
    }
}

void main_window_qt::write_to_log(const QString &message) const
{
    static QLoggingCategory category("Main window");
    qCInfo(category) << message;
}

void main_window_qt::keyPressEvent(QKeyEvent *event)
{
    // Do not close on escape key
    if (event->key() != Qt::Key_Escape)
    {
        QDialog::keyPressEvent(event);
    }
}