#ifndef TRAINING_CALENDAR_H
#define TRAINING_CALENDAR_H

#include <QCalendarWidget>
#include <QTextCharFormat>

class training_cache;

// https://github.com/siyueshiqi/LAN_IM/blob/master/IMClient/view/CustomCalendar.h
class training_calendar : public QCalendarWidget
{
    Q_OBJECT

  private:
    QTextCharFormat currentDateFormat;
    QTextCharFormat selectedDateFormat;
    QTextCharFormat validDateFormat;
    QTextCharFormat invalidDateFormat;

    std::shared_ptr<training_cache> cache;

  public:
    explicit training_calendar(std::shared_ptr<training_cache> cache, QWidget *parent = 0);

  private:
    virtual void paintCell(QPainter *painter, const QRect &rect, QDate date) const override final;
};

#endif