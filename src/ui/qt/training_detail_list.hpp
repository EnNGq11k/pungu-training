#ifndef TRAINING_DETAIL_LIST_H
#define TRAINING_DETAIL_LIST_H

#include <QStandardItem>
#include <QTreeView>

namespace pt_data
{
class training;
}

class training_detail_list : public QTreeView
{
    template <class T> void append_row(const QString &name, const T &value, QStandardItemModel *model)
    {
        QList<QStandardItem *> items;
        items.append(new QStandardItem(name));

        if (!name.isEmpty())
        {
            QString s = tr("%1").arg(value);

            if (s.isEmpty() || s == "0")
            {
                items.append(new QStandardItem("--"));
            }
            else
            {
                items.append(new QStandardItem(s));
            }
        }

        model->appendRow(items);
    }

    Q_OBJECT

  public:
    explicit training_detail_list(QWidget *parent = 0);
    void load(const pt_data::training &training);
};

#endif