#include "training_summary_detail.hpp"

#include "../../pt_proto/pt_bikes.pb.h"
#include "../../training_data/activity_type.hpp"
#include "../../training_data/duration.hpp"

training_summary_detail::training_summary_detail(QWidget *parent) : QTreeView(parent)
{
    setModel(new QStandardItemModel(this));
}

void training_summary_detail::load(const std::map<long long, pt_data::training> &training_data, const uint &year,
                                   const pt_data::training_bikes &bikes)
{
    auto model = static_cast<QStandardItemModel *>(this->model());
    model->clear();

    add_details(training_data, year, model);
    add_bikes(bikes, model);

    for (int i = 0; i < model->columnCount(); ++i)
    {
        resizeColumnToContents(i);
    }
}

void training_summary_detail::add_details(const std::map<long long, pt_data::training> &training_data, const uint &year,
                                          QStandardItemModel *model)
{
    QStringList labels = {tr("Name"), tr("Value")};
    model->setHorizontalHeaderLabels(labels);

    int total_activities = 0;

    int total_distance = 0;
    duration total_duration;

    int run_activites = 0;
    float run_distance = 0;
    duration run_duration;

    int bike_activities = 0;
    float bike_distance = 0;
    duration bike_duration;

    int swim_activites = 0;
    float swim_distance = 0;
    duration swim_duration;

    int other_activities = 0;
    duration other_duration;

    for (auto &training_pair : training_data)
    {
        auto training = &training_pair.second;

        if (year == 0 || training->date_time().year() == year)
        {
            ++total_activities;

            total_distance += training->distance();
            total_duration += training->duration();

            if (activity_type::is_running_activity(training->activity_type()))
            {
                ++run_activites;
                run_distance += training->distance();
                run_duration += training->duration();
            }
            else if (activity_type::is_bike_activity(training->activity_type()))
            {
                ++bike_activities;
                bike_distance += training->distance();
                bike_duration += training->duration();
            }
            else if (activity_type::is_swim_activity(training->activity_type()))
            {
                ++swim_activites;
                swim_distance += training->distance();
                swim_duration += training->duration();
            }
            else
            {
                ++other_activities;
                other_duration += training->duration();
            }
        }
    }

    append_row(tr("%1").arg(year), "", model);

    append_row("", "", model);
    append_row(tr("Activities"), total_activities, model);
    append_row(tr("Distance"), append(total_distance, "km"), model);
    append_row(tr("Duration"), QString::fromStdString(total_duration.to_string()), model);

    append_row("", "", model);
    append_row(tr("Run"), "", model);
    append_row(tr("Activities"), run_activites, model);
    append_row(tr("Distance"), append(run_distance, "km"), model);
    append_row(tr("Duration"), QString::fromStdString(run_duration.to_string()), model);

    append_row("", "", model);
    append_row(tr("Bike"), "", model);
    append_row(tr("Activities"), bike_activities, model);
    append_row(tr("Distance"), append(bike_distance, "km"), model);
    append_row(tr("Duration"), QString::fromStdString(bike_duration.to_string()), model);

    append_row("", "", model);
    append_row(tr("Swim"), "", model);
    append_row(tr("Activities"), swim_activites, model);
    append_row(tr("Distance"), append(swim_distance, "km"), model);
    append_row(tr("Duration"), QString::fromStdString(swim_duration.to_string()), model);

    append_row("", "", model);
    append_row(tr("Other"), "", model);
    append_row(tr("Activities"), other_activities, model);
    append_row(tr("Duration"), QString::fromStdString(other_duration.to_string()), model);
}

void training_summary_detail::add_bikes(const pt_data::training_bikes &bikes, QStandardItemModel *model)
{
    append_row("", "", model);
    append_row(tr("Bike chain wear"), "", model);

    for (auto &bike : bikes.bikes())
    {
        append_row(QString::fromStdString(bike.name()), tr("%1").arg(bike.chain_km()), model);
    }
}