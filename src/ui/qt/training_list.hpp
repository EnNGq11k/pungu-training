#ifndef TRAINING_LIST_H
#define TRAINING_LIST_H

#include <QTreeView>
#include <memory>

class training_cache;

namespace pt_data
{
class training;
} // namespace pt_data

const int date_time_role = Qt::UserRole + 1;
const int is_new_role = Qt::UserRole + 2;
const int date_role = Qt::UserRole + 3;

class training_list : public QTreeView
{
    template <class T> QString number_to_qstring_zero_empty(T in)
    {
        if (in == 0)
        {
            return QString();
        }

        return tr("%1").arg(in);
    };

    Q_OBJECT

  private:
    std::shared_ptr<training_cache> cache;

  public:
    explicit training_list(std::shared_ptr<training_cache> cache, QWidget *parent = 0);
    void load_data(const std::map<long long, pt_data::training> &training_data, const int &year,
                   const qlonglong &date_time);

    qlonglong get_selected_date_time();
    void select_item(qlonglong sortable_date_time);

  private:
    void select_item(QModelIndex &index_to_select);
    qlonglong get_date_time_from_index(const QModelIndex &index);
    QModelIndex get_current_index();
};
#endif