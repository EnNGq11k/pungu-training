#include <cstdlib>
#include <filesystem>
#include <iostream>
#include <memory>
#include <ostream>
#include <string>

#include "../../usb/device_check.hpp"
#include "../../usb/polar_usb_v1.hpp"
#include "../../usb/polar_usb_v2.hpp"

int prepare_output(const std::string &path);
void download(polar_usb &usb, const std::string &out_path);

int main(int argc, char *argv[])
{
    std::string out_path = "pungu_user_data/";

    if (argc > 1)
    {
        out_path = argv[1];
    }

    if (prepare_output(out_path) != 0)
    {
        return 1;
    }

    auto devices = device_check::check_for_polar_devices();
    std::cout << "V1 Device: " << (devices.v1_available ? "true" : "false") << std::endl;
    std::cout << "V2 Device: " << (devices.v2_available ? "true" : "false") << std::endl;

#ifdef __APPLE__
    std::cout << "V2 SN    : " << devices.v2_serial_number << std::endl;
#endif

    if (devices.v1_available)
    {
        std::cout << "Connecting to v1 device..." << std::endl;
        polar_usb_v1 v1;
        download(v1, out_path);
    }

    if (devices.v2_available)
    {
        std::cout << "Connecting to v2 device..." << std::endl;
        polar_usb_v2 v2;

#ifdef __APPLE__
        v2.serial_number = devices.v2_serial_number;
#endif

        download(v2, out_path);
    }

    std::cout << "Done" << std::endl;
    return 0;
}

int prepare_output(const std::string &path)
{
    if (!std::filesystem::exists(path))
    {
        std::error_code error_code;
        std::filesystem::create_directories(path, error_code);
        if (error_code.value() != 0)
        {
            std::cerr << "Error creating folder [" << path << "]. Error code: " << error_code.value() << std::endl;
            return 1;
        }
    }
    else if (!std::filesystem::is_directory(path))
    {
        std::cerr << "Invalid output path [" << path << "]." << std::endl;
        return 1;
    }

    return 0;
}

void download(polar_usb &usb, const std::string &out_path)
{
    int result = usb.connect();
    if (result == 0)
    {
        usb.download_all(out_path);
    }
    else
    {
        std::cout << "Failed to connect: " << result << std::endl;
    }
}