#include <filesystem>
#include <iostream>

#include "../doctest.hpp"

#include "../../proto/exercise_samples.pb.h"
#include "../../training_data/proto_parser.hpp"

TEST_CASE("Parse samples")
{
    polar_data::PbExerciseSamples exercise_samples =
        parser::parse_proto_file<polar_data::PbExerciseSamples>("../src/test/training_data/testfiles/SAMPLES.BPB");

    CHECK(exercise_samples.speed_samples().size() != 0);
}