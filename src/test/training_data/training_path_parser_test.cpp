#include <filesystem>
#include <map>
#include <set>
#include <string>

#include "../doctest.hpp"

#include "../../pt_proto/pt_data.pb.h"
#include "../../training_data/training_path_parser.hpp"

TEST_CASE("GetAllTrainingPaths")
{
    std::string path = "../src/test/training_data/testfiles/pathparser";

    std::set<std::filesystem::path> paths;
    training_path_parser::get_all_training_paths(path, paths);

    CHECK(paths.size() == 1);
    CHECK(paths.begin()->stem() == "111900");
}