#include <filesystem>
#include <iostream>

#include "../doctest.hpp"

#include "../../proto/training_session.pb.h"
#include "../../training_data/proto_parser.hpp"

TEST_CASE("Parse training session")
{
    polar_data::PbTrainingSession training_session =
        parser::parse_proto_file<polar_data::PbTrainingSession>("../src/test/training_data/testfiles/TSESS.BPB");

    CHECK(training_session.duration().hours() == 3);
    CHECK(training_session.duration().minutes() == 2);
    CHECK(training_session.duration().seconds() == 1);
}