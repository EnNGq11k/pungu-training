#include "../doctest.hpp"

#include <filesystem>

#include "../../pt_proto/pt_data.pb.h"
#include "../../pt_proto/pt_data_version.hpp"
#include "../../training_data/training_parser.hpp"

TEST_CASE("Training parser running")
{
    pt_data::training training;
    std::string path = "../src/test/training_data/testfiles/run_example/030201";
    std::string po_file_path = path + "/PT_DATA.BPB";

    if (std::filesystem::exists(po_file_path))
    {
        std::filesystem::remove(po_file_path);
    }

    auto ret = training_parser::parse(path, training);

    CHECK(ret == training_parser::return_code::OK);

    CHECK(training.distance() == 12.34f);
    CHECK(training.energy() == 100);
    CHECK(training.running_index() == 70);

    CHECK(training.speed().avg() == 12.01f);
    CHECK(training.speed().max() == 13.02f);

    CHECK(training.speed_minutes_per_km().avg() == "04:59");
    CHECK(training.speed_minutes_per_km().max() == "04:36");

    CHECK(training.speed_minutes_per_100m().avg() == "00:29");
    CHECK(training.speed_minutes_per_100m().max() == "00:27");

    CHECK(training.heart_rate().avg() == 100);
    CHECK(training.heart_rate().max() == 180);

    CHECK(training.power().avg() == 200);
    CHECK(training.power().max() == 300);

    CHECK(training.cadence().avg() == 80);
    CHECK(training.cadence().max() == 100);

    CHECK(training.altitude().avg() == 800);
    CHECK(training.altitude().min() == 700);
    CHECK(training.altitude().max() == 1000);
    CHECK(training.altitude().ascent() == 200);
    CHECK(training.altitude().descent() == 300);

    CHECK(
        (training.duration().hours() == 3 && training.duration().minutes() == 2 && training.duration().seconds() == 1));

    CHECK(training.activity_type() == pt_data::activity_type::Running);

    CHECK(training.heart_rate_samples().size() > 0);
    CHECK(training.speed_samples().size() > 0);
    CHECK(training.power_samples().size() > 0);

    CHECK(std::filesystem::exists(po_file_path));
    std::filesystem::remove(po_file_path);
}

TEST_CASE("Training parser running")
{
    pt_data::training training;
    auto ret = training_parser::parse("../src/test/training_data/testfiles/run_example/040302", training);

    CHECK(ret == training_parser::return_code::OK);

    CHECK(training.version() == DATA_VERSION);

    CHECK(training.distance() == 12.34f);
    CHECK(training.energy() == 100);
    CHECK(training.running_index() == 70);

    CHECK(training.speed().avg() == 12.01f);
    CHECK(training.speed().max() == 13.02f);

    CHECK(training.speed_minutes_per_km().avg() == "04:59");
    CHECK(training.speed_minutes_per_km().max() == "04:36");

    CHECK(training.speed_minutes_per_100m().avg() == "00:29");
    CHECK(training.speed_minutes_per_100m().max() == "00:27");

    CHECK(training.heart_rate().avg() == 100);
    CHECK(training.heart_rate().max() == 180);

    CHECK(training.power().avg() == 200);
    CHECK(training.power().max() == 300);

    CHECK(training.cadence().avg() == 80);
    CHECK(training.cadence().max() == 100);

    CHECK(training.altitude().avg() == 800);
    CHECK(training.altitude().min() == 700);
    CHECK(training.altitude().max() == 1000);
    CHECK(training.altitude().ascent() == 200);
    CHECK(training.altitude().descent() == 300);

    CHECK(
        (training.duration().hours() == 3 && training.duration().minutes() == 2 && training.duration().seconds() == 1));

    CHECK(training.activity_type() == pt_data::activity_type::Running);

    CHECK(training.heart_rate_samples().size() > 0);
    CHECK(training.speed_samples().size() > 0);
    CHECK(training.power_samples().size() > 0);
}