#include "../doctest.hpp"

#include "../../training_data/duration.hpp"

TEST_CASE("ctor")
{
    duration d;

    CHECK(d.get_seconds() == 0);
    CHECK(d.get_minutes() == 0);
    CHECK(d.get_hours() == 0);
}

TEST_CASE("set seconds")
{
    duration d(0, 0, 50);
    CHECK(d.get_seconds() == 50);
    CHECK(d.get_minutes() == 0);
    CHECK(d.get_hours() == 0);

    d = duration(0, 0, 1);
    d.set_seconds(60);
    CHECK(d.get_seconds() == 0);
    CHECK(d.get_minutes() == 1);
    CHECK(d.get_hours() == 0);

    d = duration(0, 0, 1);
    d.set_seconds(70);
    CHECK(d.get_seconds() == 10);
    CHECK(d.get_minutes() == 1);
    CHECK(d.get_hours() == 0);

    d = duration(0, 0, 1);
    d.set_seconds(120);
    CHECK(d.get_seconds() == 0);
    CHECK(d.get_minutes() == 2);
    CHECK(d.get_hours() == 0);

    d = duration(0, 0, 1);
    d.set_seconds(3600);
    CHECK(d.get_seconds() == 0);
    CHECK(d.get_minutes() == 0);
    CHECK(d.get_hours() == 1);

    d = duration(0, 0, 1);
    d.set_seconds(3661);
    CHECK(d.get_seconds() == 1);
    CHECK(d.get_minutes() == 1);
    CHECK(d.get_hours() == 1);

    d = duration(1, 1, 1);
    d.set_seconds(2);
    CHECK(d.get_seconds() == 2);
    CHECK(d.get_minutes() == 1);
    CHECK(d.get_hours() == 1);
}

TEST_CASE("set minutes")
{
    duration d(0, 50, 1);
    CHECK(d.get_seconds() == 1);
    CHECK(d.get_minutes() == 50);
    CHECK(d.get_hours() == 0);

    d = duration(0, 1, 1);
    d.set_minutes(60);
    CHECK(d.get_seconds() == 1);
    CHECK(d.get_minutes() == 0);
    CHECK(d.get_hours() == 1);

    d = duration(0, 1, 1);
    d.set_minutes(61);
    CHECK(d.get_seconds() == 1);
    CHECK(d.get_minutes() == 1);
    CHECK(d.get_hours() == 1);

    d = duration(0, 1, 1);
    d.set_minutes(120);
    CHECK(d.get_seconds() == 1);
    CHECK(d.get_minutes() == 0);
    CHECK(d.get_hours() == 2);

    d = duration(0, 1, 1);
    d.set_minutes(121);
    CHECK(d.get_seconds() == 1);
    CHECK(d.get_minutes() == 1);
    CHECK(d.get_hours() == 2);

    d = duration(1, 1, 1);
    d.set_minutes(2);
    CHECK(d.get_seconds() == 1);
    CHECK(d.get_minutes() == 2);
    CHECK(d.get_hours() == 1);
}

TEST_CASE("set hours")
{
    duration d(0, 1, 1);
    CHECK(d.get_seconds() == 1);
    CHECK(d.get_minutes() == 1);
    CHECK(d.get_hours() == 0);

    d = duration(1, 1, 1);
    d.set_hours(2);
    CHECK(d.get_seconds() == 1);
    CHECK(d.get_minutes() == 1);
    CHECK(d.get_hours() == 2);
}

TEST_CASE("add duration")
{
    duration d1(1, 1, 1);
    duration d2(2, 2, 2);

    d2 += d1;
    CHECK(d2.get_seconds() == 3);
    CHECK(d2.get_minutes() == 3);
    CHECK(d2.get_hours() == 3);

    duration d3(0, 59, 59);
    d3 += d1;
    CHECK(d3.get_seconds() == 0);
    CHECK(d3.get_minutes() == 1);
    CHECK(d3.get_hours() == 2);
}

TEST_CASE("can handle > 24 hours")
{
    duration d(25, 1, 2);
    CHECK(d.to_string() == "25:01:02");
}