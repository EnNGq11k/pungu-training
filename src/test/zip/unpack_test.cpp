#include "../doctest.hpp"

#include <filesystem>

#include "../../proto/exercise_route.pb.h"
#include "../../training_data/proto_parser.hpp"
#include "../../zip/unpack.hpp"

TEST_CASE("Unpack route")
{
    std::string filename = "../test/zip/ROUTE.GZB";

    // ToDo: Fake data
    if (std::filesystem::exists(filename))
    {
        auto data = zip::unzipBZP("../test/zip/ROUTE.GZB");
        CHECK(data.size() != 0);

        auto route = parser::parse_proto_data<polar_data::PbExerciseRouteSamples>(data);
        CHECK(route.duration().size() != 0);
    }
    else
    {
        WARN("Missing gzb file");
    }
}