#include "native_usb_mock.hpp"
#include "../doctest.hpp"

native_usb_mock::native_usb_mock(std::vector<std::vector<unsigned char>> response_data)
    : data(response_data), request_number(0)
{
}

int native_usb_mock::open_usb(int vid, int pid)
{
    return 0;
}

int native_usb_mock::write_usb(const std::vector<unsigned char> &packet)
{
    return 0;
}

std::vector<unsigned char> native_usb_mock::read_usb()
{
    // Fail on read out of bounds
    CHECK(data.size() > request_number);
    return data[request_number++];
}

int native_usb_mock::close_usb()
{
    return 0;
}