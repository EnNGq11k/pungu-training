#ifndef NATIVE_USB_MOCK_H
#define NATIVE_USB_MOCK_H

#include "../../usb/backend/native_usb.hpp"

class native_usb_mock : public native_usb
{
  public:
    native_usb_mock(std::vector<std::vector<unsigned char>> response_data);
    ~native_usb_mock() = default;
    int open_usb(int vid, int pid);
    int write_usb(const std::vector<unsigned char> &packet);
    std::vector<unsigned char> read_usb();
    int close_usb();

  private:
    std::vector<std::vector<unsigned char>> data;
    int request_number;
};

#endif // NATIVE_USB_MOCK_H