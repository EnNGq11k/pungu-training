#include "../doctest.hpp"

#include <memory>
#include <vector>

#include "../../usb/polar_usb_v1.hpp"
#include "native_usb_mock.hpp"

std::vector<std::vector<unsigned char>> dir_data{
    std::vector<unsigned char>{0x11, 0xf5, 0x00, 0x00, 0x00, 0x0a, 0x0a, 0x0a, 0x06, 'S',  'P',  'R', 'O',
                               'F',  '/',  0x10, 0x00, 0x0a, 0x0e, 0x0a, 0x0a, 'U',  'S',  'E',  'R', 'I',
                               'D',  '.',  'B',  'P',  'B',  0x10, 'E',  0x0a, 0x06, 0x0a, 0x02, 'S', '/',
                               0x10, 0x00, 0x0a, 0x0d, 0x0a, 0x08, 'D',  'B',  'D',  'C',  '.',  'D', 'A',
                               'T',  0x10, 0xa4, 0x02, 0x0a, 0x0b, 0x0a, 0x07, 'R',  'O',  'U',  0x00},
    std::vector<unsigned char>{0x11, 0xf5, 0x01, 'T', 'E',  'S',  '/',  0x10, 0x00, 0x0a, 0x08, 0x0a, 0x04,
                               'F',  'A',  'V',  '/', 0x10, 0x00, 0x0a, 0x0d, 0x0a, 0x09, '2',  '0',  '1',
                               '6',  '0',  '7',  '1', '2',  '/',  0x10, 0x00, 0x0a, 0x0a, 0x0a, 0x06, 'B',
                               'I',  'K',  'E',  'S', '/',  0x10, 0x00, 0x0a, 0x0d, 0x0a, 0x09, '2',  '0',
                               '2',  '2',  '0',  '7', '0',  '4',  '/',  0x10, 0x00, 0x0a, 0x0a, 0x00},
    std::vector<unsigned char>{0x11, 0xf5, 0x02, 0x0a, 0x06, 'P',  'B',  'E',  'S',  'T',  '/', 0x10, 0x00,
                               0x0a, 0x0d, 0x0a, 0x09, '2',  '0',  '2',  '2',  '0',  '7',  '0', '8',  '/',
                               0x10, 0x00, 0x0a, 0x0d, 0x0a, 0x09, '2',  '0',  '2',  '2',  '0', '7',  '1',
                               '5',  '/',  0x10, 0x00, 0x0a, 0x0d, 0x0a, 0x09, '2',  '0',  '2', '2',  '0',
                               '7',  '1',  '9',  '/',  0x10, 0x00, 0x0a, 0x0d, 0x0a, 0x09, '2', 0x00},
    std::vector<unsigned char>{0x11, 0xf5, 0x03, '0',  '2',  '2',  '0',  '7',  '2',  '6',  '/', 0x10, 0x00,
                               0x0a, 0x0d, 0x0a, 0x09, '2',  '0',  '2',  '2',  '0',  '7',  '2', '7',  '/',
                               0x10, 0x00, 0x0a, 0x0d, 0x0a, 0x09, '2',  '0',  '2',  '2',  '0', '7',  '2',
                               '8',  '/',  0x10, 0x00, 0x0a, 0x0d, 0x0a, 0x09, '2',  '0',  '2', '2',  '0',
                               '8',  '1',  '7',  '/',  0x10, 0x00, 0x0a, 0x0d, 0x0a, 0x09, '2', 0x00},
    std::vector<unsigned char>{0x11, 0xa8, 0x04, '0', '2', '2', '0', '8', '2', '2', '/', 0x10, 0x00, 0x0a, 0x0d,
                               0x0a, 0x09, '2',  '0', '2', '2', '0', '8', '2', '3', '/', 0x10, 0x00, 0x0a, 0x0d,
                               0x0a, 0x09, '2',  '0', '2', '2', '0', '9', '0', '4', '/', 0x10, 0x00, 0x00, 0x00},
    std::vector<unsigned char>{0x00, 0x00, 0x00}};

TEST_CASE("Read folders")
{
    auto mock = new native_usb_mock(dir_data);
    polar_usb_v1 polar_usb(mock);

    std::vector<std::string> data;
    polar_usb.get_training_files(data);
    CHECK(data.size() == 20);
}
