#!/bin/sh

## Create makefiles
./configure.sh
sed -i.bak 's=/opt/homebrew=/usr/local/homebrew=g' makefile
rm makefile.bak

## Create proto files
arch -x86_64 make proto_gen

## Build qt
pushd ui/qt/
### Workaround: Create makefile with arm64 qmake
qmake Pungu.pro
sed -i.bak 's=/opt/homebrew=/usr/local/homebrew=g' Makefile
sed -i.bak 's=arm64=x86_64=g' Makefile
rm Makefile.bak
arch -x86_64 make
cp pack_mac.sh pack_mac_cc.sh
sed -i.bak 's=/opt/homebrew=/usr/local/homebrew=g' pack_mac_cc.sh
./pack_mac_cc.sh
rm pack_mac_cc.sh
rm pack_mac_cc.sh.bak
popd