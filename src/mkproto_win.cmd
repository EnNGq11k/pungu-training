@echo on

echo ProtoBuf Path %1

pushd proto
for %%v in (*.proto) do %1\protoc.exe --cpp_out=. "%%~nv.proto"
popd

pushd pt_proto
for %%v in (*.proto) do %1\protoc.exe --cpp_out=. "%%~nv.proto"
popd