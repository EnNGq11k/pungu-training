#!/bin/sh

s="$(uname -s)"

case "${s}" in
    Linux*)     echo "Linux" \
                && cp makefile_linux makefile \
                && cp compile_flags_linux.txt compile_flags.txt \
                && cp ui/qt/Pungu_template.pro ui/qt/Pungu.pro \
                && cat ui/qt/Pungu_linux.pro >> ui/qt/Pungu.pro
                ;;
    Darwin*)
                cp makefile_osx makefile \
                && cp compile_flags_osx.txt compile_flags.txt \
                && cp ui/qt/Pungu_template.pro ui/qt/Pungu.pro \
                && cat ui/qt/Pungu_osx.pro >> ui/qt/Pungu.pro
                m="$(uname -m)"

                case "${m}" in
                    arm64)
                            echo "macOS arm64"
                            ;;

                    *)
                            echo "macOS x64"
                            sed -i.bak 's=/opt/homebrew=/usr/local/homebrew=g' makefile \
                            && sed -i.bak 's=/opt/homebrew=/usr/local/homebrew=g' ui/qt/Pungu.pro
                            sed
                            ;;
                esac
                ;;

    *)
                echo "Not supported: ${unameOut}"
                ;;
esac
