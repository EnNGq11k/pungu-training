#include "training_saver.hpp"

#include <filesystem>
#include <fstream>
#include <iostream>

#include "../proto/exercise_base.pb.h"
#include "../proto/exercise_samples.pb.h"
#include "../proto/exercise_stats.pb.h"
#include "../proto/training_session.pb.h"
#include "../pt_proto/pt_data.pb.h"
#include "../pt_proto/pt_data_version.hpp"

#include "time_extensions.hpp"

void training_saver::get_training_path(const pt_data::training &training, const std::string &data_path,
                                       std::string &training_path)
{
    // PATH = /U/0/%Y%m%d/E/%H%M%S/
    std::string date;
    date_time_to_string(training.date_time(), PATH_DATE_FORMAT, date);
    std::string time;
    date_time_to_string(training.date_time(), PATH_TIME_FORMAT, time);

    training_path = data_path + "/U/0/" + date + "/E/" + time + "/";
}

std::error_code training_saver::delete_training(const pt_data::training &training, const std::string &data_path)
{
    std::string training_path;
    get_training_path(training, data_path, training_path);

    std::error_code result;

    if (std::filesystem::exists(training_path))
    {
        std::filesystem::remove_all(training_path, result);
    }

    return result;
}

bool training_saver::training_exist(const pt_data::training &training, const std::string &data_path)
{
    std::string training_path;
    get_training_path(training, data_path, training_path);

    return std::filesystem::exists(training_path + "PT_DATA.BPB");
}

training_saver::return_code training_saver::save_training(pt_data::training &training, const std::string &data_path,
                                                          const bool &resolve_path)
{
    std::string training_path;
    if (resolve_path)
    {
        get_training_path(training, data_path, training_path);
    }
    else
    {
        training_path = data_path;
    }

    // Create training paths
    std::error_code error_code;
    std::filesystem::create_directories(training_path, error_code);
    if (error_code.value() != 0)
    {
        std::cout << "Error creating training directory: [" << training_path << "]: " << error_code << std::endl;
        return training_saver::return_code::error_creating_directory;
    }

    training.set_version(DATA_VERSION);
    return write(training, training_path + "PT_DATA.BPB");
}

training_saver::return_code training_saver::write(const google::protobuf::Message &message,
                                                  const std::string &file_path)
{
    std::ofstream stream;
    stream.open(file_path, std::ios::out | std::ios::binary);
    message.SerializePartialToOstream(&stream);
    stream.close();

    return training_saver::return_code::OK;
}