#ifndef PT_BIKES_HELPER
#define PT_BIKES_HELPER

#include <filesystem>
#include <fstream>
#include <iostream>

#include "../pt_proto/pt_bikes.pb.h"
#include "proto_parser.hpp"

namespace pt_data
{
const std::string bike_proto_file_name = "/PT_BIKES.BPB";

inline void load_bikes(const std::string &data_path, pt_data::training_bikes &bikes)
{
    std::string bikesProtoFile = data_path + bike_proto_file_name;
    if (std::filesystem::exists(bikesProtoFile))
    {
        bikes = parser::parse_proto_file<pt_data::training_bikes>(bikesProtoFile);
    }

    if (!bikes.has_next_id() || bikes.next_id() == 0)
    {
        bikes.set_next_id(1);
    }
}

inline void save_bikes(const std::string &data_path, const pt_data::training_bikes &bikes)
{
    std::ofstream stream;
    std::string bikesProtoFile = (data_path + bike_proto_file_name);
    stream.open(bikesProtoFile, std::ios::out | std::ios::binary);
    bikes.SerializePartialToOstream(&stream);
    stream.close();
}
} // namespace pt_data

#endif