#ifndef TIME_EXTENSIONS_H
#define TIME_EXTENSIONS_H

#include <iomanip>
#include <sstream>
#include <string>

#include "../pt_proto/pt_data.pb.h"

static const std::string MINUTES_SECONDS_FORMAT = "%M:%S";
static const std::string TIME_FORMAT = "%H:%M:%S";

static const std::string PATH_DATE_FORMAT = "%Y%m%d";
static const std::string PATH_TIME_FORMAT = "%H%M%S";

inline void tm_to_string(const tm &date_time, const std::string &format_string, std::string &date_time_string)
{
    std::ostringstream oss;
    oss << std::put_time(&date_time, format_string.c_str());
    date_time_string = oss.str();
}

inline void tm_to_string(const tm &date_time, const std::string &format_string, std::string *date_time_string)
{
    std::ostringstream oss;
    oss << std::put_time(&date_time, format_string.c_str());
    *date_time_string = oss.str();
}

inline void date_time_to_string(const pt_data::date_time &date_time, const std::string &format_string,
                                std::string &date_time_string)
{
    tm tm;
    tm.tm_year = date_time.year() - 1900;
    tm.tm_mon = date_time.month() - 1;
    tm.tm_mday = date_time.day();
    tm.tm_hour = date_time.hours();
    tm.tm_min = date_time.minutes();
    tm.tm_sec = date_time.seconds();

    tm_to_string(tm, format_string, date_time_string);
}

#endif