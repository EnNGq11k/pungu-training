#ifndef TRAININGPARSER_H
#define TRAININGPARSER_H

#include <string>

#include "../proto/types.pb.h"
#include "../pt_proto/pt_data.pb.h"

class training_parser
{
  public:
    enum return_code
    {
        OK = 0,
        files_missing,
    };

  private:
    struct file_paths
    {
        std::string training_session;
        std::string base;
        std::string stats;
    };

  public:
    static return_code parse(const std::string &path, pt_data::training &training);

  private:
    static void parse_training_session(const std::string &path, pt_data::training &training);
    static void parse_exercise(const std::string &path, pt_data::training &training);
    static void parse_exercise_statistics(const std::string &path, pt_data::training &training);
    static void parse_samples(const std::string &path, const bool &compressed, pt_data::training &training);
    static void parse_pt_data(const std::string &path, pt_data::training &training);

    static void pbLocalDateTime_to_tm(const PbLocalDateTime &pbLocalDateTime, tm &date_time);
    static void speed_to_minutes_per_km(const float &speed, std::string *speed_string);
    static void speed_to_minutes_per_100m(const float &speed, std::string *speed_string);

    static return_code get_file_paths(const std::string &base_path, file_paths &paths);
};
#endif