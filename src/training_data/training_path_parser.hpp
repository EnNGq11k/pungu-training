#ifndef PO_TRAININGPATHPARSER_H
#define PO_TRAININGPATHPARSER_H

#include <cstdlib>
#include <filesystem>
#include <map>
#include <set>
#include <string>

#include "../pt_proto/pt_data.pb.h"
#include "training_parser.hpp"

namespace training_path_parser
{
inline void get_all_training_paths(const std::string &base_path, std::set<std::filesystem::path> &paths)
{
    std::string trainings_path = base_path + "/U/0";

    if (std::filesystem::is_directory(trainings_path))
    {
        for (auto const &dir_entry : std::filesystem::recursive_directory_iterator{trainings_path})
        {
            if (dir_entry.path().filename() == "TSESS.BPB" || dir_entry.path().filename() == "PT_DATA.BPB")
            {
                paths.insert(dir_entry.path().parent_path());
            }
        }
    }
}

inline void get_all_training_data(const std::string &base_path, std::map<long long, pt_data::training> &data)
{
    std::set<std::filesystem::path> paths;
    get_all_training_paths(base_path, paths);

    for (auto &path : paths)
    {
        std::string time = path.stem().generic_string();
        std::string date = path.parent_path().parent_path().stem().generic_string();
        // Long long because long is 32-Bit on windows
        // https://learn.microsoft.com/en-us/cpp/build/common-visual-cpp-64-bit-migration-issues?redirectedfrom=MSDN&view=msvc-170
        long long sortable_date_time = atoll((date + time).c_str());

        pt_data::training training;
        if (training_parser::parse(path.generic_string(), training) == training_parser::return_code::OK)
        {
            data[sortable_date_time] = training;
        }
    }
}

} // namespace training_path_parser

#endif