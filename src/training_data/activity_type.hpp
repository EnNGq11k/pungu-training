#ifndef ACTIVITYTYPES_H
#define ACTIVITYTYPES_H

#include <map>
#include <string>

#include "../pt_proto/pt_data.pb.h"

namespace activity_type
{
inline bool is_running_activity(const pt_data::activity_type &activity_type)
{
    return activity_type == pt_data::activity_type::Running || activity_type == pt_data::activity_type::RoadRunning ||
           activity_type == pt_data::activity_type::TreadmillRunning ||
           activity_type == pt_data::activity_type::TrackFieldRunning ||
           activity_type == pt_data::activity_type::TrailRunning ||
           activity_type == pt_data::activity_type::ImportedRunning;
}

inline bool is_swim_activity(const pt_data::activity_type &activity_type)
{
    return activity_type == pt_data::activity_type::Swim_Pool ||
           activity_type == pt_data::activity_type::SwimOpenWater ||
           activity_type == pt_data::activity_type::ImportedSwim;
}

inline bool is_bike_activity(const pt_data::activity_type &activity_type)
{
    return activity_type == pt_data::activity_type::BikeRoad || activity_type == pt_data::activity_type::BikeIndoor ||
           activity_type == pt_data::activity_type::ImportedBike || activity_type == pt_data::activity_type::Cycling ||
           activity_type == pt_data::activity_type::MountainBiking;
}
} // namespace activity_type
#endif