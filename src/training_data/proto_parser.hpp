#ifndef EXERCISEPARSER_H
#define EXERCISEPARSER_H

#include <fstream>
#include <ios>
#include <iostream>
#include <istream>
#include <iterator>
#include <ostream>
#include <string>
#include <vector>

namespace parser
{

template <class T> T parse_proto_data(const std::vector<unsigned char> &data)
{
    T proto_data;
    proto_data.ParseFromArray(data.data(), (int)data.size());

    return proto_data;
};

template <class T> T parse_proto_file(const std::string &path)
{
    std::ifstream data(path, std::ios::binary);
    std::vector<unsigned char> buffer(std::istreambuf_iterator<char>(data), {});

    return parse_proto_data<T>(buffer);
};

} // namespace parser

#endif