#ifndef PO_TRAINING_CACHE_H
#define PO_TRAINING_CACHE_H

#include <map>
#include <type_traits>

#include "../pt_proto/pt_data.pb.h"
#include "training_parser.hpp"
#include "training_path_parser.hpp"

class training_cache
{
  private:
    std::map<long long, pt_data::training> cache;
    pt_data::training empty;
    std::string path;

  public:
    void init_cache(const std::string &path)
    {
        if (!path.empty())
        {
            this->path = path;
        }

        refresh_cache();
    }

    void refresh_cache()
    {
        cache.clear();
        training_path_parser::get_all_training_data(path, cache);
    }

    const std::map<long long, pt_data::training> &get_all() const
    {
        return cache;
    }

    const pt_data::training &get_training(long long date_time) const
    {
        auto training_pair = cache.find(date_time);
        if (training_pair != cache.end())
        {
            return training_pair->second;
        }

        return empty;
    }

    std::vector<const pt_data::training> get_trainings_forDay(long long date_time) const
    {
        std::vector<const pt_data::training> result;

        long long begin = date_time - (date_time % 1000000);
        long long end = date_time + 1000000;

        for (const auto &training : cache)
        {
            if (training.first >= begin && training.first < end)
            {
                result.push_back(training.second);
            }
        }

        return result;
    }
};

#endif