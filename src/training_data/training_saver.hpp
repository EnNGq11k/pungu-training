#ifndef TRAININGSAVER_H
#define TRAININGSAVER_H

#include "../pt_proto/pt_data.pb.h"
#include "google/protobuf/message_lite.h"

class training_saver
{
  public:
    enum return_code
    {
        OK = 0,
        error_creating_directory,
        error_saving_file,
    };

  public:
    static bool training_exist(const pt_data::training &training, const std::string &data_path);
    static return_code save_training(pt_data::training &training, const std::string &data_path,
                                     const bool &resolve_path = true);
    static std::error_code delete_training(const pt_data::training &training, const std::string &data_path);

  private:
    static void get_training_path(const pt_data::training &training, const std::string &data_path,
                                  std::string &training_path);

    static return_code write(const google::protobuf::Message &message, const std::string &file_path);
};

#endif