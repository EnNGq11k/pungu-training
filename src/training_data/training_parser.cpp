#include "training_parser.hpp"
#include <cmath>
#include <sstream>
#include <string>

#include <filesystem>

#include "../proto/exercise_base.pb.h"
#include "../proto/exercise_samples.pb.h"
#include "../proto/exercise_stats.pb.h"
#include "../proto/training_session.pb.h"
#include "../pt_proto/pt_data.pb.h"

#include "activity_type.hpp"
#include "proto_parser.hpp"
#include "time_extensions.hpp"
#include "training_saver.hpp"

#include "../zip/unpack.hpp"

training_parser::return_code training_parser::parse(const std::string &path, pt_data::training &training)
{
    return_code result = return_code::OK;

    auto pt_data_path = path + "/PT_DATA.BPB";

    // Already converted to pt_data
    if (std::filesystem::exists(pt_data_path))
    {
        parse_pt_data(pt_data_path, training);
    }
    else // Convert to pt_data
    {
        file_paths paths;
        result = get_file_paths(path, paths);
        if (result != return_code::OK)
        {
            return result;
        }

        std::string samples_path = path + "/00/SAMPLES.GZB";
        bool samples_compressed = true;

        if (!std::filesystem::exists(samples_path))
        {
            // check for uncompressed file
            samples_path = path + "/00/SAMPLES.BPB";
            if (!std::filesystem::exists(samples_path))
            {
                return return_code::files_missing;
            }

            samples_compressed = false;
        }

        training.set_data_type(pt_data::data_type::IMPORTED);
        training.set_is_new(true);
        parse_training_session(paths.training_session, training);
        parse_exercise(paths.base, training);
        parse_exercise_statistics(paths.stats, training);
        parse_samples(samples_path, samples_compressed, training);

        training_saver::save_training(training, path + "/", false);
    }

    return result;
}

void training_parser::parse_training_session(const std::string &path, pt_data::training &training)
{
    auto training_session = parser::parse_proto_file<polar_data::PbTrainingSession>(path);

    training.mutable_date_time()->set_year(training_session.start().date().year());
    training.mutable_date_time()->set_month(training_session.start().date().month());
    training.mutable_date_time()->set_day(training_session.start().date().day());
    training.mutable_date_time()->set_hours(training_session.start().time().hour());
    training.mutable_date_time()->set_minutes(training_session.start().time().minute());
    training.mutable_date_time()->set_seconds(training_session.start().time().seconds());

    // distance in m -> round to 10 m -> convert to km
    training.set_distance(roundf(training_session.distance() / 10) / 100);
    training.set_energy(training_session.calories());
}

void training_parser::parse_exercise(const std::string &path, pt_data::training &training)
{
    auto exercise = parser::parse_proto_file<polar_data::PbExerciseBase>(path);

    training.set_activity_type((pt_data::activity_type)exercise.sport().value());

    training.mutable_duration()->set_hours(exercise.duration().hours());
    training.mutable_duration()->set_minutes(exercise.duration().minutes());
    training.mutable_duration()->set_seconds(exercise.duration().seconds());

    training.set_running_index(exercise.running_index().value());

    training.mutable_altitude()->set_ascent(roundf(exercise.ascent()));
    training.mutable_altitude()->set_descent(roundf(exercise.descent()));
}

void training_parser::parse_exercise_statistics(const std::string &path, pt_data::training &training)
{
    auto exercise_stats = parser::parse_proto_file<polar_data::PbExerciseStatistics>(path);

    training.mutable_speed()->set_avg(roundf(exercise_stats.speed().average() * 100) / 100);
    training.mutable_speed()->set_max(roundf(exercise_stats.speed().maximum() * 100) / 100);
    speed_to_minutes_per_km(exercise_stats.speed().average(), training.mutable_speed_minutes_per_km()->mutable_avg());
    speed_to_minutes_per_km(exercise_stats.speed().maximum(), training.mutable_speed_minutes_per_km()->mutable_max());
    speed_to_minutes_per_100m(exercise_stats.speed().average(),
                              training.mutable_speed_minutes_per_100m()->mutable_avg());
    speed_to_minutes_per_100m(exercise_stats.speed().maximum(),
                              training.mutable_speed_minutes_per_100m()->mutable_max());

    training.mutable_heart_rate()->set_avg(exercise_stats.heart_rate().average());
    training.mutable_heart_rate()->set_max(exercise_stats.heart_rate().maximum());

    training.mutable_power()->set_avg(exercise_stats.power().average());
    training.mutable_power()->set_max(exercise_stats.power().maximum());

    training.mutable_cadence()->set_avg(exercise_stats.cadence().average());
    training.mutable_cadence()->set_max(exercise_stats.cadence().maximum());

    training.mutable_altitude()->set_avg((uint32_t)exercise_stats.altitude().average());
    training.mutable_altitude()->set_min((uint32_t)exercise_stats.altitude().minimum());
    training.mutable_altitude()->set_max((uint32_t)(exercise_stats.altitude().maximum()));
}

void training_parser::speed_to_minutes_per_km(const float &speed, std::string *speed_string)
{
    if (speed > 0)
    {
        int totalSeconds = (int)(3600 / speed);

        tm date_time; // tm for easy format string
        date_time.tm_min = totalSeconds / 60;
        date_time.tm_sec = totalSeconds - (date_time.tm_min * 60);

        tm_to_string(date_time, MINUTES_SECONDS_FORMAT, speed_string);
    }
}

void training_parser::speed_to_minutes_per_100m(const float &speed, std::string *speed_string)
{
    if (speed > 0)
    {
        int totalSeconds = (int)(360 / speed);

        tm date_time; // tm for easy format string
        date_time.tm_min = totalSeconds / 60;
        date_time.tm_sec = totalSeconds - (date_time.tm_min * 60);

        tm_to_string(date_time, MINUTES_SECONDS_FORMAT, speed_string);
    }
}

void training_parser::parse_samples(const std::string &path, const bool &compressed, pt_data::training &training)
{
    polar_data::PbExerciseSamples samples;

    if (compressed)
    {
        std::vector<unsigned char> data = zip::unzipBZP(path);
        samples = parser::parse_proto_data<polar_data::PbExerciseSamples>(data);
    }
    else
    {
        samples = parser::parse_proto_file<polar_data::PbExerciseSamples>(path);
    }

    for (auto &sample : samples.speed_samples())
    {
        training.mutable_speed_samples()->Add(sample);
    }

    // pedal only contains **half** power.
    // left_pedal is also used for running power
    for (auto &sample : samples.left_pedal_power_samples())
    {
        training.mutable_power_samples()->Add(sample.current_power() * 2);
    }

    for (auto &sample : samples.heart_rate_samples())
    {
        training.mutable_heart_rate_samples()->Add(sample);
    }
}

void training_parser::parse_pt_data(const std::string &path, pt_data::training &training)
{
    training = parser::parse_proto_file<pt_data::training>(path);
}

training_parser::return_code training_parser::get_file_paths(const std::string &base_path, file_paths &paths)
{
    paths.training_session = base_path + "/TSESS.BPB";
    paths.base = base_path + "/00/BASE.BPB";
    paths.stats = base_path + "/00/STATS.BPB";

    if (!std::filesystem::exists(paths.training_session) || !std::filesystem::exists(paths.base) ||
        !std::filesystem::exists(paths.stats))
    {
        return return_code::files_missing;
    }

    return return_code::OK;
}