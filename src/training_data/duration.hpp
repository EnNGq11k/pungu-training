#ifndef PO_DURATION_H
#define PO_DURATION_H

#include <sstream>
#include <string>

#include "../proto/types.pb.h"
#include "../pt_proto/pt_data.pb.h"

class duration
{
  private:
    int hours;
    int minutes;
    int seconds;

  public:
    duration() : hours(0), minutes(0), seconds(0)
    {
    }

    duration(int hours, int minutes, int seconds)
    {
        set_seconds(seconds);
        set_minutes(minutes);
        set_hours(hours);
    }

    duration(const PbDuration &duration)
        : hours(duration.hours()), minutes(duration.minutes()), seconds(duration.seconds())
    {
    }

    void operator+=(const duration &other)
    {
        set_seconds(this->seconds + other.seconds);
        set_minutes(this->minutes + other.minutes);
        set_hours(this->hours + other.hours);
    }

    void operator+=(const pt_data::duration &duration)
    {
        set_seconds(this->seconds + duration.seconds());
        set_minutes(this->minutes + duration.minutes());
        set_hours(this->hours + duration.hours());
    }

    void set_hours(const int &h)
    {
        hours = h;
    }
    void set_minutes(const int &m)
    {
        int overflow = m / 60;
        if (overflow > 0)
        {
            hours = hours + overflow;
            minutes = m % 60;
        }
        else
        {
            minutes = m;
        }
    }

    void set_seconds(const int &s)
    {
        int overflow = s / 60;
        if (overflow > 0)
        {
            set_minutes(minutes + overflow);
            seconds = s % 60;
        }
        else
        {
            seconds = s;
        }
    }

    int get_hours() const
    {
        return hours;
    }
    int get_minutes() const
    {
        return minutes;
    }
    int get_seconds() const
    {
        return seconds;
    }

    std::string to_string()
    {
        std::stringstream sstream;
        sstream << (hours < 10 ? "0" : "") << hours << ":";
        sstream << (minutes < 10 ? "0" : "") << minutes << ":";
        sstream << (seconds < 10 ? "0" : "") << seconds;
        return sstream.str();
    }
};

#endif