#include "polar_usb.hpp"
#include "../proto/pftp_response.pb.h"

#include <filesystem>
#include <fstream>
#include <iostream>
#include <ostream>

void polar_usb::get_training_files(std::vector<std::string> &files) const
{
    return get_files_and_directories(TRAINING_ROOT_DIR, files);
}

void polar_usb::download_all(const std::string &outPath) const
{
    std::cout << "Downloading all files from " << TRAINING_ROOT_DIR << std::endl;
    download_recursive(outPath, TRAINING_ROOT_DIR);
    std::cout << std::endl << "Download finished" << std::endl;
}

void polar_usb::download_recursive(const std::string &outPath, const std::string &path) const
{
#if DEBUG
    std::cout << "Path: " << path << std::endl;
#endif

    std::vector<std::string> files_and_folders;
    get_files_and_directories(path, files_and_folders);

    for (size_t i = 0; i < files_and_folders.size(); ++i)
    {
        if (files_and_folders[i].find("/") != std::string::npos)
        {
            download_recursive(outPath, path + files_and_folders[i]);
        }
        else
        {
            download_file(outPath, path, files_and_folders[i]);
        }
    }
}

polar_usb::download_error polar_usb::download_file(const std::string &outPath, const std::string &path,
                                                   const std::string &filename) const
{
    std::string remoteFilePath = path + filename;

#if DEBUG
    std::cout << "File: " << remoteFilePath << std::endl;
#endif

    std::string localPath = outPath + path;
    std::string localFilePath = localPath + filename;

    // Maybe compare file size and overwrite with new data.
    if (!std::filesystem::exists(localFilePath))
    {
        std::error_code error;
        std::filesystem::create_directories(localPath, error);

        if (error.value() == 0)
        {
            std::cout << "Downloading: " << remoteFilePath << " to " << localFilePath << std::endl;

            std::vector<unsigned char> data;
            get_data(remoteFilePath, data);

            if (data.size() != 0)
            {
                std::ofstream stream;
                stream.open(localFilePath, std::ios::out | std::ios::binary);

                if (stream)
                {
                    stream.write(reinterpret_cast<char *>(&data[0]), data.size() * sizeof(data[0]));
                    stream.close();
                }
                else
                {
                    std::cerr << "Opening file [" << localFilePath << "] failed" << std::endl;
                    return download_error::cannot_open_output;
                }
            }
            else
            {
                std::cerr << "Unexpected empty file [" << localFilePath << "]. Skip download." << std::endl;
                return download_error::file_empty;
            }
        }
        else
        {
            std::cerr << "Error creating directory: [" << localPath << "] error: " << error << std::endl;
            return download_error::cannot_create_directory;
        }
    }
    else
    {
#if DEBUG
        std::cout << "Skip: file [" << localFilePath << "] already exists" << std::endl;
#endif
    }

    return download_error::ok;
}

void polar_usb::get_files_and_directories(const std::string &request,
                                          std::vector<std::string> &files_and_directories) const
{
    std::vector<unsigned char> packet;
    get_data(request, packet);
    polar_protocol::PbPFtpDirectory dir;
    dir.ParseFromArray(packet.data(), (int)packet.size());

    for (auto &entry : dir.entries())
    {
        bool isEmptyFile = entry.name().find("/") == std::string::npos && entry.size() == 0;
        if (!isEmptyFile)
        {
            files_and_directories.push_back(entry.name());
        }
    }
}