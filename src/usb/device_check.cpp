#include "device_check.hpp"

#include <iostream>
#include <ostream>

#include "libusb.h"

device_check::polar_devices device_check::check_for_polar_devices()
{
    device_check::polar_devices available_devices;

    int result = libusb_init(NULL);
    if (result < 0)
    {
        std::cout << "libusb init failed: " << result << std::endl;
        return available_devices;
    }

    libusb_device **device_list;
    ssize_t device_count = libusb_get_device_list(NULL, &device_list);
    if (device_count < 0)
    {
        libusb_exit(NULL);
        return available_devices;
    }

    libusb_device *dev;
    int i = 0;

    while ((dev = device_list[i++]) != NULL)
    {
        struct libusb_device_descriptor desc;
        result = libusb_get_device_descriptor(dev, &desc);

        if (result < 0)
        {
            std::cout << "failed to get device descriptor: " << result << std::endl;
            continue;
        }

        if (desc.idVendor == device_check::polar_vendor)
        {
            if (desc.idProduct == device_check::polar_v1_device)
            {
                available_devices.v1_available = true;
            }
            else if (desc.idProduct == device_check::polar_v2_device)
            {
#if defined(__APPLE__)
                libusb_device_handle *devHandle = NULL;
                result = libusb_open(dev, &devHandle);
                if (result != LIBUSB_SUCCESS)
                {
                    std::cout << "failed to open v2 device: " << result << std::endl;
                    continue;
                }

                uint8_t sn[42];
                result = libusb_get_string_descriptor_ascii(devHandle, desc.iSerialNumber, sn, 42);
                libusb_close(devHandle);
                devHandle = NULL;

                if (result == -1)
                {
                    std::cout << "failed to get v2 serial number: " << result << std::endl;
                    continue;
                }

                std::string serial(reinterpret_cast<char *>(sn));
                // ToDo: Always serial + 1 ??
                available_devices.v2_serial_number = serial + "1";
#endif

                available_devices.v2_available = true;
            }
        }
    }

    libusb_free_device_list(device_list, 1);
    libusb_exit(NULL);

    return available_devices;
}