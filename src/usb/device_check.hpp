#ifndef PO_DEVICECHECK_H
#define PO_DEVICECHECK_H

#include <string>

namespace device_check
{
struct polar_devices
{
    bool v1_available = false;
    bool v2_available = false;
    std::string v2_serial_number = ""; // For apple only
};

const int polar_vendor = 0x0da4;
const int polar_v1_device = 0x0008;
const int polar_v2_device = 0x0014;

polar_devices check_for_polar_devices();
} // namespace device_check
#endif