#ifndef PO_POLARUSB_H
#define PO_POLARUSB_H

#include <string>
#include <vector>

static const std::string TRAINING_ROOT_DIR = "/U/0/";
class polar_usb
{
  public:
    enum download_error
    {
        ok,
        cannot_open_output,
        cannot_create_directory,
        error_during_download,
        file_empty,
    };

  public:
    void get_training_files(std::vector<std::string> &files) const;
    void download_all(const std::string &outPath) const;
    void download_recursive(const std::string &outPath, const std::string &path) const;
    download_error download_file(const std::string &outPath, const std::string &path,
                                 const std::string &filename) const;

    void get_files_and_directories(const std::string &request, std::vector<std::string> &files_and_directories) const;

    // VIRTUAL
  public:
    virtual int connect() = 0;

  protected:
    virtual void get_data(const std::string &request, std::vector<unsigned char> &data) const = 0;
};

#endif