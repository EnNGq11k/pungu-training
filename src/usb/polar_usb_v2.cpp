#include "polar_usb_v2.hpp"

#include "../proto/pftp_request.pb.h"
#include "backend/serial_usb.hpp"

polar_usb_v2::polar_usb_v2() : polar_usb_v2(0)
{
}

polar_usb_v2::polar_usb_v2(serial_usb *usb) : usb(usb)
{
}

polar_usb_v2::~polar_usb_v2()
{
    if (usb != 0)
    {
        usb->close_usb();
        delete usb;
        usb = 0;
    }
}

int polar_usb_v2::connect()
{
    int ret = -1;

    if (usb == 0)
    {
        usb = new serial_usb();
    }

    ret = usb->open_usb(serial_number);

    return ret;
}

void polar_usb_v2::get_data(const std::string &request, std::vector<unsigned char> &data) const
{
    std::vector<unsigned char> packet;
    generate_request(request, packet);
    usb->write_usb(packet);
    data = usb->read_usb();
}

void polar_usb_v2::generate_request(const std::string &request, std::vector<unsigned char> &packet) const
{
    polar_protocol::PbPFtpOperation op;
    op.set_command(polar_protocol::PbPFtpOperation_Command::PbPFtpOperation_Command_GET);
    op.set_path(request);

    int size = (int)op.ByteSizeLong();
    std::vector<unsigned char> data(size);
    op.SerializeToArray(data.data(), size);

    packet.resize(2);
    packet[0] = size & 255;
    packet[1] = size >> 8;
    packet.insert(packet.end(), data.begin(), data.end());
}