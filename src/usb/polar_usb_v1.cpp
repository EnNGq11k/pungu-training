#include "polar_usb_v1.hpp"
#include "backend/native_usb.hpp"

polar_usb_v1::polar_usb_v1() : polar_usb_v1(0)
{
}

polar_usb_v1::polar_usb_v1(native_usb *usb) : usb(usb)
{
}

polar_usb_v1::~polar_usb_v1()
{
    if (usb != 0)
    {
        usb->close_usb();
        delete usb;
        usb = 0;
    }
}

int polar_usb_v1::connect()
{
    int ret = -1;

    if (usb == 0)
    {
        usb = new native_usb();
    }

    ret = usb->open_usb(0x0da4, 0x0008);

    return ret;
}

void polar_usb_v1::get_data(const std::string &request, std::vector<unsigned char> &data) const
{
    std::vector<unsigned char> packet;
    int packet_num = 0;
    bool initial_packet = true;
    bool more = true;
    usb_state usb_state = usb_state::Send;

    while (more)
    {
        switch (usb_state)
        {
        case usb_state::Send:
            packet.clear();
            packet = generate_request(request);

            usb->write_usb(packet);

            packet_num = 0;
            usb_state = usb_state::Read;
            break;
        case usb_state::Read:
            packet.clear();
            packet = usb->read_usb();

            // check for end of buffer
            if (is_end(packet))
            {
                add_to_full(packet, data, initial_packet, true);
                usb_state = usb_state::Finish;
            }
            else
            {
                add_to_full(packet, data, initial_packet, false);
                usb_state = usb_state::ACK;
            }

            // initial packet seems to always have two extra bytes in the front, 0x00 0x00
            if (initial_packet)
            {
                initial_packet = false;
            }
            break;
        case usb_state::ACK:
            packet.clear();
            packet = generate_ack(packet_num);
            if (packet_num == 0xff)
            {
                packet_num = 0x00;
            }
            else
            {
                packet_num++;
            }

            usb->write_usb(packet);

            usb_state = usb_state::Read;
            break;

        case usb_state::Finish:
            more = false;
            break;
        }
    }
}

std::vector<unsigned char> polar_usb_v1::generate_request(const std::string &request) const
{
    // Insert will resize by request size
    std::vector<unsigned char> packet(9);

    packet[0] = 0x01;
    packet[1] = (unsigned char)((request.length() + 8) << 2);
    packet[2] = 0x00;
    packet[3] = (unsigned char)(request.length() + 4);
    packet[4] = 0x00;
    packet[5] = 0x08;
    packet[6] = 0x00;
    packet[7] = 0x12;
    packet[8] = (unsigned char)request.length();
    packet.insert(packet.end(), request.begin(), request.end());

    return packet;
}

std::vector<unsigned char> polar_usb_v1::generate_ack(const unsigned char packet_num) const
{
    std::vector<unsigned char> packet(3);

    packet[0] = 0x01;
    packet[1] = 0x05;
    packet[2] = packet_num;

    return packet;
}

int polar_usb_v1::is_end(const std::vector<unsigned char> &packet) const
{
    if ((packet[1] & 0x03) == 1)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

void polar_usb_v1::add_to_full(const std::vector<unsigned char> &packet, std::vector<unsigned char> &full,
                               const bool initial_packet, const bool final_packet) const
{
    unsigned int size = packet[1] >> 2;

    if (initial_packet)
    {
        // final packets have a trailing 0x00 we don't want
        if (final_packet)
        {
            size -= 4;
        }
        else
        {
            size -= 3;
        }

        auto begin = packet.begin() + 5;
        full.insert(full.end(), begin, begin + size);
    }
    else
    {
        // final packets have a trailing 0x00 we don't want
        if (final_packet)
        {
            size -= 2;
        }
        else
        {
            size -= 1;
        }

        auto begin = packet.begin() + 3;
        full.insert(full.end(), begin, begin + size);
    }
}