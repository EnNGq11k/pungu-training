/*
    Copyright 2014 Christian Weber

    This file is part of V800 Downloader.

    V800 Downloader is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    V800 Downloader is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with V800 Downloader.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "native_usb.hpp"
#include <iostream>
#include <vector>

#if defined(__linux__)
#include <cstring>
#include <unistd.h>
#endif

#if defined(__APPLE__)
extern "C"
{
    int rawhid_open(int max, int vid, int pid, int usage_page, int usage);
    int rawhid_recv(int num, void *buf, int len, int timeout);
    int rawhid_send(int num, void *buf, int len, int timeout);
    void rawhid_close(int num);
}
#endif

int native_usb::open_usb(int vid, int pid)
{
#if defined(__linux__) || defined(_WIN32)
    int r;
    usb = NULL;

    r = libusb_init(NULL);
    if (r < 0)
        return -1;

    // libusb_set_debug(NULL, LIBUSB_LOG_LEVEL_INFO);

    std::cout << "Opening the USB device..." << std::endl;
    usb = libusb_open_device_with_vid_pid(NULL, vid, pid);
    if (usb == NULL)
    {
        std::cout << "Error opening device";
        return -1;
    }

#if defined(__linux__)
    r = libusb_detach_kernel_driver(usb, 0);
    if (r != 0)
    {
        std::cout << "Detach Kernel Driver: " << libusb_error_name(r) << std::endl;
    }
#endif

    r = libusb_claim_interface(usb, 0);
    if (r != 0)
    {
        std::cout << "Claim Interface: " << libusb_error_name(r) << std::endl;
        return -1;
    }

    std::cout << "Releasing interface and closing the USB device..." << std::endl;
    libusb_release_interface(usb, 0);
    libusb_close(usb);
    usb = NULL;

#if defined(_WIN32)
    Sleep(500);
#endif
#if defined(__linux__)
    usleep(500000);
#endif

    std::cout << "Reopening the USB device..." << std::endl;
    usb = libusb_open_device_with_vid_pid(NULL, vid, pid);
    if (usb == NULL)
    {
        std::cout << "Error opening device" << std::endl;
        return -1;
    }

    r = libusb_claim_interface(usb, 0);
    if (r != 0)
    {
        std::cout << "Reclaim Interface: " << libusb_error_name(r) << std::endl;
        return -1;
    }

#if defined(_WIN32)
    Sleep(500);
#endif
#if defined(__linux__)
    usleep(500000);
#endif

    return 0;
#endif
#if defined(__APPLE__)
    int r;

    r = rawhid_open(1, vid, pid, 0, 0);
    if (r == 0)
    {
        usb = -1;
        return -1;
    }

    usb = 0;
    return 0;
#endif
}

int native_usb::write_usb(const std::vector<unsigned char> &packet)
{
#if defined(__APPLE__)
    if (usb == -1)
        return -1;
#endif
#if defined(__linux__) || defined(_WIN32)
    if (usb == NULL)
        return -1;
#endif

    if (packet.size() > 64)
        return -1;

    std::vector<unsigned char> correct_packet(64 - packet.size());
    correct_packet.insert(correct_packet.begin(), packet.begin(), packet.end());

    int actual_length;
#if defined(__APPLE__)
    actual_length = rawhid_send(usb, static_cast<void *>(correct_packet.data()), correct_packet.size(), 1000);
#endif
#if defined(__linux__) || defined(_WIN32)
    libusb_interrupt_transfer(usb, (1 | LIBUSB_ENDPOINT_OUT), (unsigned char *)correct_packet.data(),
                              (int)correct_packet.size(), &actual_length, 0);
#endif

    return actual_length;
}

std::vector<unsigned char> native_usb::read_usb()
{
    unsigned char char_packet[64];
    int actual_length;

#if defined(__APPLE__)
    if (usb == -1)
        return std::vector<unsigned char>();
#endif
#if defined(__linux__) || defined(_WIN32)
    if (usb == NULL)
        return std::vector<unsigned char>();
#endif

    memset(char_packet, 0x00, sizeof(char_packet));

#if defined(__APPLE__)
    actual_length = rawhid_recv(usb, char_packet, sizeof(char_packet), 1000);
#endif
#if defined(__linux__) || defined(_WIN32)
    libusb_interrupt_transfer(usb, (1 | LIBUSB_ENDPOINT_IN), char_packet, sizeof(char_packet), &actual_length, 0);
#endif

    return std::vector<unsigned char>(&char_packet[0], &char_packet[actual_length]);
}

int native_usb::close_usb()
{
#if defined(__linux__) || defined(_WIN32)
    int r;

    if (usb == NULL)
        return -1;

    r = libusb_release_interface(usb, 0);
    if (r != 0)
    {
        std::cout << "Release interface: " << libusb_error_name(r) << std::endl;
        return -1;
    }

#if defined(__linux__)
    libusb_attach_kernel_driver(usb, 0);
#endif

    libusb_close(usb);
    libusb_exit(NULL);

    return 0;
#endif
#if defined(__APPLE__)
    if (usb == -1)
        return -1;

    rawhid_close(usb);
    usb = -1;

    return 0;
#endif
}
