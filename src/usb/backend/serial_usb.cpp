#include "serial_usb.hpp"

#if defined(__APPLE__)
#include "osx/rs232_osx.h"
#else
#include "rs232.h"
#endif
#include <chrono>
#include <iostream>
#include <thread>
#include <vector>

serial_usb::serial_usb() : connected(false)
{
}

int serial_usb::open_usb([[maybe_unused]] const std::string &serial_number)
{
    int result = 0;
    if (!connected)
    {
#if defined(__APPLE__)
        std::string com_name = "/dev/tty.usbmodem" + serial_number;
        result = RS232_OpenComport(com_name.c_str(), 115200, "8N1", 0);
#elif defined(_WIN32)
        result = RS232_OpenComport(3, 115200, "8N1", 0);
#else
        result = RS232_OpenComport(24, 115200, "8N1", 0);
#endif
        if (result == 0)
        {
            connected = true;
        }
    }

    return result;
}

int serial_usb::write_usb(const std::vector<unsigned char> &packet)
{
    std::vector<unsigned char> data(3);
    data[0] = 5;
    data[1] = (unsigned char)(packet.size() & 255);
    data[2] = (unsigned char)(packet.size() >> 8);
    data.insert(data.end(), packet.begin(), packet.end());
#if defined(__APPLE__)
    int sent = RS232_SendBuf(data.data(), (int)data.size());
#elif defined(_WIN32)
    int sent = RS232_SendBuf(3, data.data(), (int)data.size());
#else
    int sent = RS232_SendBuf(24, data.data(), (int)data.size());
#endif
    if (sent == -1)
    {
        std::cout << "error sending bytes" << std::endl;
    }
    if (sent == 0)
    {
        std::cout << "nothing written ?!?" << std::endl;
    }

    return sent;
}

std::vector<unsigned char> serial_usb::read_usb()
{
    bool done = false;
    bool expect_header = true;
    bool started = false;
    bool is_notification = false;
    bool has_more = false;
    bool initial_packet = false;
    int expected_size = 0;
    int received_size = 0;
    std::vector<unsigned char> packet;
    std::vector<unsigned char> response;
    std::vector<unsigned char> notification;

    while (!done || !packet.empty())
    {
        std::vector<unsigned char> buffer(65536);
#if defined(__APPLE__)
        int size = RS232_PollComport(buffer.data(), 65536);
#elif defined(_WIN32)
        int size = RS232_PollComport(3, buffer.data(), 65536);
#else
        int size = RS232_PollComport(24, buffer.data(), 65536);
#endif
        if (size == 0)
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
            continue;
        }

        // Packet can contain data if we received a overflow. Append new data.
        packet.insert(packet.end(), buffer.begin(), buffer.begin() + size);

        std::vector<unsigned char> payload;
        if (expect_header)
        {
            if (packet.size() < 3)
            {
                std::cout << "Packet too short?" << std::endl;
                response.clear();
                return response;
            }

            int packet_flags = packet[0];
            initial_packet = (packet_flags & 1) != 0;
            is_notification = (packet_flags & 2) != 0;
            // int unknown_flag = (packet_flags & 4) != 0;
            has_more = (packet_flags & 8) != 0;

            expected_size = packet[1] + (packet[2] << 8);
            received_size = 0;
            payload.insert(payload.begin(), packet.begin() + 3, packet.end());

            if (packet_flags == 1)
            {
                // process_error payload
                std::cout << "received error: " << (int)payload[0] << std::endl;
                response.clear();
                return response;
            }

            if (!started && !initial_packet)
            {
                std::cout << "Initial packet expected?" << std::endl;
            }
            else if (initial_packet && started)
            {
                std::cout << "Initial packet not expected?" << std::endl;
            }
            /* ToDo check
            else if (unknown_flag)
            {
                std::cout << "Unknown packet flag? #{packet_flags}" << std::endl;
            }
            */
            else if ((packet_flags & 0xf0) != 0)
            {
                std::cout << "Unknown packet flags? " << packet_flags << std::endl;
            }

            expect_header = false;
            started = true;
        }
        else
        {
            payload = packet;
        }

        int overflow = received_size + (int)payload.size() - expected_size;
        if (overflow > 0)
        {
            // We have received more data than expected. Only consume what was expected and let next iteration process
            // the remaining data.
            packet.clear();
            packet.insert(packet.begin(), payload.end() - overflow, payload.end());
            payload.erase(payload.end() - overflow, payload.end());
        }
        else
        {
            packet.clear();
        }

        received_size += (int)payload.size();

        if (is_notification)
        {
            notification.insert(notification.end(), payload.begin(), payload.end());
        }
        else
        {
            if (done)
            {
                std::cout << "Received unexpected data after completion of previous transfer" << std::endl;
            }

            response.insert(response.end(), payload.begin(), payload.end());
        }

        if (received_size == expected_size)
        {
            expect_header = true;

            if (has_more)
            {
                // Ask more
                unsigned char more[]{8, 0, 0};
#if defined(__APPLE__)
                RS232_SendBuf(more, 3);
#elif defined(_WIN32)
                RS232_SendBuf(3, more, 3);
#else
                RS232_SendBuf(24, more, 3);
#endif
            }
            else if (is_notification)
            {
                std::cout << "Notifiation" << notification[0] << std::endl;
                started = false;
            }
            else
            {

                started = false;
                done = true;
            }
        }
    }

    return response;
}

int serial_usb::close_usb()
{
    if (connected)
    {
#if defined(__APPLE__)
        RS232_CloseComport();
#elif defined(_WIN32)
        RS232_CloseComport(3);
#else
        RS232_CloseComport(24);
#endif
    }

    connected = false;
    return 0;
}