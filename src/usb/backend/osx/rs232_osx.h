#ifndef PO_RS232_OSX_H
#define PO_RS232_OSX_H

/* Modified version of rs232 by Teunis van Beelen
   see rs232.c & rs232.h for original files

   Modifications made for dev/tty.usbmodem<ID> access on macOS.
   Modified for single com connection
*/

#if defined(__APPLE__)

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdio.h>
#include <string.h>

#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <sys/file.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <termios.h>
#include <unistd.h>

    int RS232_OpenComport(const char*, int, const char *, int);
    int RS232_PollComport(unsigned char *, int);
    int RS232_SendBuf(unsigned char *, int);
    void RS232_CloseComport();

#ifdef __cplusplus
}
#endif // __cplusplus
#endif // __APPLE__
#endif // PO_RS232_OSX_H