#ifndef PO_SERIAL_USB_H
#define PO_SERIAL_USB_H

#include "native_usb.hpp"
#include <string>

class serial_usb
{
  public:
    serial_usb();
    ~serial_usb() = default;
    int open_usb(const std::string &serial_number = "");
    int write_usb(const std::vector<unsigned char> &packet);
    std::vector<unsigned char> read_usb();
    int close_usb();

  private:
    bool connected;
};

#endif // NATIVE_USB_MOCK_H