#ifndef PO_PolarUSB_V1_H
#define PO_PolarUSB_V1_H

#include <string>
#include <vector>

#include "polar_usb.hpp"

class native_usb;

class polar_usb_v1 : public polar_usb
{
    enum usb_state
    {
        Send = 0,
        Read = 1,
        ACK = 2,
        Finish = 3
    };

  private:
    native_usb *usb;

  public:
    explicit polar_usb_v1();
    polar_usb_v1(native_usb *usb);
    ~polar_usb_v1();

    int connect() override;

  private:
    void get_data(const std::string &request, std::vector<unsigned char> &data) const override;
    std::vector<unsigned char> generate_request(const std::string &request) const;
    std::vector<unsigned char> generate_ack(const unsigned char packet_num) const;
    int is_end(const std::vector<unsigned char> &packet) const;

    void add_to_full(const std::vector<unsigned char> &packet, std::vector<unsigned char> &full,
                     const bool initial_packet, const bool final_packet) const;
};

#endif // POLARUSB_H
