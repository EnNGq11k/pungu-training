#ifndef PO_POLARUSB_V2_H
#define PO_POLARUSB_V2_H

#include <string>
#include <vector>

#include "polar_usb.hpp"

class serial_usb;

class polar_usb_v2 : public polar_usb
{
  public:
    std::string serial_number; // Apple only

  private:
    serial_usb *usb;

  public:
    explicit polar_usb_v2();
    polar_usb_v2(serial_usb *usb);
    ~polar_usb_v2();

    int connect() override;

  private:
    void get_data(const std::string &request, std::vector<unsigned char> &data) const override;
    void generate_request(const std::string &data, std::vector<unsigned char> &request) const;
};

#endif // POLARUSB_H
