# Pungu Training
A training diary with Polar device support

![](Pungu.png)

## Goals
* Digital diary
    * No more spreadsheets
    * Import data from Polar devices (No more typos :))
* Privacy
    * Keep your data on your devices
* Multi platform
    * Switch operating systems and keep your diary
* Lightweight UI
    * Should run on every PC/Mac with an up-to-date operating system
* Polar device support
    * Download from device

## Features
* Everything is on your device. No Internet connection required
* Import activities from your Polar devices (USB)
* Diary
    * Summary
    * Overview
    * Create new activities
    * Speed, heart rate and power graphs for imported activities
    * Add a comment to imported activities

## Pending for version 1
* Better builds and releases
* Better Icon :)
* More reliable downloads on Linux and Windows
* Fix Protobuf for Windows builds

# Requirements
| Name     | Min version      |
|----------|------------------|
| Protobuf | 21.7             |
| libusb   | 1.0.26           |
| qt       | 6.3.1            |
| zlib     | 1.2.12           |
| macOS    | 10.15            |
| Windows  | 10 (64-Bit)      |
| Linux    | With QT6 support |

# Run
Builds are not signed. Warnings on macOS and Windows are expected!

## Windows
~~~
# Requires 64-Bit Windows. Create a Bug if you really need a 32-Bit Version!

# Install C++ Redistributable x64 https://learn.microsoft.com/en-us/cpp/windows/latest-supported-vc-redist?view=msvc-170

# Download latest release
# Unpack and run
~~~

## macOS
~~~
## M1/M2
# Download latest release
# Install homebrew from https://brew.sh/

# Install dependencies
brew install protobuf
brew install libusb
brew install qt
brew install zlib

# Remove quarantine flag
sudo xattr -cr /Applications/Pungu.app

# Run
~~~

## Linux (Debian/Ubuntu example)
~~~
# Download latest release

# Backports required for Debian 11 (https://backports.debian.org/Instructions/)
sudo apt install protobuf-compiler
sudo apt install libusb-1.0-0-dev
sudo apt install qmake6
sudo apt install libqt6widgets6
sudo apt install libqt6charts6
sudo apt install qt6-charts-dev

# Add udev rule to access USB-Devices without sudo/root
sudo cp linux/99-polar.rules /etc/udev/rules.d
sudo udevadm control --reload
sudo udevadm trigger

# Make pungu executable
chmod +x Pungu

# Run
~~~

# Build

## OSX
~~~
# Install homebrew from https://brew.sh/

# Install dependencies
brew install protobuf
brew install libusb
brew install zlib
brew install qt

# Make
cd src
chmod +x configure.sh
./configure.sh
make all

# Build and run qt ui
make runqt

# Build and run console app (downloader)
make console && ../bin/pungu_console <outdir>
~~~

## Linux (Debian/Unbuntu example)
~~~
# Dependencies
# Backports required for Debian 11 (https://backports.debian.org/Instructions/)
sudo apt install make
sudo apt install g++
sudo apt install protobuf-compiler
sudo apt install libusb-1.0-0-dev
sudo apt install qmake6
sudo apt install libqt6widgets6
sudo apt install libqt6charts6
sudo apt install qt6-charts-dev

# Make
cd src
chmod +x configure.sh
./configure.sh
make all

# Add udev rule to access USB-Devices without sudo/root
sudo cp linux/99-polar.rules /etc/udev/rules.d
sudo udevadm control --reload
sudo udevadm trigger

# run qt ui
../bin/Pungu

# Build and run console app (downloader)
make console && ../bin/pungu_console <outdir>
~~~

## Windows
~~~
# Install "Desktop development with C++" for Visual Studio 2022 or Visual Studio 2022 Build Tools

# Install or compile QT 6.4.0 (expected path = c:\qt\QT-6.4.0)

# Start x64 Native Tools Command Promt for VS 2022

# Make
cd src
configure.cmd
nmake all

# Run
..\bin\Pungu.exe
~~~

# License
GPLv3 : https://www.gnu.org/licenses/gpl-3.0.en.html

# Attribution
## Sources
### Protobuf files (src/proto)
* https://github.com/cmaion/polar
* https://github.com/pcolby/bipolar
* https://github.com/profanum429/v800_downloader

### Original Code for "v1"-USB (src/usb/polar_usb_v1)
* https://github.com/profanum429/v800_downloader

### RS232.c
* https://www.teuniz.net/RS-232/

### Logger
* https://andydunkel.net/2017/11/08/qt_log_file_rotation_with_qdebug/

## Icons
### Run icons created by Smashicons - Flaticon
* https://www.flaticon.com/free-icons/run